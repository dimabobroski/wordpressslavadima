��          �      <      �  &   �     �  I   �  	   9  e   C     �     �     �     �     �     �       h        �  
   �     �     �  �  �  N   %  '   t  �   �     P  �   n  "   ;  2   ^     �  '   �  %   �  7   �     /  �   @     	     	      9	     Z	                                	                                                          
           Accepted post types for showing plugin E.g. 'blue', '#006699' E.g. 250 for fast readers, 150 for slow readers; the default value is 200 Free Text If active, remember to change the text accordingly (e.g.... SSSS minutes instead of ... SSSS seconds) Progress bar color Progress bar display Reading Time Round Up/Down Save Changes Show minutes instead of seconds Speed Use 'SSSS' as a placeholder for seconds, e.g. 'The estimated reading time for this post is SSSS seconds' no round down round up yes Project-Id-Version: Reading Time
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-Basepath: .
Last-Translator: 
Language: ru
X-Poedit-SearchPath-0: rd-ru_RU.po
 Принятые ‘post-types’ для отображения плагина Например, “Blue”, "#006699" например, 250 для быстрых читателей, 150 для медленных читателей; значение по умолчанию составляет 200 Свободный Текст Если активен, не забудьте соответствующим образом изменить текст (например, «… SSSS минут» вместо «… SSSS секунд»). Цвет прогресс-бара Показывание бара "Прогресс" Время чтения Округление в верх/низ Сохранить Изменения Показать минуты вместо секунд Скорость Используйте 'SSSS' в качестве атрибута-placeholder для  секунд, например, 'Оценочное время чтения для этой записи SSSS секунд' нет округление в низ округление в верх да 