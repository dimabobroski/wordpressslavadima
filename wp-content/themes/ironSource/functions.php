<?php
// Disable Gutenberg Completely.
if ( version_compare( $GLOBALS['wp_version'], '5.0-beta', '>' ) ) {
	// WP > 5 beta
	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );
} else {
	// WP < 5 beta
	add_filter( 'gutenberg_can_edit_post_type', '__return_false' );
}

// Register Style and Scripts.
function register_styles() {
	$theme_version = wp_get_theme()->get( 'Version' );
	wp_enqueue_style( 'main-style', get_stylesheet_uri(), array(), $theme_version );
	wp_deregister_script( 'wp-embed' );
	wp_deregister_script( 'jquery' );
	wp_deregister_script( 'jquery-migrate' );
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', [], '3.4.1', false );
	wp_enqueue_script( 'isScript', get_template_directory_uri() . '/js/isscript.js', [], '3.4.1', false );

}

add_action( 'wp_enqueue_scripts', 'register_styles' );

add_action( 'rest_api_init', function () {
	register_rest_route( 'post-type/v1', '/get-titles', array(
		'methods'             => 'GET',
		'callback'            => 'get_post_titles',
		'permission_callback' => '__return_true',
	) );
} );

function get_post_titles() {
	$args       = array(
		'posts_per_page'   => - 1,
		'offset'           => 0,
		'order'            => 'rand',
		'post_type'        => ! empty($_GET['post-type']) ? $_GET['post-type'] : 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true,
	);
	$posts_list = get_posts( $args );
	foreach ( $posts_list as $posts ) {
		$post_data[ $posts->ID ]['title'] = $posts->post_title;
	}
	wp_reset_postdata();

	return rest_ensure_response( $post_data );
}

class newCustomPotType {

	public function custom_post_type($post_name) {

		$labels = array(
			'name'                  => _x( $post_name, 'Post Type General Name', 'custom-' . $post_name ),
			'singular_name'         => _x( $post_name, 'Post Type Singular Name', 'custom-' . $post_name ),
			'menu_name'             => __( $post_name, 'custom-' . $post_name ),
			'name_admin_bar'        => __( $post_name, 'custom-' . $post_name ),
			'archives'              => __( 'Item Archives', 'custom-' . $post_name ),
			'attributes'            => __( 'Item Attributes', 'custom-' . $post_name ),
			'parent_item_colon'     => __( 'Parent Item:', 'custom-' . $post_name ),
			'all_items'             => __( 'All Items', 'custom-' . $post_name ),
			'add_new_item'          => __( 'Add New Item', 'custom-' . $post_name ),
			'add_new'               => __( 'Add New', 'custom-' . $post_name ),
			'new_item'              => __( 'New Item', 'custom-' . $post_name ),
			'edit_item'             => __( 'Edit Item', 'custom-' . $post_name ),
			'update_item'           => __( 'Update Item', 'custom-' . $post_name ),
			'view_item'             => __( 'View Item', 'custom-' . $post_name ),
			'view_items'            => __( 'View Items', 'custom-' . $post_name ),
			'search_items'          => __( 'Search Item', 'custom-' . $post_name ),
			'not_found'             => __( 'Not found', 'custom-' . $post_name ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'custom-' . $post_name ),
			'featured_image'        => __( 'Featured Image', 'custom-' . $post_name ),
			'set_featured_image'    => __( 'Set featured image', 'custom-' . $post_name ),
			'remove_featured_image' => __( 'Remove featured image', 'custom-' . $post_name ),
			'use_featured_image'    => __( 'Use as featured image', 'custom-' . $post_name ),
			'insert_into_item'      => __( 'Insert into item', 'custom-' . $post_name ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'custom-' . $post_name ),
			'items_list'            => __( 'Items list', 'custom-' . $post_name ),
			'items_list_navigation' => __( 'Items list navigation', 'custom-' . $post_name ),
			'filter_items_list'     => __( 'Filter items list', 'custom-' . $post_name ),
		);
		$args   = array(
			'label'               => __( $post_name, 'custom-' . $post_name ),
			'description'         => __( $post_name . ' Description', 'custom-' . $post_name ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'thumbnail', 'editor'),
			'taxonomies'          => array(),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
			'show_in_rest'        => true,
		);
		register_post_type( $post_name, $args );

	}
}

$test = new newCustomPotType();
$test->custom_post_type('trata');
$test->custom_post_type('paren');
$test->custom_post_type('dahtyn');