$(document).ready(function () {
    $('button').on('click',function (event) {
        event.preventDefault();
        $('.posts-titles').html('');
        let pystType = {
            'post-type': $('#custom-post-type-select').val()
        };
        $.get({
            url: window.location.protocol + "//" + window.location.host + "/wp-json/post-type/v1/get-titles",
            data: pystType,
            success: function( data ) {
               console.log(data);
                $.each(data, function(postId) {
                    $('.posts-titles').append(
                        '<div class="post-title" data-post-id="'+ postId +'">'
                        + this.title
                        + '</div>'
                    );
                });
            },
            error: function() {
                console.log('ajax error');
            },
        });
    });
});