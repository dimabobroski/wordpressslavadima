<?php
/*
Template Name: Home Page
Template Post Type: page
*/
get_header();
?>
<form>
	<?php
	$args       = array(
		'public' => true,
	);
	$output     = 'names'; // 'names' or 'objects' (default: 'names')
	$operator   = 'and'; // 'and' or 'or' (default: 'and')
	$post_types = get_post_types( $args, $output, $operator );
	if ( in_array( 'attachment', $post_types ) ) {
		unset( $post_types['attachment'] );
	}
	?>
	<select name="post-types" id="custom-post-type-select">
		<?php foreach ( $post_types as $post_type ) { ?>
			<option value="<?php echo $post_type; ?>"> <?php esc_html_e( $post_type ); ?></option>
		<?php } ?>
	</select>
	<button type="button">Submit</button>
</form>
<div class="posts-titles"> </div>
<?php get_footer(); ?>
