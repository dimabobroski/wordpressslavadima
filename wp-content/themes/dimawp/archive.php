<?php get_header(); global $WPG;
$pt = 'post'; ?>
<div id="ArchivePage" class="ajaxPostsBox">
    <div class="container c12 inrowf deffpad">
        <div class="addHere inrowf">
            <?php echo get_posts_ajax(['pt'=>$pt]); ?>
            <span class="addBefore col3s30 mcol2s20 smw100"></span>
        </div>
        <div class="fcc mt50">
            <button class="ajaxLoadMore" data-step="0" data-pt="<?php echo $pt; ?>" data-total="<?php echo $total = wp_count_posts($pt)->publish; ?>">
                <?php echo multilang('load_more'); ?>
            </button>
        </div>
    </div>
</div>
<?php get_footer(); ?>