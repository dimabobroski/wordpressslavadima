<?php
////****** Function to easy add Custom Post Types ******////

add_action( 'init', function () {
	$cptsArr = array(
		array(
			'slug'   => 'recommendations',
			'tax'    => 'recommendations' . '_cat',
			'single' => 'המלצה',
			'many'   => 'המלצות',
			'icon'   => 'dashicons-groups',
		),
		array(
			'slug'   => 'services',
			'tax'    => 'services' . '_cat',
			'single' => 'שירות',
			'many'   => 'שירותים',
			'icon'   => 'dashicons-portfolio',
		),
	);
	if ( $cptsArr[0]['slug'] ) {
		foreach ( $cptsArr as $new_cpt ) {
			if ( $new_cpt['slug'] ) {
				add_cpt( $new_cpt );
			}
		}
	}
} );

function add_cpt( $cpt ) {

	$single = $cpt['single'];
	$many   = $cpt['many'];
	$slug   = $cpt['slug'];
	$icon   = $cpt['icon'];

	if ( is_array( $cpt['tax'] ) ) {
		$txes = $cpt['tax'];
		foreach ( $txes as $tx => $val ) {
			unset( $cpt['tax'] );
			$taxs_array[] = $tx;
			$cpt['tax']   = array( $tx => $val );
			add_cpt_tax( $cpt );
		}
	} elseif ( $cpt['tax'] ) {
		$taxs_array = array( $cpt['tax'] );
		add_cpt_tax( $cpt );
	} else {
		unset( $tax );
	}

	if ( key_exists( 'supports', $cpt ) ) {
		$supports = $cpt['supports'];
	} else {
		$supports = array( 'title', 'editor', 'excerpt', 'author', 'thumbnail' );
	}

	$all      = multilang( 'all', false, false );
	$parent   = multilang( 'parent', false, false );
	// $parent   = trans( array( 'en' => 'Parent ' . $single, 'he' => $single . ' הורה' ) );
	$add_new = multilang( 'add_new', false, false );
	// $add_new  = trans( array( 'en' => 'Add New ', 'he' => 'הוסף חדש', 'ru' => 'Добавить' ) );
	$add_news = multilang( 'add_new', false, false );
	// $add_news = trans( array( 'en' => 'Add New ', 'he' => 'הוסף ' . $single . ' חדש', 'ru' => 'Добавить' ) );

	$labels = array(
		'name'               => $many,
		'singular_name'      => $single,
		'menu_name'          => $many,
		'parent_item_colon'  => $parent,
		'all_items'          => $all . $many,
		'view_item'          => __( 'View' ) . ' ' . $single,
		'add_new_item'       => $add_news,
		'add_new'            => $add_new,
		'edit_item'          => __( 'Edit' ) . ' ' . $single,
		'update_item'        => __( 'Update' ) . ' ' . $single,
		'search_items'       => __( 'Search' ) . ' ' . $single,
		'not_found'          => __( 'Not Found' ),
		'not_found_in_trash' => __( 'Not found in Trash' ),
	);

	$args = array(
		'label'                 => $many,
		'description'           => '',
		'labels'                => $labels,
		'supports'              => $supports,
		'taxonomies'            => $taxs_array,
		'menu_icon'             => $icon,
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_nav_menus'     => true,
		'show_in_admin_bar'     => true,
		'menu_position'         => 5,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rest_base'             => $slug,
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'capability_type'       => 'post',
	);

	register_post_type( $slug, $args );
}

function add_cpt_tax( $cpt ) {
	$single = $cpt['single'];
	$many   = $cpt['many'];
	$slug   = $cpt['slug'];
	$tax    = $cpt['tax'];

	$all   = multilang( 'all', false, false );
	$cat   = multilang( 'cat', false, false );
	$cats  = multilang( 'cats', false, false );
	$tax_t  = multilang( 'tax_t', false, false );
	// $tax_t = trans( array( 'en' => $many . ' Category', 'he' => 'קטגוריות ' . $many, 'ru' => 'Категории' . $many ) );

	if ( is_array( $tax ) ) {
		$key = array_keys( $tax )[0];
		if ( $tax[ $key ]['name'] ) {
			$cat = $tax[ $key ]['name'];
		}
		if ( $tax[ $key ]['mname'] ) {
			$cats = $tax[ $key ]['mname'];
		}
		if ( $tax[ $key ]['title'] ) {
			$tax_t = $tax[ $key ]['title'];
		}
		$tax = $key;
	}

	$parent   = multilang( 'parent', false, false );
	// $parent   = trans( array( 'en' => 'Parent ' . $cat, 'he' => $cat . ' הורה' ) );
	$add_news = multilang( 'add_new', false, false );
	// $add_news = trans( array( 'en' => 'Add New ' . $many . ' ' . $cat, 'he' => 'הוסף קטגורית ' . $many ) );

	$labels = array(
		'name'              => $tax_t,
		'singular_name'     => $tax_t,
		'search_items'      => __( 'Search' ) . ' ' . $many,
		'all_items'         => $all . $many,
		'parent_item'       => $parent,
		'parent_item_colon' => $parent . ':',
		'edit_item'         => __( 'Edit' ) . ' ' . $single,
		'update_item'       => __( 'Update' ) . ' ' . $single,
		'add_new_item'      => $add_news,
		'new_item_name'     => __( 'New' ) . ' ' . $single . ' ' . __( 'Name' ),
		'menu_name'         => $cats,
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => $tax ),
	);

	register_taxonomy( $tax, $slug, $args );
}