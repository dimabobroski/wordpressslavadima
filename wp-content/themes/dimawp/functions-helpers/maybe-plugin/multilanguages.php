<?php
function multilang( $str, $params = [], $echo = false ) {
	global $WPG;
	$lang   = $WPG['lang'] ? $WPG['lang'] : '';
	$return = '';

	$translates = array(
		'all' => [
			'en' => 'All ', 
			'he' => 'כל ה', 
			'ru' => 'Все '
		],
		'cat' => [
			'en' => 'Category', 
			'he' => 'קטגוריית', 
			'ru' => 'Категория'
		],
		'cats' => [
			'en' => 'Categories', 
			'he' => 'קטגוריות', 
			'ru' => 'Категории'
		],
		'tax_t' => [
			'en' => 'Category', 
			'he' => 'קטגוריות ', 
			'ru' => 'Категории'
		],
		'search' => [
			'en' => 'Search',
			'he' => 'חפש',
			'ru' => 'Поиск'
		],
		'parent'          => [
			'en' => 'Parent', 
			'he' => 'הורה',
			'ru' => 'Родитель',
		],
		'add_new' => [
			'en' => 'Add New', 
			'he' => 'הוסף חדש',
			'ru' => 'Добавить новое',
		],
		'contact'   => [ 'en' => 'Contact', 'he' => 'צור קשר', 'ru' => 'Контакт' ],
		'back_home' => [ 'en' => 'Back to the homepage', 'he' => 'חזרה לעמוד הבית', 'ru' => 'Назад на главную' ],
		'load_more' => [ 'en' => 'Load More', 'he' => 'טען עוד', 'ru' => 'Поиск' ],
		'read_more' => [ 'en' => 'Read more', 'he' => 'קרא אוד', 'ru' => 'Читать далее' ],
		'read_less' => [ 'en' => 'Close', 'he' => 'סגור', 'ru' => 'Закрыть' ],
		'title_404' => [
			'en' => 'The page you reached does not exist.',
			'he' => 'העמוד אליו הגעת אינו קיים.',
			'ru' => 'Страница, которую вы достигли, не существует.'
		],
		'stitle_404'      => [
			'en' => 'Here are some links to help you:',
			'he' => 'הנה כמה לינקים שיוכלו לעזור לך:',
			'ru' => 'Вот несколько ссылок, чтобы помочь вам:'
		],
		'mobile_btn_menu' => [
			'en' => 'Click To Open Main Menu',
			'he' => 'לחץ לפתיחת תפריט ראשי',
			'ru' => 'Нажмите, чтобы открыть главное меню'
		],
	);

	if ( key_exists( $str, $translates ) ) {
		if ( key_exists( $WPG['lang'], $translates[ $str ] ) ) {
			$return = $translates[ $str ][ $WPG['lang'] ];
		} else {
			$return = $translates[ $str ]['en'];
		}
	} else {
		$return = $str;
	}

	// if (has($params['before']??'')) {
	// 	$return = $params['before'].$return;
	// }

	// if (has($params['after']??'')) {
	// 	$return = $return.$params['after'];
	// }

	if ( $echo ) {
		echo $return;
	} elseif ( $return ) {
		return $return;
	}
}