<?php

function get_posts_ajax( $args = array(), $query = array() ) {
	$ajaxCall = false;
	if ( empty( $args ) ) {
		$args     = $_REQUEST;
		$ajaxCall = true;
	}
	$step          = isset( $args['step'] ) ? $args['step'] : 0;
	$pt            = $args['pt'];
	$postz         = array();
	$postz['html'] = '';
	if ( isset( $args['ppp'] ) ) {
		$showposts = $args['ppp'];
	} else {
		$showposts = get_option( 'posts_per_page' );
	}
	$offset = $showposts * $step;

	$defargs = array(
		'posts_per_page'   => $showposts,
		'offset'           => $offset,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => $pt,
		'post_status'      => 'publish',
		'suppress_filters' => true,
	);
	foreach ( $defargs as $key => $val ) {
		if ( ! key_exists( $key, $query ) ) {
			$query[ $key ] = $val;
		}
	}
	$mposts = get_posts( $query );
	if ( $mposts ) {
		foreach ( $mposts as $mp ) {
			$postz['html'] .= get_posts_ajax_html( $mp );
		}
		$postz['success'] = true;
	} else {
		$postz['success'] = false;
		$postz['message'] = 'אין פרטים נוספים לתצוגה.';
	}

	if ( $ajaxCall ) {
		echo json_encode( $postz );
		wp_die();
	} else {
		return $postz['html'];
	}
}

add_action( 'wp_ajax_get_posts_ajax', 'get_posts_ajax' );
add_action( 'wp_ajax_nopriv_get_posts_ajax', 'get_posts_ajax' );

global $ajax_html_templates, $ajax_html_templates_pipes;

$ajax_html_templates_pipes = [
	'words'  => function ( $data ) {
		return preg_replace( '/[0-9]/', '', $data );
	},
	'digets' => function ( $data ) {
		return preg_replace( '/[^0-9]/', '', $data );
	},
];
$ajax_html_templates       = [
	'post_type' => '<div class="ajaxEl">
        {{wpbfif:permalink}}
        <a class="std-btn lightblue" href="{{permalink}}">{{post_title}} - {{acf:date|words}}</a>
        {{wpbfendif:permalink}}
    </div>',
];

function get_posts_ajax_html( $data, $args = array() ) {
	global $ajax_html_templates, $ajax_html_templates_pipes;

	$data->thumb     = get_thumb_url( $data->ID );
	$data->permalink = get_the_permalink( $data->ID );
	$data->acf       = get_fields( $data->ID );
	if ( empty( $data->post_excerpt ) && ! empty( $data->post_content ) ) {
		$data->post_excerpt = trunc( $data->post_content, 20 );
	}

	if ( isset( $args['type'] ) ) {
		$ptype = $args['type'];
	} else {
		$ptype = $data->post_type;
	}
	$template = $ajax_html_templates[ $ptype ];

	$black_tokens = [ 'wpbfif', 'wpbfendif' ];

	preg_match_all( '/{{(.*?)}}/', $template, $tokens );
	if ( is_object( $data ) && is_array( $tokens ) ) {
		$theData = '';
		$tokens  = $tokens[1];
		foreach ( $tokens as $token ) {
			$origen = $token;
			$from   = $filter = '';

			if ( strpos( $token, '|' ) !== false ) {
				$temp   = explode( '|', $token );
				$token  = $temp[0];
				$filter = $temp[1];
			}

			if ( strpos( $token, ':' ) !== false ) {
				$temp  = explode( ':', $token );
				$from  = $temp[0];
				$token = $temp[1];
			}

			if ( ! in_array( $from, $black_tokens ) ) {
				if ( $from ) {
					$theData = $data->$from[ $token ];
				} elseif ( $token == 'func' ) {
					$theData = $data;
				} else {
					$theData = $data->$token;
				}
				if ( $filter ) {
					$theData = $ajax_html_templates_pipes[ $filter ]( $theData );
				}
				if ( strpos( $template, '{{wpbfif:' . $token . '}}' ) !== false ) {
					if ( empty( $theData ) || $theData == '' ) {
						$pattern = '/{{wpbfif:' . $token . '}}';
						$pattern .= '[\S\s]*?';
						$pattern .= '{{wpbfendif:' . $token . '}}/';

						preg_match_all( $pattern, $template, $after_if );
						if ( isset( $after_if[0] ) ) {
							$template = str_replace( $after_if[0], '', $template );
						}
					} else {
						$template = str_replace( '{{wpbfif:' . $token . '}}', '', $template );
						$template = str_replace( '{{wpbfendif:' . $token . '}}', '', $template );
						$template = str_replace( '{{' . $origen . '}}', $theData, $template );
					}
				} else {
					$template = str_replace( '{{' . $origen . '}}', $theData, $template );
				}
			}
		}
	}

	return $template;
}