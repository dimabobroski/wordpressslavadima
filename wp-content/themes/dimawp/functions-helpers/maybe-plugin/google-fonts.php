<?php
//*** Load Fonts ***//
function init_font( $name = 'Assistant', $sizes = '300,400,700', $he = true ) {
	$url = 'https://fonts.googleapis.com/css?family=' . $name . ':' . $sizes . '&display=swap';

	if ( $he ) {
		$url .= '&subset=hebrew';
	}

	$link = '<link href="' . $url . '" rel="stylesheet">';

	return $link;
}

function init_fonts( $fonts, $he = true ) {
	$url   = 'https://fonts.googleapis.com/css?family=';
	$sizes = '300,400,700';

	if ( is_array( $fonts ) ) {
		foreach ( $fonts as $i => $f ) {
			if ( $i ) {
				$url .= '|';
			}
			$url .= $f['name'];
			if ( isset( $f['sizes'] ) ) {
				$url .= $f['sizes'];
			} else {
				$url .= $sizes;
			}
		}
	} else {
		return init_font( $fonts );
	}

	$url .= '&display=swap';
	if ( $he ) {
		$url .= '&subset=hebrew';
	}

	$link = '<link href="' . $url . '" rel="stylesheet">';

	return $link;
}