<?php
//*** Set tiny mce options ***//
function tiny_mce_before_init( $in ) {
	$in['toolbar1']             = 'bold,italic,blockquote,bullist,numlist,alignleft,aligncenter,alignright,link,unlink,table,fullscreen,undo,redo,wp_adv,spellchecker,dfw,y_video_button,ltr';
	$in['toolbar2']             = 'fontselect,fontsizeselect,formatselect,alignjustify,strikethrough,outdent,indent,pastetext,removeformat,charmap,wp_more,emoticons,forecolor,wp_help';
	$in['wordpress_adv_hidden'] = false;

	return $in;
}

add_filter( 'tiny_mce_before_init', 'tiny_mce_before_init' );