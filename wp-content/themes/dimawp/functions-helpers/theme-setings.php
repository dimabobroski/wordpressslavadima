<?php
//*** Customize dashboard menu NEED REFUCTOR***//
function remove_admin_menu_links() {
	remove_theme_support( 'genesis-admin-menu' );
	remove_submenu_page( 'themes.php', 'nav-menus.php' );
	remove_submenu_page( 'themes.php', 'widgets.php' );
	remove_submenu_page( 'themes.php', 'customize.php' );
	add_menu_page( 'Menus', __( 'Menus' ), 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
	//add_menu_page('Widgets', __('Widgets'), 'manage_options', 'widgets.php', '', 'dashicons-welcome-widgets-menus', 31);
	add_menu_page( 'wpbf', 'wpbf', 'manage_options', 'admin.php?page=wpbf', 'wpbf_settings_page', 'https://wpbf.com/wp-content/themes/wpbf/images/favicon.png', 59 );
}

add_action( 'admin_menu', 'remove_admin_menu_links', 900 );

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title'  => 'Theme Settings',
		'menu_title'  => 'Theme Settings',
		'menu_slug'   => 'wpbf-theme-settings',
		'capability'  => 'manage_options',
		'redirect'    => false,
		'parent_slug' => 'admin.php?page=wpbf',
	) );
}

function wpbf_settings_page() {
	$return = '<div id="wpbfSettingsPage" class="fcc">';
	$return .= '<div class="box">';
	$return .= '<div class="logo fcc"></div>';
	$return .= '</div>';
	$return .= '</div>';

	echo $return;
}

if ( current_user_can( 'administrator' ) && ! is_admin() && 1 ) {
	ini_set( 'display_errors', 1 );
	ini_set( 'display_startup_errors', 1 );
	error_reporting( E_ALL );
}