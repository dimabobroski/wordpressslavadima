<?php
//*** Function to get object and recive default if empty ***//
function get_object( $type = 'text', $slug, $field_empty = '', $from = '', $size = 'medium' ) {
	$field = get_field_from( $slug, $from );
	$fe    = ( empty( $field ) ) ? true : false;
	switch ( $type ) {
		case 'svg' :
			$class  = $from;
			$from   = $field_empty;
			$alt    = $size;
			$field  = get_field_from( $slug, $from );
			$result = svg( $field, $class, $alt );
			break;
		case 'img' :
			if ( $fe ) {
				$img_url = img_empty( $field_empty );
			} else {
				$img_url = img_size( $field, $size );
			}
			$result = $img_url;
			break;
		case 'bg' :
			if ( $fe ) {
				$img_url = img_empty( $field_empty );
			} else {
				$img_url = img_size( $field, $size );
			}
			$result = 'background-image:url(' . $img_url . ');';
			break;
		default :
			$result = get_text( $field, $field_empty );
	}

	return $result;
}

//*** Help function for get_object() to get text ***//
function get_text( $txt, $string ) {
	if ( empty( $txt ) && ! is_array( $string ) ) {
		$txt = $string;
	}

	return $txt;
}

//*** Help function for get_object() to get filed ***//
function get_field_from( $slug, $from = '' ) {
	if ( is_object( $from ) ) {
		if ( array_key_exists( 'taxonomy', $from ) ) {
			$sel = 'term';
		} else {
			$sel = 'post';
		}
	} elseif ( strpos( $from, '_' ) !== false ) {
		$sel = 'term';
	} else {
		$sel = $from;
	}
	switch ( $sel ) {
		case 'term' :
			$field = get_field( $slug, $from );
			break;
		case 'options' :
			$field = get_field( $slug, 'options' );
			break;
		default :
			$field = get_field( $slug, $from );
	}

	return $field;
}

//*** Help function for get_object() to get defaul if image empty ***//
function img_empty( $def = '' ) {
	global $WPG;

	if ( strpos( $def, 'http' ) !== false || strpos( $def, 'www' ) !== false ) {
		$url = $def;
	} elseif ( $def ) {
		$url = $WPG['dir_img'] . $def;
	} else {
		$url = $WPG['dir_img'] . 'defthumb.jpg';
	}

	return $url;
}

//*** Help function for get_object() to get image ***//
function img_size( $img, $size ) {
	if ( is_array( $img ) ) {
		if ( $size == 'url' || $size == 'full' ) {
			$url = $img['url'];
		} elseif ( empty( $size ) ) {
			$url = $img['sizes']['medium'];
		} else {
			$url = $img['sizes'][ $size ];
		}
	} else {
		$url = $img;
	}

	return $url;
}