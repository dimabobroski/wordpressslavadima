<?php
function make_select( $data, $class = '', $options_only = false ) {
	$loader = '<div class="sk-circle" tabindex="-1"><div class="sk-circle1 sk-child"></div><div class="sk-circle2 sk-child"></div><div class="sk-circle3 sk-child"></div><div class="sk-circle4 sk-child"></div><div class="sk-circle5 sk-child"></div><div class="sk-circle6 sk-child"></div><div class="sk-circle7 sk-child"></div><div class="sk-circle8 sk-child"></div><div class="sk-circle9 sk-child"></div><div class="sk-circle10 sk-child"></div><div class="sk-circle11 sk-child"></div><div class="sk-circle12 sk-child"></div></div>';

	$id   = 'sel-' . uniqid();
	$lfor = $id . '-i';
	if ( key_exists( 'multi', $data ) ) {
		$multi = ' multiple';
		$chkbx = '<span class="checkbox"></span>';
		$class .= ' multi';
	} else {
		$multi = '';
		$chkbx = '';
	}
	if ( key_exists( 'icon', $data ) ) {
		$icon = $data['icon'];
	} else {
		$icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 240.811 240.811" style="enable-background:new 0 0 240.811 240.811;" xml:space="preserve"><g><path d="M220.088,57.667l-99.671,99.695L20.746,57.655c-4.752-4.752-12.439-4.752-17.191,0   c-4.74,4.752-4.74,12.451,0,17.203l108.261,108.297l0,0l0,0c4.74,4.752,12.439,4.752,17.179,0L237.256,74.859   c4.74-4.752,4.74-12.463,0-17.215C232.528,52.915,224.828,52.915,220.088,57.667z"/><g></g><g></g><g></g><g></g><g></g><g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
	}
	if ( $data['sel'] || strpos( $class, 'disabled' ) !== false ) {
		$i       = 0;
		$lis     = $options = '';
		$options = '<option></option>';
		if ( $data['sel'] ) {
			foreach ( $data['sel'] as $key => $option ) {
				$el_id = $seld = '';
				if ( is_array( $option ) ) {
					$el_id  = $option['id'];
					$option = $option['option'];
				}
				if ( trim( $option ) ) {
					if ( strpos( $option, '|' ) !== false ) {
						$tr      = explode( '|', $option );
						$moptios = '<div class="moptions anim flex aic fw">';
						foreach ( $tr as $i => $radio ) {
							if ( $i ) {
								$rid     = $id . '-rdio' . $i;
								$moptios .= '<div class="radiobox flex aic">';
								$moptios .= '<div class="flex"><input type="radio" id="' . $rid . '" name="' . strtolower( str_replace( ' ', '-', trim( $tr[0] ) ) ) . '-option" value="' . $radio . '"></div>';
								$moptios .= '<label for="' . $rid . '">' . $radio . '</label>';
								$moptios .= '</div>';
							}
						}
						$moptios .= '</div>';
						$option  = $tr[0];
					} else {
						$moptios = '';
					}
					if ( ! key_exists( 'key', $data ) ) {
						$key = $option;
					}
					if ( key_exists( 'selected', $data ) && $key == $data['selected'] ) {
						$selected = ' selected';
						$seld     = $option;
						$class    .= ' focus';
					} else {
						$selected = '';
					}
					if ( strpos( $option, 'Other:' ) !== false ) {
						$other = ' other';
					} else {
						$other = '';
					}
					$clss    = 'option-' . $i;
					$lis     .= '<li class="anim enterToClick arrowsNav' . $other . $selected . '" data-select="' . $id . '" data-class="' . $clss . '" data-shel="' . $el_id . '" tabindex="0"><div class="flex aic">' . $chkbx . '<span class="lbl">' . $option . '</span></div>' . $moptios . '</li>';
					$options .= '<option class="' . $clss . '" value="' . $key . '"' . $selected . '>' . $option . '</option>';
					$i ++;
				}
			}
		}
		$ul   = '<ul>' . $lis . '</ul>';
		$drop = '<div class="dropbox anim"><div class="sizer">';
		if ( $data['search'] ) {
			if ( is_string( $data['search'] ) ) {
				$splh = $data['search'];
			} else {
				$splh = multilang('search');
			}
			$drop .= '<div class="sbox"><input type="text" placeholder="' . $splh . '"></div>' . $loader;
		}
		$drop .= $ul;
		$drop .= '</div></div>';
		if ( key_exists( 'name', $data ) ) {
			$name = $data['name'];
		} else {
			$name = '';
		}
		$select = '<select id="' . $id . '" name="' . $name . '" style="display:none;"' . $multi . '>' . $options . '</select>';
		if ( key_exists( 'req', $data ) ) {
			$req = ' required';
		} else {
			$req = '';
		}
		if ( key_exists( 'iclass', $data ) ) {
			$iclas = $data['iclass'];
		} else {
			$iclas = '';
		}
		if ( key_exists( 'placeholder', $data ) ) {
			$input = '<input value="' . $seld . '" type="text" class="enterToClick' . $iclas . '" placeholder="' . $data['placeholder'] . '"' . $req . ' readonly>';
		} else {
			$input = '<input value="' . $seld . '" id="' . $lfor . '" type="text" class="enterToClick' . $iclas . '"' . $req . ' readonly>';
		}
		$input = '<span class="iw">' . $input . $icon . '</span>';
		if ( key_exists( 'label', $data ) ) {
			$label = '<label for="' . $lfor . '">';
			if ( key_exists( 'req', $data ) ) {
				$label .= '<span class="req">*</span>';
			}
			$label .= $data['label'] . '</label>';
		}

		if ( $options_only ) {
			$sbox['select'] = $select;
			$sbox['drop']   = $drop;
		} else {
			if ( $el_id ) {
				$class .= ' shel';
			}
			if ( key_exists( 'inholder', $data ) ) {
				$inholder = $data['inholder'];
			} else {
				$inholder = '';
			}
			$temp = '<div class="holder" style="position:relative;">' . $label . $input . $drop . $inholder . '</div>';
			if ( key_exists( 'id', $data ) ) {
				$sbox = '<div id="' . $data['id'] . '" class="reselbox cfi cfs' . $class . '">';
			} else {
				$sbox = '<div class="reselbox cfi cfs' . $class . '">';
			}
			if ( key_exists( 'sel', $data ) ) {
				$sbox .= $select;
			}
			$sbox .= $temp . '</div>';
		}

		return $sbox;
	} else {
		return;
	}
}

function make_upload( $data, $class = '' ) {
	$id = 'upload-' . uniqid();

	if ( $data['req'] ) {
		$req = ' required';
	}
	if ( $data['placeholder'] ) {
		$input = '<input type="text" placeholder="' . $data['placeholder'] . '" readonly>';
	} else {
		$input = '<input id="' . $id . '" type="text" readonly>';
	}
	$input = '<span class="iw">' . $input . '</span>';
	if ( $data['label'] ) {
		$label = '<label for="' . $id . '">' . $data['label'] . '</label>';
	}
	$file = '<input id="' . $id . '" type="file" name="file" style="opacity:0;width:100%;height:100%;"' . $req . '>';
	$sbox = '<div class="fileupbox cfi' . $class . '" style="position:relative"><div class="float">' . $label . $input . '</div>' . $file . '</div>';

	return $sbox;
}

function make_tabs( $data, $params = array() ) {
	$h_tabs  = $b_tabs = $h_class = $b_class = $n_class = '';
	$tabs_id = 'tabs-' . uniqid();
	if ( key_exists( 'h_class', $params ) ) {
		$h_class = $params['h_class'];
	}
	if ( key_exists( 'b_class', $params ) ) {
		$b_class = $params['b_class'];
	}
	if ( empty( $params['svg_size'] ) ) {
		$params['svg_size'] = 35;
	}
	if ( empty( $params['svg'] ) ) {
		$params['svg'] = svg_s( '<svg class="color" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129"><g><g><path d="m64.5,122.6c32,0 58.1-26 58.1-58.1s-26-58-58.1-58-58,26-58,58 26,58.1 58,58.1zm0-108c27.5,5.32907e-15 49.9,22.4 49.9,49.9s-22.4,49.9-49.9,49.9-49.9-22.4-49.9-49.9 22.4-49.9 49.9-49.9z"></path><path d="m70,93.5c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2 1.6-1.6 1.6-4.2 0-5.8l-23.5-23.5 23.5-23.5c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8,0l-26.4,26.4c-0.8,0.8-1.2,1.8-1.2,2.9s0.4,2.1 1.2,2.9l26.4,26.4z"></path></g></g></svg>', $params['svg_size'] );
	}
	if ( ! empty( $data ) && is_array( $data ) ) {
		$head_wrap = '<div class="TabsHead ' . $tabs_id . $h_class . '" data-tid="' . $tabs_id . '"><div class="thbox anim">';
		$body_wrap = '<div class="TabsHolder ' . $tabs_id . $b_class . '" data-tid="' . $tabs_id . '">';
		$cdiv      = '</div>';

		$i = 0;
		foreach ( $data as $key => $val ) {
			$id = 'tab-' . uniqid();
			if ( $i == 0 ) {
				$class = ' active';
			} else {
				$class = '';
			}
			if ( is_array( $val ) ) {
				$title = $val['title'];
				$text  = $val['text'];
			} else {
				$title = $key;
				$text  = $val;
			}
			$h_tabs .= '<button id="' . $id . '" class="anim enterToClick arrowsNav' . $class . '" type="button" data-tid="' . $tabs_id . '">' . $title . '</button>';
			$b_tabs .= '<div class="tabbox ' . $id . $class . ' anim" data-tid="' . $tabs_id . '">' . $text . '</div>';

			$i ++;
		}

		$next = '<button class="next enterToClick ltr-scaleX-1" type="button" data-tid="' . $tabs_id . '">' . $params['svg'] . '</button>';
		$prev = '<button class="prev enterToClick disabled rtl-scaleX-1" type="button" data-tid="' . $tabs_id . '">' . $params['svg'] . '</button>';

		if ( key_exists( 'n_class', $params ) ) {
			$n_class = $params['n_class'];
		}

		$tabs['head'] = $head_wrap . $h_tabs . $cdiv . $cdiv;
		$tabs['body'] = $body_wrap . $b_tabs . $cdiv;
		$tabs['navs'] = '<div class="TabsNav inrowf ' . $tabs_id . $n_class . '" data-tid="' . $tabs_id . '">' . $prev . $next . '</div>';

		return $tabs;
	} else {
		return;
	}
}

function make_faqs( $data, $params = array() ) {
	$faqs = '';
	if ( key_exists( 'icon', $params ) ) {
		$icon = $params['icon'];
	} else {
		$icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="color" version="1.1" x="0px" y="0px" width="284.929px" height="284.929px" viewBox="0 0 284.929 284.929" style="enable-background:new 0 0 284.929 284.929;" xml:space="preserve"><g><path d="M282.082,76.511l-14.274-14.273c-1.902-1.906-4.093-2.856-6.57-2.856c-2.471,0-4.661,0.95-6.563,2.856L142.466,174.441   L30.262,62.241c-1.903-1.906-4.093-2.856-6.567-2.856c-2.475,0-4.665,0.95-6.567,2.856L2.856,76.515C0.95,78.417,0,80.607,0,83.082   c0,2.473,0.953,4.663,2.856,6.565l133.043,133.046c1.902,1.903,4.093,2.854,6.567,2.854s4.661-0.951,6.562-2.854L282.082,89.647   c1.902-1.903,2.847-4.093,2.847-6.565C284.929,80.607,283.984,78.417,282.082,76.511z"></path></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
	}
	if ( is_array( $data ) ) {
		foreach ( $data as $faq ) {
			$title  = '<h3 class="flex aic jcsb anim enterToClick"><span>' . $faq['q'] . '</span>' . $icon . '</h3>';
			$answer = '<div class="answrap anim overh"><div class="sizer">' . $faq['a'] . '</div></div>';
			$faqs   .= '<li class="arrowsNav" tabindex="0">' . $title . $answer . '</li>';
		}
	}
	$faqs = '<div class="FAQsBlock"><ul>' . $faqs . '</ul></div>';

	return $faqs;
}

function make_chkbox( $data, $params = array() ) {
	$chkboxes = '';
	if ( key_exists( 'icon', $params ) ) {
		$icon = $params['icon'];
	} else {
		$icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="color" version="1.1" viewBox="0 0 26 26" enable-background="new 0 0 26 26"><path d="m.3,14c-0.2-0.2-0.3-0.5-0.3-0.7s0.1-0.5 0.3-0.7l1.4-1.4c0.4-0.4 1-0.4 1.4,0l.1,.1 5.5,5.9c0.2,0.2 0.5,0.2 0.7,0l13.4-13.9h0.1v-8.88178e-16c0.4-0.4 1-0.4 1.4,0l1.4,1.4c0.4,0.4 0.4,1 0,1.4l0,0-16,16.6c-0.2,0.2-0.4,0.3-0.7,0.3-0.3,0-0.5-0.1-0.7-0.3l-7.8-8.4-.2-.3z"></path></svg>';
	}
	if ( $data ) {
		foreach ( $data as $key => $val ) {
			$id       = 'chkbox-' . uniqid();
			$input    = '<div class="checkbox"><input id="' . $id . '" type="checkbox" name="' . $key . '" value="' . $val . '" tabindex="-1"><span class="fcc anim">' . $icon . '</span></div>';
			$label    = ' <label for="' . $id . '">' . $val . '</label>';
			$chkboxes .= '<div class="CHeckWrap enterToClickOn" data-click-on="#' . $id . '" tabindex="0"><div class="flex aic">' . $input . $label . '</div></div><br>';
		}
	}

	return $chkboxes;
}