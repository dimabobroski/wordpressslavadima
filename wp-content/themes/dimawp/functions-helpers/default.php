<?php
//*** Load current page data to global $WPG NEED REFUCTOR !! ***//
function cpage_load() {
	global $WPG, $template;
	$WPG['noindex'] = false;

	$wp              = $GLOBALS['wp_query'];
	$WPG['id']       = $wp->queried_object_id;
	$WPG['format']   = get_post_format( $WPG['id'] ) ?: 'standard';
	$temp            = explode( '/', $template );
	$WPG['template'] = end( $temp );

	$WPG['is_home']  = $wp->is_home;
	$WPG['is_front'] = $wp->is_front_page();

	$WPG['is_page']   = $wp->is_page;
	$WPG['is_single'] = $wp->is_single;

	$WPG['is_cat']     = $wp->is_category;
	$WPG['is_tag']     = $wp->is_tag;
	$WPG['is_tax']     = $wp->is_tax;
	$WPG['is_term']    = ( $wp->is_tax || $wp->is_category || $wp->is_archive ) ? true : false;
	$WPG['is_archive'] = $wp->is_archive;

	$WPG['is_loop']       = $wp->in_the_loop;
	$WPG['is_search']     = $wp->is_search;
	$WPG['is_404']        = $wp->is_404;
	$WPG['is_paged']      = $wp->is_paged;
	$WPG['is_admin']      = $wp->is_admin;
	$WPG['is_attachment'] = $wp->is_attachment;
	$WPG['is_singular']   = $wp->is_singular;
	$WPG['is_robots']     = $wp->is_robots;
	$WPG['is_embed']      = $wp->is_embed;

	if ( $WPG['is_term'] ) {
		$WPG['title'] = $wp->queried_object->name;
		$WPG['type']  = $wp->queried_object->taxonomy;
		$temp         = explode( '-', str_replace( '.php', '', $WPG['template'] ) );
		if ( empty( $WPG['type'] ) ) {
			$WPG['type'] = end( $temp );
		}
		$WPG['tax'] = $WPG['type'] . '_' . $WPG['id'];
	} elseif ( $wp->queried_object ) {
		$WPG['title'] = $wp->queried_object->post_title;
		$WPG['type']  = $wp->queried_object->post_type;
	}
	if ( empty( $WPG['type'] ) && $wp->query_vars ) {
		$WPG['type'] = $wp->query_vars['post_type'];
	}

	if ( ! empty( $WPG['id'] && empty( $WPG['template'] ) ) ) {
		$template = get_post_meta( $WPG['id'], '_wp_page_template', true );
		if ( ! $template || 'default' == $template ) {
			return '';
		}
		$WPG['template'] = $template;
	}

	$noindex = array(
		'search.php',
		'page-wpbf-login.php',
	);
	if ( $WPG['is_search'] || in_array( $WPG['template'], $noindex ) ) {
		$WPG['noindex'] = true;
	}

	if ( $WPG['is_page'] || $WPG['is_single'] ) {
		$WPG['from'] = $WPG['id'];
	} elseif ( key_exists( 'tax', $WPG ) ) {
		$WPG['from'] = $WPG['tax'];
	}
	if ( function_exists( 'get_field' ) && key_exists( 'from', $WPG ) ) {
		$WPG['fields'] = get_fields( $WPG['from'] );
	}
	$WPG = array_filter( $WPG, function ( $value ) {
		return $value !== '';
	} );
}

function favicon() {
	global $WPG;

	$icon = ( isset( $WPG['options']['favicon'] ) && ! empty( $WPG['options']['favicon'] ) ) ? $WPG['options']['favicon'] : $WPG['dir_img'] . 'favicon.png';

	echo '<link rel="icon" href="' . $icon . '" type="image/png"/>';
}

add_action( 'wp_head', 'favicon' );
add_action( 'admin_head', 'favicon' );

//*** Disable Gutenberg Completely ***//
if ( version_compare( $GLOBALS['wp_version'], '5.0-beta', '>' ) ) {
	// WP > 5 beta
	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );
} else {
	// WP < 5 beta
	add_filter( 'gutenberg_can_edit_post_type', '__return_false' );
}

//*** Async scripts loading ***//
function async_scripts_load( $url ) {
	global $WPG;
	if ( strpos( $url, '#asyncload' ) === false ) {
		return $url;
	} elseif ( $WPG['is_admin'] ) {
		return str_replace( '#asyncload', '', $url );
	} else {
		return str_replace( '#asyncload', '', $url ) . "' async='async";
	}
}
add_filter( 'clean_url', 'async_scripts_load', 11, 1 );
////****** WP Speed up END ******////

//*** Enqueue custom scripts and styles ***//
function custom_admin_js_css() {
	global $WPG;
	if ( $WPG['is_admin'] ) {
		wp_enqueue_script( 'admin_script', $WPG['dir_js'] . 'admin.js' );
		wp_enqueue_style( 'dashboard-style', $WPG['dir_css'] . 'dashboard.css' );
	}
}

add_action( 'admin_head', 'custom_admin_js_css' );

//*** Add support of posts thumbnail ***//
add_theme_support( 'post-thumbnails' );

//*** Add titles for custom media sizes ***//
function custom_sizes_titles( $sizes ) {

	return array_merge( $sizes, array(
		'hd'           => 'HD 720p',
		'full-hd'      => 'Full HD',
		'mobile'       => 'Mobile',
		'medium_large' => 'Medium Large',
	) );
}

add_filter( 'image_size_names_choose', 'custom_sizes_titles' );

//*** Add custom media sizes *** NEED REFACTOR//
add_image_size( 'thumbnail', 250, 250, true );
add_image_size( 'medium', 550, 550 );
add_image_size( 'large', 1024, 1024, true );
add_image_size( 'full-hd', 1920, 1080 );
add_image_size( 'hd', 1280, 720 );
add_image_size( 'mobile', 800, 9999 );
add_image_size( 'medium_large', 760, 9999 );

//*** Customize dashboard menu NEED REFUCTOR***//
function remove_admin_menu_links() {
	remove_theme_support( 'genesis-admin-menu' );
	remove_submenu_page( 'themes.php', 'nav-menus.php' );
	remove_submenu_page( 'themes.php', 'widgets.php' );
	remove_submenu_page( 'themes.php', 'customize.php' );
	add_menu_page( 'Menus', __( 'Menus' ), 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
	//add_menu_page('Widgets', __('Widgets'), 'manage_options', 'widgets.php', '', 'dashicons-welcome-widgets-menus', 31);
	add_menu_page( 'wpbf', 'wpbf', 'manage_options', 'admin.php?page=wpbf', 'wpbf_settings_page', 'https://wpbf.com/wp-content/themes/wpbf/images/favicon.png', 59 );
}

add_action( 'admin_menu', 'remove_admin_menu_links', 900 );

//*** Register custom menu locations ***//
function register_menus() {
	register_nav_menu( 'header-menu', __( 'Header Menu' ) );
	register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
	register_nav_menu( 'sidebar-menu', __( 'Sidebar Menu' ) );
	register_nav_menu( 'menu-404', __( '404 Menu' ) );
}

add_action( 'init', 'register_menus' );

//*** Function to better way to enqueue scripts or styles ***//
function add_script_style( $files ) {
	global $WPG, $page_scripts_styles;

	if ( is_array( $files ) ) {
		foreach ( $files as $file ) {
			if ( ! in_array( $file, $page_scripts_styles ) ) {
				if ( strpos( $file, 'http' ) !== false || strpos( $file, 'www' ) !== false ) {
					wp_enqueue_script( parse_url( $file, PHP_URL_HOST ) . '-scripts', $file, false, '', true );
				} else {
					if ( strpos( $file, '.js' ) !== false ) {
						if ( strpos( $file, '/' ) !== false ) {
							wp_enqueue_script( $file . '-scripts', $WPG['dir_root'] . $file, false, '', true );
						} else {
							wp_enqueue_script( $file . '-scripts', $WPG['dir_js'] . $file, false, '', true );
						}
					} elseif ( strpos( $file, '.css' ) !== false ) {
						if ( strpos( $file, '/' ) !== false ) {
							wp_enqueue_style( $file . '-style', $WPG['dir_root'] . $file );
						} else {
							wp_enqueue_style( $file . '-style', $WPG['dir_css'] . $file );
						}
					}
				}
				$page_scripts_styles[] = $file;
			}
		}
	} elseif ( ! in_array( $files, $page_scripts_styles ) ) {
		if ( strpos( $files, 'http' ) !== false || strpos( $files, 'www' ) !== false ) {
			wp_enqueue_script( parse_url( $files, PHP_URL_HOST ) . '-scripts', $files, false, '', true );
		} else {
			if ( strpos( $files, '.js' ) !== false ) {
				wp_enqueue_script( $files . '-scripts', $WPG['dir_js'] . $files, false, '', true );
			} elseif ( strpos( $files, '.css' ) !== false ) {
				wp_enqueue_style( $files . '-style', $WPG['dir_css'] . $files );
			}
		}
		$page_scripts_styles[] = $files;
	}
}

//*** Function to easy load svg file by name ***//
function svg( $url, $class = '', $field = '', $alt = '' ) {
	global $WPG;
	$nosvg = $WPG['dir_root'] . '/images/icons/nosvg.png';
	if ( $field == 'f' ) {
		$url = get_field( $url );
	} elseif ( $field == 'fo' ) {
		$url = get_field( $url, 'options' );
	}
	if ( is_array( $url ) ) {
		if ( empty( $alt ) ) {
			$alt = $url['alt'];
		}
		$url = $url['url'];
		$svg = $url;
	} else {
		$svg = $url;
	}
	$svg  = explode( '.', $svg );
	$csvg = count( $svg );
	if ( $csvg == 2 ) {
		$path = TEMPLATEPATH . '/images/' . $svg[0] . '.svg';
		if ( file_exists( $path ) ) {
			$svg = file_get_contents( $path );
			$svg = preg_replace( '#\s(id|class)="[^"]+"#', '', $svg );
			$svg = explode( '<svg', $svg );
			$svg = '<svg class="' . $class . ' anim" ' . $svg[1];
		} else {
			$svg = '<img src="' . $nosvg . '" alt="" class="' . $class . '"/>';
		}
	} else {
		$place = $csvg - 1;
		if ( $svg[ $place ] == 'svg' ) {
			$url  = explode( '/', $url );
			$c    = count( $url ) - 1;
			$path = WP_CONTENT_DIR . '/uploads/' . $url[ $c - 2 ] . '/' . $url[ $c - 1 ] . '/' . $url[ $c ];
			$svg  = file_get_contents( $path );
			$svg  = preg_replace( '#\s(id|class)="[^"]+"#', '', $svg );
			$svg  = explode( '<svg', $svg );
			$svg  = '<svg class="' . $class . ' anim" ' . $svg[1];
		} else {
			$svg = '<img src="' . $url . '" alt="' . $alt . '" class="' . $class . '"/>';
		}
	}

	return $svg;
}

function svg_s( $svg, $size, $class = '' ) {
	$temp = explode( '>', $svg );
	if ( is_string( $size ) ) {
		$size = explode( ':', $size );
		$wd   = $size[0];
		$he   = $size[1];
	} else {
		$wd = $he = $size;
	}
	$temp[0] = preg_replace( '/(<[^>]+) id=".*?"/i', '$1', $temp[0] );
	if ( strpos( $temp[0], 'style="' ) !== false ) {
		$temp[0] = str_replace( 'style="', 'style="width:' . $wd . 'px;height:' . $he . 'px;min-width:' . $wd . 'px;min-height:' . $he . 'px;', $temp[0] );
	} else {
		$temp[0] = str_replace( '<svg', '<svg style="width:' . $wd . 'px;height:' . $he . 'px;min-width:' . $wd . 'px;min-height:' . $he . 'px;" ', $temp[0] );
	}
	if ( $class ) {
		if ( strpos( $temp[0], 'class="' ) !== false ) {
			$temp[0] = str_replace( 'class="', 'class="' . $class . ' ', $temp[0] );
		} else {
			$temp[0] = str_replace( '<svg', '<svg class="' . $class . '" ', $temp[0] );
		}
	}
	$sized = implode( '>', $temp );

	return $sized;
}

function js_variables() {
	global $WPG;

	$variables = array(
		'test' => 't',
		'ajax_url'  => admin_url( 'admin-ajax.php' ),
		'is_mobile' => $WPG['is_mobile']
	);
	echo(
		'<script type="text/javascript">window.wp_data = ' . json_encode( $variables ) . ';</script>'
	);
}

add_action( 'wp_head', 'js_variables' );
add_action( 'admin_head', 'js_variables' );

//*** Limit maximum file size for upload ***//
function media_upload_size_limit( $size ) {
	$size = 10 * 1024 * 1024;

	return $size;
}

add_filter( 'upload_size_limit', 'media_upload_size_limit', 20 );