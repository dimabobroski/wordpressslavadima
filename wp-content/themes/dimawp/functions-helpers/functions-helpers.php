<?php
function phone_link( $num ) {
	return preg_replace( "/[^A-Za-z0-9+]/", "", $num );
}

function map_link( $addr, $waze = false ) {
	global $WPG;

	if ( $WPG['is_mobile'] || $waze ) {
		$link = 'waze://?q=' . $addr['address'];
	} else {
		$link = 'http://www.google.com/maps/place/' . $addr["lat"] . ',' . $addr["lng"];
	}

	return $link;
}

//*** Remove disallowed tags from string ***//
function remove_ctags( $str, $disallowed = array( 'http://', 'https://', 'www.', 'WWW.' ) ) {
	if ( $disallowed ) {
		foreach ( $disallowed as $d ) {
			if ( strpos( $str, $d ) === 0 ) {
				$str = str_replace( $d, '', $str );
			}
		}

		return $str;
	} else {
		return;
	}
}

//*** Function to use before get_header() to insert script to wp_head ***//
function add_head_scripts( $files ) {
	global $WPG, $head_scripts;

	$lang = $WPG['lang'];

	if ( is_array( $files ) ) {
		foreach ( $files as $file ) {
			if ( strpos( $file, 'maps.googleapis.com' ) !== false ) {
				$file .= '&language=' . $lang;
			}
			$head_scripts .= '<script type="text/javascript" src="' . $file . '"></script>';
		}
	} else {
		if ( strpos( $files, 'maps.googleapis.com' ) !== false ) {
			$files .= '&language=' . $lang;
		}
		$head_scripts = '<script type="text/javascript" src="' . $files . '"></script>';
	}
}

function init_head_scripts() {
	global $head_scripts;
	if ( ! empty( $head_scripts ) ) {
		echo $head_scripts;
	}
}

add_action( 'wp_head', 'init_head_scripts' );

//** NEED REFUCTORING **//
function btn( $inn = '', $url = '', $params = [], $loading = true ) {
	//https://ianlunn.github.io/Hover/ Used animations

	$loader = '';

	if ( isset( $params['tag'] ) ) {
		$tag = $params['tag'];
	} elseif ( ! empty( $url ) ) {
		$tag = 'a';
	} else {
		$tag = 'button';
	}
	if ( isset( $params['class'] ) ) {
		$class = $params['class'];
	} else {
		$class = 'stdbtn def hfs16 br20';
	}

	$html = '<' . $tag;
	if ( ! empty( $url ) ) {
		$html .= ' href="' . $url . '"';
	}
	$html .= ' role="button"';
	if ( $loading ) {
		if ( $loading != true ) {
			$loader = $loading;
		} else {
			$class  .= ' loadbtn';
			$loader = '<div class="loadLayer fcc">';
			$loader .= '<div class="bloader">Loading...</div>';
			$loader .= '</div>';
		}
	}
	$html .= ' class="' . $class . '"';
	if ( isset( $params['attr'] ) ) {
		$html .= ' ' . $params['attr'];
	}
	if ( isset( $params['label'] ) ) {
		$html .= ' aria-label="' . $params['label'] . '"';
	}
	if ( isset( $params['labelby'] ) ) {
		$html .= ' aria-labelledby="' . $params['labelby'] . '"';
	}
	if ( isset( $params['desc'] ) ) {
		$html .= ' aria-describedby="' . $params['desc'] . '"';
	}
	if ( isset( $params['data'] ) ) {
		if ( is_array( $params['data'] ) ) {
			foreach ( $params['data'] as $k => $v ) {
				$html .= ' data-' . $k . '="' . $v . '"';
			}
		} else {
			$html .= ' ' . $params['data'];
		}
	}
	$html .= '>';
	if ( isset( $params['before'] ) ) {
		$html .= '<i class="icon">' . $params['before'] . '</i>';
	}
	$html .= '<span class="notouch">';
	$html .= $inn;
	$html .= '</span>';
	if ( isset( $params['after'] ) ) {
		$html .= '<i class="icon">' . $params['after'] . '</i>';
	}
	if ( $loader ) {
		$html .= $loader;
	}
	$html .= '</' . $tag . '>';

	return $html;
}

function rimg( $slug, $params = [ 'id' => null, 'def' => '', 'return' => 'url' ] ) {
	global $WPG;
	$data = null;
	$def  = $return = '';

	if ( $WPG['is_mobile'] ) {
		$mob_slug = $slug . '_mob';
	}
	if ( isset( $params['def'] ) && ! empty( $params['def'] ) ) {
		$def = $params['def'];
	} else {
		$def = 'defthumb.jpg';
	}
	if ( isset( $params['id'] ) && $params['id'] != null && $params['id'] != '' ) {
		if ( $WPG['is_mobile'] ) {
			$data = get_field( $mob_slug, $params['id'] );
		}
		if ( $data == null || $data == '' ) {
			$data = get_field( $slug, $params['id'] );
		}
	} else {
		if ( $WPG['is_mobile'] ) {
			if ( isset( $WPG['fields'][ $mob_slug ] ) && ! empty( $WPG['fields'][ $mob_slug ] ) ) {
				$data = $WPG['fields'][ $mob_slug ];
			}
		}
		if ( isset( $WPG['fields'][ $slug ] ) && ! empty( $WPG['fields'][ $slug ] ) && $data == null ) {
			$data = $WPG['fields'][ $slug ];
		}
	}
	if ( $data == null || $data == '' ) {
		$data = $WPG['dir_img'] . $def;
	}

	switch ( $params['return'] ) {
		case 'img':
			$alt = $attr = '';
			if ( isset( $params['alt'] ) && ! empty( $params['alt'] ) ) {
				$alt = $params['alt'];
			}
			if ( isset( $params['attr'] ) && ! empty( $params['attr'] ) ) {
				$attr = $params['attr'];
			}
			$return = '<img src="' . $data . '"';
			if ( isset( $params['class'] ) && ! empty( $params['class'] ) ) {
				$return .= ' ' . $params['class'];
			}
			if ( $attr != '' ) {
				$return .= ' ' . $attr;
			}
			$return .= ' alt="' . $alt . '"/>';
			break;

		case 'bg':
			$return = 'background-image: url(' . $data . ');';
			break;

		default:
			$return = $data;
			break;
	}

	return $return;
}

function get_menu( $name, $class = '', $depth = 2 ) {
	if ( has_nav_menu( $name ) ) {
		wp_nav_menu( array(
			'theme_location'  => $name,
			'depth'           => $depth,
			'container_class' => $name,
			'menu_class'      => $class
		) );
	} else {
		return '';
	}
}

function get_page_url( $id ) {
	$link = '';

	if ( is_string( $id ) ) {
		$tpl_pages = get_pages( array(
			'meta_key'   => '_wp_page_template',
			'meta_value' => $id
		) );
		if ( is_array( $tpl_pages ) && count( $tpl_pages ) ) {
			$tpl_pages = $tpl_pages[0];
		}
		$link = get_the_permalink( $tpl_pages->ID );
	} elseif ( is_integer( $id ) ) {
		$link = get_the_permalink( $id );
	} elseif ( is_object( $id ) ) {
		if ( isset( $id->term_id ) ) {
			$link = get_term_link( $id );
		} else {
			$link = get_the_permalink( $id );
		}
	}

	return $link;
}

function has( $el = '' ) {
	$return = false;

	if ( is_array( $el ) && sizeof( $el ) || is_object( $el ) && sizeof( (array) $el ) ) {
		$return = true;
	} elseif ( ! empty( $el ) ) {
		$return = true;
	}

	return $return;
}

//*** Function to easy work with dates ***//
function date_convert( $date, $format, $rformat = '', $params ) {
	if ( empty( $rformat ) ) {
		$rformat = $format;
		$backf   = false;
	} else {
		$backf = true;
	}

	$Timezone              = new DateTimeZone( 'Asia/Jerusalem' );
	$DateTime              = DateTime::createFromFormat( $format, $date, $Timezone );
	$date_array['date']    = $DateTime->format( $rformat );
	$date_array['date_wd'] = $DateTime->format( 'w' );

	if ( $params['calc'] ) {
		$DateTime->modify( $params['calc'] );
		$date_array['calc']    = $DateTime->format( $rformat );
		$date_array['calc_wd'] = $DateTime->format( 'w' );
	}

	if ( $params['diff'] == 'now' ) {
		$now                  = ( new DateTime( 'now' ) )->format( 'YmdHis' );
		$ymd                  = $DateTime->format( 'YmdHis' );
		$willbe               = ( $ymd - $now > 0 ? true : false );
		$date_array['willbe'] = $willbe;
	}

	if ( $params['return'] == 'array' ) {
		$date_array['last'] = $DateTime->format( 't' );
		$date               = $date_array;
	} else {
		if ( $params['diff'] == 'now' ) {
			$date = $willbe;
		} else {
			$date = $DateTime->format( $rformat );
		}
	}

	return $date;
}

function get_thumb_url( $id = '', $size = 'medium', $def = '', $alt = '0' ) {
	if ( $id == '' ) {
		$id = $post->ID;
	}
	if ( is_object( $id ) ) {
		$thumb = get_field( 'thumb', $id->taxonomy . '_' . $id->term_id );
		$url   = $thumb["sizes"][ $size ];
	} elseif ( is_array( $id ) ) {
		$thumb = get_field( 'thumb', $id["tax"] . '_' . $id["id"] );
		$url   = $thumb["sizes"][ $size ];
	} else {
		$thumbnail_id = get_post_thumbnail_id( $id );
		$url          = wp_get_attachment_image_src( $thumbnail_id, $size )[0];
	}
	if ( empty( $url ) ) {
		if ( $def ) {
			$url = get_dir( 'images/' . $def );
		} else {
			$url = get_dir( 'images/defthumb.jpg' );
		}
	}
	if ( $alt ) {
		$alt        = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
		$arr['url'] = $url;
		$arr['alt'] = $alt;

		return $arr;
	} else {
		return $url;
	}
}