<?php get_header(); ?>
<div id="SinglePage">
	<div class="timer"><?php the_reading_time(array(
				'reading_time_text'        => 'The estimated reading time for this post is SSSS seconds',
				'reading_time_speed'       => '500',
				'reading_time_bar_color'   => 'green',
				'reading_time_bar_display' => 'yes',
				'reading_time_minutes'     => 'no',
				'reading_time_round'       => 'up',
			)
		);?></div>
	<span></span>
	<div class="second-timer"><?php echo get_reading_time(array(
			'reading_time_text'        => 'The estimated reading time for this post is SSSS seconds',
			'reading_time_speed'       => '500',
			'reading_time_bar_color'   => 'red',
			'reading_time_bar_display' => 'yes',
			'reading_time_minutes'     => 'no',
			'reading_time_round'       => 'up',
		));?></div>
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <div class="entry">
            <?php the_content(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>