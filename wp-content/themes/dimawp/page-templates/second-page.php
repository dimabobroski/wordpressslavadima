<?php
/*
Template Name: Second Template for tests
Template Post Type: post, page, two-post
*/
get_header();
$current_id = get_queried_object()->ID;
$all_sections_fields = get_fields($current_id);
if ( count( $all_sections_fields['modules_holder'] ) > 0 ) {
	foreach ( $all_sections_fields['modules_holder'] as $module ) {
		if ( ! empty( $module['acf_fc_layout'] ) ) {
			$module_id         = $module['acf_fc_layout'];
			$module_id_changed = str_replace( '_', '-', $module_id );
			get_template_part( './modules/' . $module_id_changed, $module_id_changed, $module[ $module_id ] );
		}
	}
}
?>
<?php get_footer(); ?>
