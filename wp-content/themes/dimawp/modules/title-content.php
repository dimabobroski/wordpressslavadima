<?php
if ( ! empty( $args ) ) {
	extract( $args );
	$section_title = ! empty( $section_title ) ? $section_title : 'no title';;
	$section_content = ! empty( $section_content ) ? $section_content : 'no content';
	?>
	<div> <?php esc_html_e( $section_title ); ?> </div>
	<div> <?php echo $section_content; ?> </div>
	<?php
}
?>
