<?php
if ( ! empty( $args ) ) {
	extract( $args );
	$section_title = ! empty( $section_title ) ? $section_title : 'no title';;
	$section_content = ! empty( $section_content ) ? $section_content : 'no content';
	$section_image   = ! empty( $section_image ) ? $section_image : 'no image';
	?>
	<?php if ( is_array( $section_image ) ) { ?>
		<img src="<?php echo esc_url( $section_image['url'] ); ?>"
		     alt="<?php echo esc_attr( $section_image['alt'] ); ?>"/>
	<?php } else { ?>
		<div>no image</div>
	<?php } ?>
	<div> <?php echo esc_html( $section_title ); ?> </div>
	<div> <?php echo $section_content; ?> </div>
<?php } ?>