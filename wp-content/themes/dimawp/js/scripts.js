var w_width = $(window).width();
var w_height = $(window).height();
var mobWidth = 800;
var isMobile = false;
var SmallScreen = false;
var header = '';
var footerh = '';
var is_rtl = false;
if ($('html').attr('dir') == 'rtl') { is_rtl = true; }

// Small Screen Detect //
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

if (isMobile || w_width < mobWidth) { SmallScreen = true; }

if (SmallScreen && $('table').length > 0) {
    (function () {
        var headertext = [];
        var headers = document.querySelectorAll("thead");
        var tablebody = document.querySelectorAll("tbody");

        for (var i = 0; i < headers.length; i++) {
            headertext[i] = [];
            for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) {
                var current = headrow;
                headertext[i].push(current.textContent.replace(/\r?\n|\r/, ""));
            }
        }

        if (headers.length > 0) {
            for (var h = 0, tbody; tbody = tablebody[h]; h++) {
                for (var i = 0, row; row = tbody.rows[i]; i++) {
                    for (var j = 0, col; col = row.cells[j]; j++) {
                        col.setAttribute("data-th", headertext[h][j]);
                    }
                }
            }
        }
    }());
}

// Events //

$(document).ready(function () {
    drady_init();
    $(window).on("load", function () {
        imgsratio();
        obj_hcheck();
    });
});

$(window).resize(function () {
    imgsratio();
    obj_hcheck();
});

$(document).on('wpcf7:mailsent', function (e) {
    wpcf7Noty(e);
});

$(document).on('wpcf7:invalid', function (e) {
    wpcf7Noty(e, false);
});

if (0) {
    var lastScrollTop = 0;
    $(window).on('scroll', function () {
        var st = $(this).scrollTop();
        if (st > 300) {
            $('#BackToTop').addClass('show');
            if (!$('header').hasClass('stiky')) {
                $('#HeadFix').height($('header').outerHeight());
            }
            $('header').addClass('stiky');
            if (st > lastScrollTop) {
                $('header').addClass('sdown');
                $('header').removeClass('sup');
            } else {
                $('header').removeClass('sdown');
                $('header').addClass('sup');
            }
        } else {
            $('#BackToTop').removeClass('show');
            $('header').removeClass('stiky');
            $('#HeadFix').height(0);
            $('header').removeClass('sup sdown');
        }
        lastScrollTop = st;
    });
}

// Functions //
function obj_hcheck() {
    header = $('header').outerHeight();
    footerh = $('footer').outerHeight();
    var hp = $('header').css('position');
    if (hp == 'fixed' || hp == 'absolute') {
        $('#HeadFix').height(header);
    }
}

function imgsratio() {
    if ($('.resize').length > 0) {
        $('.resize').each(function () {
            var rait;
            if (isMobile) var mob = $(this).attr('data-mratio');
            if (mob) rait = mob;
            else rait = $(this).attr('data-ratio');
            if (rait == null) rait = '16:9';
            var ow = $(this).width();
            var tmp = rait.split(':');
            var w = tmp[0];
            var h = tmp[1];
            $(this).height((ow / w) * h);
            $(this).addClass('resized');
        });
    }
}

function wpcf7Noty(formWrap, success) {
    var mess = '';
    var $form = $(formWrap.target).find('form');
    var $submit = $form.find('input[type="submit"]');
    $submit.prop('disabled', true);
    if (success != false) {
        success = true;
    }
    setTimeout(function () {
        mess = $('div.wpcf7-response-output').text();
        if (mess) {
            if (success) {
                $('body').mnSuccess(mess);
                $form.get(0).reset();
            } else {
                $('body').mnError(mess);
            }
        }
        setTimeout(function () {
            $submit.prop('disabled', false);
        }, 1500);
    }, 200);
}

function m_nav() {
    $('nav#nav ul.menu > li.menu-item-has-children > a,nav#nav ul.menu > li.menu-item-has-children > span').append('<button class="arrow"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 306 306" style="enable-background:new 0 0 306 306;" xml:space="preserve"><g><g><polygon points="247.35,270.3 130.05,153 247.35,35.7 211.65,0 58.65,153 211.65,306" /></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g<g></g><g></g><g></g><g></g><g></g></svg></button>');
    $('nav#nav ul.menu > li.menu-item-has-children').click(function (e) {
        $('nav#nav ul.menu > li.menu-item-has-children').not($(this)).removeClass('active');
        var $target = $(e.target);
        if ($target.hasClass('arrow')) {
            e.preventDefault();
            var $this = $(this);
            $this.toggleClass('active');
            if ($this.hasClass('active')) {
                var $subm = $this.find('> ul.sub-menu');
                var h = 20;
                $subm.find('> li').each(function () {
                    h += $(this).outerHeight();
                });
                $subm.height(h);
            }
        }
    });
}
// Easy Life //

function drady_init() {

    if ($('.videopop').length) {
        init_video_popup();
    }
    if ($('.TabsHead').length) {
        init_tabs();
    }
    if ($('.reselbox').length) {
        init_selects();
    }
    if ($('.FAQsBlock').length) {
        init_faqs();
    }
    if ($('.labelsform').length) {
        init_labelsform();
    }
    if ($(":-webkit-autofill").length) {
        $(":-webkit-autofill").parents('.cfi').addClass('focus');
    }
    if (SmallScreen) {
        m_nav();
    }
    $('li.menu-item-has-children').find('> a').attr('aria-expanded', 'false');
    $(document).on({
        focus: function () {
            $(this).find('> a').attr('aria-expanded', 'true');
        },
        focusout: function () {
            $(this).find('> a').attr('aria-expanded', 'false');
        },
        mouseenter: function () {
            $(this).find('> a').attr('aria-expanded', 'true');
        },
        mouseleave: function () {
            $(this).find('> a').attr('aria-expanded', 'false');
        }
    }, 'li.menu-item-has-children');
}

function init_video_popup() {
    var scriptEls = document.getElementsByTagName('script');
    var scriptPath = '';
    $.each(scriptEls, function () {
        var src = $(this).get(0).src;
        if (src.indexOf("scripts.js") >= 0 && src.indexOf("contact-form-7") < 0) {
            scriptPath = src;
            return false;
        }
    });
    var scriptFolder = scriptPath.substr(0, scriptPath.lastIndexOf('/') + 1);
    var vpscript = document.createElement("script");
    vpscript.type = 'text/javascript';
    vpscript.src = scriptFolder + 'jquery.dcd.video.min.js';

    var $style = $('<style type="text/css">.popupbox{position:fixed;top:0;right:0;bottom:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,.8);z-index:900;opacity:0;visibility:hidden;pointer-events:none;-webkit-transition:all .5s ease;transition:all .5s ease}.popupbox.active{opacity:1;visibility:visible;pointer-events:auto;-webkit-transition:opacity .4s;transition:opacity .4s}.popupbox .vbox{width:96%;max-height:96%;max-width:860px;height:auto;background-color:#252525;-webkit-transition:opacity .2s,height .5s;transition:opacity .2s,height .5s;transition-delay:0s}.popupbox.active .vbox{transition-delay:.5s}.popupbox:not(.active) .vbox{height:0!important}.popupbox .vbox #cPlayer,.popupbox .vbox #cPlayer iframe{width:100%!important;height:100%!important}.popupbox .vbox .cls{position:absolute;width:0;height:0;background-color:#fff;-webkit-transform:translate(50%,-50%) scale(1);transform:translate(50%,-50%) scale(1);-webkit-transition:all .3s ease;transition:all .3s ease;transition-delay:0s}.popupbox.active .vbox .cls{transition-delay:1s;width:30px;height:30px}.popupbox .vbox .cls svg{width:100%;height:100%;fill:#E2574C}.popupbox .vbox .cls:hover{background-color:#E2574C}.popupbox .vbox .cls:hover svg{fill:#fff}</style>');
    $style.appendTo('head');

    $('#Wrapper').after(vpscript);
    $('#Wrapper').after('<div id="cPlayBox" class="fcc popupbox"><div class="vbox relative" data-ratio="16:9"><div class="cls cp circlebox"><svg class="anim" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 286.054 286.054" style="enable-background:new 0 0 286.054 286.054;" xml:space="preserve"><g><path style="fill:#E2574C;" d="M168.352,142.924l25.28-25.28c3.495-3.504,3.495-9.154,0-12.64l-12.64-12.649c-3.495-3.486-9.145-3.495-12.64,0l-25.289,25.289l-25.271-25.271c-3.504-3.504-9.163-3.504-12.658-0.018l-12.64,12.649c-3.495,3.486-3.486,9.154,0.018,12.649l25.271,25.271L92.556,168.15c-3.495,3.495-3.495,9.145,0,12.64l12.64,12.649c3.495,3.486,9.145,3.495,12.64,0l25.226-25.226l25.405,25.414c3.504,3.504,9.163,3.504,12.658,0.009l12.64-12.64c3.495-3.495,3.486-9.154-0.009-12.658L168.352,142.924z M143.027,0.004C64.031,0.004,0,64.036,0,143.022c0,78.996,64.031,143.027,143.027,143.027s143.027-64.031,143.027-143.027C286.054,64.045,222.022,0.004,143.027,0.004z M143.027,259.232c-64.183,0-116.209-52.026-116.209-116.209s52.026-116.21,116.209-116.21s116.209,52.026,116.209,116.209S207.21,259.232,143.027,259.232z"></path></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div><div id="cPlayer" class="overh" data-type="" data-code=""></div></div></div>');

    var rait = $('#cPlayBox .vbox').attr('data-ratio');
    if (rait == null) rait = '16:9';
    var ow = $('#cPlayBox .vbox').width();
    var tmp = rait.split(':');
    var w = tmp[0];
    var h = tmp[1];
    $('#cPlayBox .vbox').height((ow / w) * h);

    $('.videopop').click(function () {
        $('#cPlayer').attr('data-type', $(this).attr('data-type'));
        $('#cPlayer').attr('data-code', $(this).attr('data-code'));
        setTimeout(function () {
            $('#cPlayer').video();
            $('#cPlayBox').addClass('active');
            $('body').addClass('overh');
        }, 500);
    });
    $('#cPlayBox .cls').click(function () {
        $('#cPlayBox').removeClass('active');
        $('body').removeClass('overh');
        setTimeout(function () {
            $('#cPlayer').remove();
        }, 500);
        $('#cPlayBox .vbox').append('<div id="cPlayer" class="overh" data-type="" data-code=""></div>');
    });
}
$('.enterToClick').keyup(function(e) {
    var key = e.which;
    if (key == 13) {
        $(this).trigger('click');
    }
});
$('.enterToClickOn').keyup(function(e) {
    var key = e.which;
    if (key == 13) {
        var on = $(this).data('click-on');
        $(on).trigger('click');
    }
});
$('.arrowsNav').keyup(function(e) {
    var key = e.which;
    var el = '';
    if (key == 37 || key == 40) {
        if (is_rtl) {
            el = $(this).next();
        } else {
            el = $(this).prev();
        }
    } else if (key == 39 || key == 38) {
        if (is_rtl) {
            el = $(this).prev().focus();
        } else {
            el = $(this).next().focus();
        }
    }
    if ($(this).hasClass('focusClick')) {
        $(el).focus().trigger('click');
    } else {
        $(el).focus();
    }
});
function init_tabs() {
    $('.TabsHolder').each(function(){
        $(this).height($(this).find('.tabbox.active').outerHeight());
    });
    $('.TabsHead button').click(function(){
        var $this = $(this);
        var $tab = $('.tabbox.' + $(this).attr('id'));
        var $tabs_id = $this.data('tid');
        var $tabs_nav = $('.TabsNav.' + $tabs_id);

        $this.addClass('active');
        $this.siblings().removeClass('active');
        $tab.addClass('active');
        $tab.siblings().removeClass('active');
        $tab.parents('.TabsHolder').height($tab.outerHeight());

        if (SmallScreen) {
            var $th = $($this.parents('.TabsHead'));
            var $thb = $($this.parents('.thbox'));
            var toh = $this.outerHeight();
            if ($th.hasClass('dropped')) {
                $thb.prepend($this);
                $th.height(toh);
                $thb.height(toh);
                setTimeout(function(){
                    $th.removeClass('dropped');
                },600);
            } else {
                $th.height(toh);
                $thb.height(toh);
                $th.addClass('dropped');
                var dh = 0;
                $thb.find('button').each(function(){
                    dh += $(this).outerHeight();
                });
                $thb.height(dh);
            }
            if ($tabs_nav.length) {
                $tabs_nav.find('.disabled').removeClass('disabled');
            }
        } else {
            if ($tabs_nav.length) {
                if ($this.next().length) {
                    $tabs_nav.find('.next').removeClass('disabled');
                } else {
                    $tabs_nav.find('.next').addClass('disabled');
                }
                if ($this.prev().length) {
                    $tabs_nav.find('.prev').removeClass('disabled');
                } else {
                    $tabs_nav.find('.prev').addClass('disabled');
                }
            }
        }
    });
    $('.TabsNav button').click(function() {
        var id = $(this).data('tid');
        var $tabs = $('.TabsHead.' + id + ' .thbox');
        var $current = $($tabs.find('button.active'));
        var $tabs_nav = $('.TabsNav.' + id);

        if (SmallScreen) {
            $tabs_nav.find('.disabled').removeClass('disabled');
        }
        if ($(this).hasClass('next')) {
            if (SmallScreen) {
                var $next = $tabs.find('button').last();
                if ($next) {
                    $next.addClass('active');
                    $next.siblings().removeClass('active');
                    $tabs.prepend($next);
                }
            } else {
                var $next = $($current.next());
                if ($next) {
                    $next.trigger('click');
                }
            }
        } else {
            if (SmallScreen) {
                var $prev = $tabs.find('button').eq(2);
                if ($prev) {
                    $prev.addClass('active');
                    $prev.siblings().removeClass('active');
                    $tabs.prepend($prev);
                }
            } else {
                var $prev = $($current.prev());
                if ($prev) {
                    $prev.trigger('click');
                }
            }
        }
    });
}

function init_selects() {
    $('.reselbox .iw input').keyup(function (e) {
        var $papa = $(this).parents('.reselbox');
        switch (e.which) {
            case 37: // left
                break;

            case 38: // up
                $papa.find('.dropbox ul li:last-child').focus();
                break;

            case 39: // right
                break;

            case 40: // down
                $papa.find('.dropbox ul li:first-child').focus();
                break;

            default: return; // exit this handler for other keys
        }
        e.preventDefault();
    });
    $('.reselbox .iw input').focus(function () {
        var $input = $(this);
        var $box = $input.parents('.reselbox');

        $box.toggleClass('dropped');
        if ($box.hasClass('dropped')) {
            $box.find('.dropbox').height($box.find('.dropbox .sizer').outerHeight());
        } else {
            $box.find('.dropbox').height(0);
        }
    });
    $('.reselbox .iw input').click(function () {
        var $input = $(this);
        var $box = $input.parents('.reselbox');

        $box.toggleClass('dropped');
        if ($box.hasClass('dropped')) {
            $box.find('.dropbox').height($box.find('.dropbox .sizer').outerHeight());
        } else {
            $box.find('.dropbox').height(0);
        }
    });
    $('.reselbox .dropbox input').keyup(function () {
        var $this = $(this);
        var val = $this.val();
        var dbox = $this.parents('.dropbox');

        dbox.find('li.finded').removeClass('finded');
        if (val) {
            dbox.addClass('searching');
            if (val.length >= 3) {
                var result = dbox.find('li:contains(' + val + ')');
                result.addClass('finded');
                if (result.length) {
                    dbox.addClass('hfinded');
                } else {
                    dbox.removeClass('hfinded');
                }
            }
        } else {
            dbox.removeClass('searching hfinded');
        }
        dbox.height(dbox.find('.sizer').outerHeight());
    });
    $('.reselbox li').click(function () {
        var $this = $(this);
        var $select = $('#' + $this.data('select'));
        var multi = false;
        if ($select.attr('multiple')) {
            multi = true;
        }
        var $box = $this.parents('.reselbox');
        var $input = $box.find('.iw input');
        var cls = '.' + $(this).data('class');

        $this.toggleClass('selected');
        if (multi) {
            var inp_val = '';
            $select.find("option:selected").prop("selected", false);
            $box.find('li.selected').each(function (i) {
                var tcls = '.' + $(this).data('class');
                $select.find(tcls).prop('selected', true);
                if (i) {
                    inp_val += ', ' + $(this).text();
                } else {
                    inp_val = $(this).text();
                }
            });
            $input.prop('value', inp_val);
            $select.change();
            $box.addClass('focus');
        } else {
            $this.siblings().removeClass('selected');
            if ($this.hasClass('selected')) {
                $select.find(cls).prop('selected', true).change();
                $input.prop('value', $this.text());
                $box.addClass('focus');
            } else {
                $select.find(cls).prop('selected', false).change();
                $input.prop('value', '');
                $box.removeClass('focus');
            }
            $box.removeClass('dropped');
            $box.find('.dropbox').height(0);
        }
    });
    $(document).on('click', function (e) {
        var $resel = $(e.target).parents('.reselbox');
        if (!$resel.length) {
            $('.reselbox').removeClass('dropped');
            $('.reselbox .dropbox').height(0);
        } else {
            $('.reselbox').not($resel).removeClass('dropped').find('.dropbox').height(0);
        }
    });
}

function init_faqs() {
    $('.FAQsBlock h3').click(function(){
        var li = $(this).parents('li');
        var not = li.siblings();
        li.toggleClass('active');
        if (li.hasClass('active')) {
            li.find('.answrap').height(li.find('.answrap .sizer').outerHeight());
        } else {
            li.find('.answrap').height(0);
        }
        not.removeClass('active');
        not.find('.answrap').height(0);
    });
    $('.FAQsBlock li').keyup(function(e){
        var key = e.which;
        if (key == 13) {
            $(this).find('h3').trigger('click');
        }
    });
}

function init_labelsform() {
    $(document).on('focus click', '.labelsform input, .labelsform textarea', function(e) {
        var $this = $(e.target);
        if (e.target.type == 'textarea') {
            $this.parents('.cft').addClass('focus');
        } else {
            $this.parents('.cfi').addClass('focus');
        }
    });
    $(document).on('focusout', '.labelsform input, .labelsform textarea', function(e) {
        var $this = $(e.target);
        if ($this.val() === '') {
            if (e.target.type == 'textarea') {
                $this.parents('.cft').removeClass('focus');
            } else {
                $this.parents('.cfi').removeClass('focus');
            }
        }
    });
}

// Click Events //
$('#BackToTop').click(function() {
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
});
$('#toggleDir').click(function() {
    var dir = $('html').attr('dir');
    switch (dir) {
        case 'ltr':
            dir = 'rtl';
            break;
        default:
            dir = 'ltr';
            break;
    }
    $('html').attr('dir',dir);
});
$('#MobNavBtn').click(function () {
    $('nav#nav').toggleClass('active');
    if ($('nav#nav').hasClass('active')) {
        $(this).addClass('active');
        $('body').addClass('overh');
    } else {
        $(this).removeClass('active');
        $('body').removeClass('overh');
    }
});
$('.ajaxLoadMore').click(function () {
    var clickedBtn = $(this);
    var appendTo = $(this).parents('.ajaxPostsBox').find('.addHere');
    var pt = $(this).data('pt');
    var tax = $(this).data('tax');
    var step = parseInt($(this).attr('data-step')) + 1;
    var total = $(this).data('total');

    clickedBtn.attr('disabled', 'disabled');

    $.ajax({
        type: "POST",
        url: window.wp_data.ajax_url,
        dataType: "json",
        data: {
            action: 'get_posts_ajax',
            step: step,
            tax: tax,
            pt: pt
        },
        //contentType: false,
        //processData: false,
        success: function (response) {
            //console.log(response);
            if (response.success) {
                if (appendTo.find('.addBefore').length) {
                    appendTo.find('.addBefore').before(response.html);
                } else {
                    appendTo.append(response.html);
                }
                if (appendTo.find('.ajaxEl').length != total) {
                    clickedBtn.removeAttr('disabled', 'disabled');
                }
            } else if (response.message) {
                $('body').mnError(response.message);
            }
        }
    });
    $(this).attr('data-step', step);
});