<form id="SearchForm" action="<?php bloginfo('siteurl'); ?>" method="get">
    <div class="flex labelsform">
        <div class="relative cfi f1">
            <label class="hfs20 tc33 fw-light" for="s"></label>
            <input class="enterToClickOn" data-click-on="#SearchSubmit" type="text" id="s" name="s" value=""/>
        </div>
        <div class="cfb"><input id="SearchSubmit" type="submit" value="חפש"/></div>
    </div>
</form>
