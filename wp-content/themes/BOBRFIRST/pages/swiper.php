<?php
/*
Template Name: Swiper
*/
get_header(); global $WPG; $fe = $WPG['fields']; ?>
<div id="SwiperPage">
    <div class="container">
        <?php if (has($fe['gall']??'')) { ?>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach($fe['gall'] as $g) { ?>
                <div class="swiper-slide bgimg" style="background-image:url(<?= $g['url']; ?>);"></div>
                <?php } ?>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-scrollbar"></div>
        </div>
        <?php function SwiperJS() { ?>
        <script>
            var mySwiper = new Swiper ('.swiper-container', {
                //direction: 'vertical',
                slidesPerView: 4,
                spaceBetween: 20,
                loop: true,
                speed: 900,
                virtualTranslate: true,
                effect: 'fade',
                autoplay: {
                    delay: 4000,
                },
                pagination: {
                    el: '.swiper-pagination',
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                scrollbar: {
                    el: '.swiper-scrollbar',
                },
                breakpoints: {
                    320: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    },
                    640: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    }
                }
            });
        </script>
        <?php } if (!function_exists('SwiperJS')) { 
            add_action( 'wp_footer', 'SwiperJS',999); 
        }
        add_script_style(array('swiper.min.css','swiper.min.js')); ?>
        <?php } ?>
    </div>
</div>
<?php get_footer(); ?>