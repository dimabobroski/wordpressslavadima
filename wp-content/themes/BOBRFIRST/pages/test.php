<?php
/*
Template Name: Test
*/
get_header(); global $WPG; $fe = &$WPG['fields']; ?>
<div id="TestPage">
    <div class="container" style="padding-top:50px;">
        <div class="entry mb50">
            <?php the_content(); ?>
            <div class="clear"></div>
        </div>
        <div class="mb40">
            <?= make_upload([
                'name'  => 'upload',
                'label' => 'Upload',
            ]); ?>
        </div>
        <div class="mb40">
        <?= make_chkbox([
            'name'  => 'name ?',
            'label' => 'label ?',
        ]); ?>
        </div>
        <?php if (has($fe['test']??'')) { ?>
        <div class="mb40">
            <?= svg($fe['test'],'hd'); ?>
        </div>
        <?php } ?>
        <?php if (has($fe['select']??'')) { ?>
        <div class="mb40">
            <?= make_select($fe['select']); ?>
        </div>
        <?php } ?>
        <?php $select = [
            'name'  => 'test',
            'label' => 'test',
            'multi' => true,
            'search' => true,
            'sel'   => ['--Please choose an option--','opt 1','opt 2','opt 3','opt 4','opt 5'],
            'selected' => '--Please choose an option--'
        ];
        if (has($select??'')) { ?>
        <div class="mb40">
            <?= make_select($select); ?>
        </div>
        <?php } ?>

        <div class="mb40">
        <?php if (has($fe['tabs']??'')) {
            $tabs = make_tabs($fe['tabs']); 
            if ($tabs) {
                echo $tabs['head']; 
                echo $tabs['body']; 
            } ?>
        <?php } ?>
        </div>
        <?php if (has($fe['faqs']??'')) { ?>
        <div class="mb40">
            <?= make_faqs($fe['faqs']); ?>
        </div>
        <?php } ?>
        <?php if (has($fe['iframe']??'')) { ?>
        <div class="vbox resize" data-ratio="16:9">
            <?= iframe($fe['iframe']); ?>
        </div>
        <?php } ?>
    </div>
    <form action="" class="labelsform">
        <div class="cfi">
            <label for="">test</label>
            <input type="text">
        </div>
        <div class="cft">
            <label for="">Text</label>
            <textarea name="" id="" cols="30" rows="10"></textarea>
        </div>
    </form>
</div>
<?php get_footer(); ?>