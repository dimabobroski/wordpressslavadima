<?php
/*
Template Name: FAQ`s
*/
get_header(); global $WPG; $fe = $WPG['fields']; ?>
<div id="FAQsPage">
    <div class="container">
        <?php if (isset($fe['faqs'])) { echo make_faqs($fe['faqs']); } ?>
    </div>
</div>
<?php get_footer(); ?>