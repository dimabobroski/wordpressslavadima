<?php
/*
Template Name: Buttons
*/
get_header(); global $WPG; ?>
<div id="ButtonsPage">
    <?php $effects = ['hov-str','hov-stl','hov-stb','hov-stt','hov-sih','hov-soh','hov-siv','hov-sov','hov-ufl','hov-ufc','hov-ufr','hov-ofl','hov-ofc','hov-ofr','hov-rev','hov-urev','hov-orev','hov-holl'];
    foreach ($effects as $effect) {
        echo btn('Click Here '.$effect,'#',['class'=>'stdbtn def br20 b2 mb5 '.$effect]);
    } ?>
</div>
<?php get_footer(); ?>