<?php
/*
Template Name: Fonts
*/
get_header(); global $WPG; ?>
<div id="FontsPage">
    <?php $size = 14;
    while ($size != 60) {
        echo '<div style="padding:20px 40px;">';
        echo '<p class="hfs'.$size.'">בדוק גודל טקסט זה ברזולוציית מסך מסוימת '.$size.'</p>';
        echo '<p class="hfs'.$size.'">Test This Text Size In some screen resolution '.$size.'</p>';
        echo '<p class="hfs'.$size.'">Протестируйте этот размер текста в некотором разрешении экрана '.$size.'</p>';
        echo '</div>';
        if($size < 40) {
            $size++;
        } else {
            $size = $size + 5;
        }
    } ?>
</div>
<?php get_footer(); ?>