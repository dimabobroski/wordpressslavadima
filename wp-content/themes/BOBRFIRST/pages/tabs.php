<?php
/*
Template Name: Tabs
*/
get_header(); global $WPG; $fe = $WPG['fields']; ?>
<div id="TabsPage">
    <div class="container">
        <?php if (isset($fe['tabs'])) { 
            $tabs = make_tabs($fe['tabs']); 
            if ($tabs) {
                echo $tabs['head']; 
                echo $tabs['body']; 
            }
        } ?>
    </div>
</div>
<?php get_footer(); ?>