<?php get_header(); global $WPG; ?>
<div id="ArchiveSearch">
    <div class="container c14 defpadd">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article>
            <h3 class="hfs25 fw-bold mb15"><a href="<?php echo get_the_permalink($post->ID); ?>" class="diblock anim"><?php echo $post->post_title; ?></a></h3>
            <div class="entry">
                <?php the_content(); ?>
            </div>
        </article>
        <?php endwhile; endif; ?>
    </div>
</div>
<?php get_footer(); ?>