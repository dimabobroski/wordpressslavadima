const
    $window = $(window),
    $header = $('header'),
    keyEnter = 13,
    keySpace = 32,
    keyUp    = 38,
    keyEsc   = 27,
    keyDown  = 40,
    keyLeft  = 39,
    keyRight = 37,
    isMobile = window.wp_data.isMobile,
    mobWidth = window.wp_data.mobWidth,
    isRtl    = window.wp_data.isRtl;
var
    tagsInited   = false,
    windowWidth  = $window.width(),
    windowHeight = $window.height(),
    smallScreen  = false,
    headerHeight = $header.outerHeight(),
    lastScrollTop = 0;

//** Document Ready */
$(function () {
    docReadyInit();
});

//** Window Scroll Follow */
$window.on('load', docReadyInit);

//** Window Resize Follow */
$window.resize(function () {
    docReadyInit();
});

//** Window Scroll Follow */
if (0) {
    $window.on('scroll', onScrollEvents);
}

// Global Functions //

function docReadyInit() {
    
    sizeObjects();
    imgsRatio();

    if (!tagsInited) {
        tagsInited = true;

        $('body').bfNoty({ 
            'title': 'We use cookies', 
            'message': 'Please, accept these sweeties to continue enjoying our site!', 
            'yes': 'Accept', 
            'no':'DECLINE'
        }, '', false);
        $('body').bfNoty({
            'title': 'Error',
            'message': 'Please, accept these sweeties to continue enjoying our site!',
            'yes': 'Accept',
            // 'no': 'DECLINE'
        }, 'err', false);
        $('body').bfNoty('info','info');
        $('body').bfNoty('warning','att');

        if ($('.alignleft').length || $('.alignright').length) {
            entry_aligns_popup();
        }
        if ($('.videopop').length) {
            init_video_popup();
        }
        if ($('.TabsHead').length) {
            init_tabs();
        }
        if ($('.reselbox').length) {
            initSelects();
        }
        if ($('.FAQsBlock').length) {
            init_faqs();
        }
        if ($('.labelsform').length) {
            init_labelsform();
        }
        if ($(":-webkit-autofill").length) {
            $(":-webkit-autofill").parents('.cfi').addClass('focus');
        }
        if ($('nav').length) {
            navAccessibility();
            if (smallScreen) {
                m_nav();
            }
        }
    }

}

function sizeObjects() {
    windowWidth = $window.width();
    windowHeight = $window.height();
    headerHeight = $header.outerHeight();
    if (isMobile || windowWidth < mobWidth) { smallScreen = true; }
}

function imgsRatio() {
    if ($('.resize').length) {
        $('.resize').each(function () {
            let rait, mob;
            if (isMobile) mob = $(this).attr('data-mratio');
            if (mob) rait = mob;
            else rait = $(this).attr('data-ratio') ?? '16:9';
            let ow = $(this).width();
            let tmp = rait.split(':');
            let w = tmp[0];
            let h = tmp[1];
            $(this).height((ow / w) * h).addClass('resized');
        });
    }
}

function log(mess, type) {
    switch (type) {
        case 'alert':
            alert(mess);
            break;

        case 'err' || 'error':
            console.error(mess);
            break;

        default:
            console.log(mess);
            break;
    }
}

function onScrollEvents() {
    let st = $window.scrollTop();

    if (st > 300) {
        $('#BackToTop').addClass('show');
        if (!$('header').hasClass('stiky')) {
            $('#HeadFix').height($('header').outerHeight());
        }
        $('header').addClass('stiky');
        if (st > lastScrollTop) {
            $('header').addClass('sdown');
            $('header').removeClass('sup');
        } else {
            $('header').removeClass('sdown');
            $('header').addClass('sup');
        }
    } else {
        $('#BackToTop').removeClass('show');
        $('header').removeClass('stiky');
        $('#HeadFix').height(0);
        $('header').removeClass('sup sdown');
    }
    
    lastScrollTop = st;
}

function navAccessibility() {
    $('li.menu-item-has-children').find('> a').attr('aria-expanded', 'false');
    $(document).on({
        focus: function () {
            $(this).find('> a').attr('aria-expanded', 'true');
        },
        focusout: function () {
            $(this).find('> a').attr('aria-expanded', 'false');
        },
        mouseenter: function () {
            $(this).find('> a').attr('aria-expanded', 'true');
        },
        mouseleave: function () {
            $(this).find('> a').attr('aria-expanded', 'false');
        }
    }, 'li.menu-item-has-children');
}

function init_video_popup() {
    var scriptEls = document.getElementsByTagName('script');
    var scriptPath = '';
    $.each(scriptEls, function () {
        var src = $(this).get(0).src;
        if (src.indexOf("scripts.js") >= 0 && src.indexOf("contact-form-7") < 0) {
            scriptPath = src;
            return false;
        }
    });
    var scriptFolder = scriptPath.substr(0, scriptPath.lastIndexOf('/') + 1);
    var vpscript = document.createElement("script");
    vpscript.type = 'text/javascript';
    vpscript.src = scriptFolder + 'jquery.dcd.video.min.js';

    var $style = $('<style type="text/css">.popupbox{position:fixed;top:0;right:0;bottom:0;left:0;width:100%;height:100%;background-color:rgba(0,0,0,.8);z-index:900;opacity:0;visibility:hidden;pointer-events:none;-webkit-transition:all .5s ease;transition:all .5s ease}.popupbox.active{opacity:1;visibility:visible;pointer-events:auto;-webkit-transition:opacity .4s;transition:opacity .4s}.popupbox .vbox{width:96%;max-height:96%;max-width:860px;height:auto;background-color:#252525;-webkit-transition:opacity .2s,height .5s;transition:opacity .2s,height .5s;transition-delay:0s}.popupbox.active .vbox{transition-delay:.5s}.popupbox:not(.active) .vbox{height:0!important}.popupbox .vbox #cPlayer,.popupbox .vbox #cPlayer iframe{width:100%!important;height:100%!important}.popupbox .vbox .cls{position:absolute;width:0;height:0;background-color:#fff;-webkit-transform:translate(50%,-50%) scale(1);transform:translate(50%,-50%) scale(1);-webkit-transition:all .3s ease;transition:all .3s ease;transition-delay:0s}.popupbox.active .vbox .cls{transition-delay:1s;width:30px;height:30px}.popupbox .vbox .cls svg{width:100%;height:100%;fill:#E2574C}.popupbox .vbox .cls:hover{background-color:#E2574C}.popupbox .vbox .cls:hover svg{fill:#fff}</style>');
    $style.appendTo('head');

    $('#Wrapper').after(vpscript);
    $('#Wrapper').after('<div id="cPlayBox" class="fcc popupbox"><div class="vbox relative" data-ratio="16:9"><div class="cls cp circlebox"><svg class="anim" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 286.054 286.054" style="enable-background:new 0 0 286.054 286.054;" xml:space="preserve"><g><path style="fill:#E2574C;" d="M168.352,142.924l25.28-25.28c3.495-3.504,3.495-9.154,0-12.64l-12.64-12.649c-3.495-3.486-9.145-3.495-12.64,0l-25.289,25.289l-25.271-25.271c-3.504-3.504-9.163-3.504-12.658-0.018l-12.64,12.649c-3.495,3.486-3.486,9.154,0.018,12.649l25.271,25.271L92.556,168.15c-3.495,3.495-3.495,9.145,0,12.64l12.64,12.649c3.495,3.486,9.145,3.495,12.64,0l25.226-25.226l25.405,25.414c3.504,3.504,9.163,3.504,12.658,0.009l12.64-12.64c3.495-3.495,3.486-9.154-0.009-12.658L168.352,142.924z M143.027,0.004C64.031,0.004,0,64.036,0,143.022c0,78.996,64.031,143.027,143.027,143.027s143.027-64.031,143.027-143.027C286.054,64.045,222.022,0.004,143.027,0.004z M143.027,259.232c-64.183,0-116.209-52.026-116.209-116.209s52.026-116.21,116.209-116.21s116.209,52.026,116.209,116.209S207.21,259.232,143.027,259.232z"></path></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div><div id="cPlayer" class="overh" data-type="" data-code=""></div></div></div>');

    var rait = $('#cPlayBox .vbox').attr('data-ratio');
    if (rait == null) rait = '16:9';
    var ow = $('#cPlayBox .vbox').width();
    var tmp = rait.split(':');
    var w = tmp[0];
    var h = tmp[1];
    $('#cPlayBox .vbox').height((ow / w) * h);

    $('.videopop').click(function () {
        $('#cPlayer').attr('data-type', $(this).attr('data-type'));
        $('#cPlayer').attr('data-code', $(this).attr('data-code'));
        setTimeout(function () {
            $('#cPlayer').video();
            $('#cPlayBox').addClass('active');
            $('body').addClass('overh');
        }, 500);
    });
    $('#cPlayBox .cls').click(function () {
        $('#cPlayBox').removeClass('active');
        $('body').removeClass('overh');
        setTimeout(function () {
            $('#cPlayer').remove();
        }, 500);
        $('#cPlayBox .vbox').append('<div id="cPlayer" class="overh" data-type="" data-code=""></div>');
    });
}

//** Click Events */

$('#BackToTop').click(function () {
    scrollTo(0);
});

$('.scrollTo').click(function () {
    let scrollTop = 0;
    let to = $.data($(this),'to');
    if ($.isNumeric(to)) {
        scrollTop = to;
    } else {
        scrollTop = $(to).offset().top??0;
    }
    scrollTo(scrollTop);
});

function scrollTo(scrollTop) {
    $('html, body').animate({
        scrollTop: scrollTop
    }, 1000);
}

$('.enterToClick,.spaceToClick').keyup(function (e) {
    let $this = $(this), key = e.which;
    if (key == keyEnter) $this.trigger('click');
    if (key == keySpace) $this.trigger('click');
});

$('.enterToClickOn').keyup(function (e) {
    let key = e.which;
    if (key == keyEnter) {
        let on = $(this).data('click-on');
        $(on).trigger('click');
    }
});

$('.textReadMore').click(function () {
    let $this = $(this),
        $morew = $this.parents('.morewrap'),
        $sibMorew = $morew.siblings();
    $sibBtn = $sibMorew.find('.textReadMore')
    $sibMorew.removeClass('expanded').find('.morebox').height(0);
    $sibBtn.attr('aria-expanded', 'false').find('span').html($sibBtn.data('rmore'));
    $morew.toggleClass('expanded');
    if ($morew.hasClass('expanded')) {
        $morew.find('.morebox').height($morew.find('.sizer').outerHeight());
        $this.attr('aria-expanded', 'true').find('span').html($this.data('rless'));
    } else {
        $morew.find('.morebox').height(0);
        $this.attr('aria-expanded', 'false').find('span').html($this.data('rmore'));
    }
});

$('#toggleDir').click(function() {
    let dir = $('html').attr('dir');
    if (dir === 'ltr') {
        dir = 'rtl';
    } else {
        dir = 'ltr';
    }
    $('html').attr('dir',dir);
});

//** MOBILE */

function detectMobile() {
    // Mobile Detect //
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)));

}

$('#MobNavBtn').click(function () {
    $('nav#nav').toggleClass('active');
    if ($('nav#nav').hasClass('active')) {
        $(this).addClass('active');
        $('body').addClass('overh');
    } else {
        $(this).removeClass('active');
        $('body').removeClass('overh');
    }
});


//** Make responsive Table tag  */
if (smallScreen && $('table').length) {
    (function () {
        let headertext = [];
        let headers = document.querySelectorAll("thead");
        let tablebody = document.querySelectorAll("tbody");

        for (let i = 0; i < headers.length; i++) {
            headertext[i] = [];
            for (let j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) {
                let current = headrow;
                headertext[i].push(current.textContent.replace(/\r?\n|\r/, ""));
            }
        }

        if (headers.length > 0) {
            for (let h = 0, tbody; tbody = tablebody[h]; h++) {
                for (let i = 0, row; row = tbody.rows[i]; i++) {
                    for (let j = 0, col; col = row.cells[j]; j++) {
                        col.setAttribute("data-th", headertext[h][j]);
                    }
                }
            }
        }
    }());
}

//** AJAX */
$('.ajaxLoadMore').click(function () {
    let clickedBtn = $(this),
        appendTo = $(this).parents('.ajaxPostsBox').find('.addHere'),
        pt = $(this).data('pt'),
        tax = $(this).data('tax'),
        step = parseInt($(this).attr('data-step')) + 1,
        total = $(this).data('total');

    clickedBtn.attr('disabled', 'disabled');

    $.ajax({
        type: "POST",
        url: window.wp_data.ajaxUrl,
        dataType: "json",
        data: {
            action: 'get_posts_ajax',
            step: step,
            tax: tax,
            pt: pt
        },
        //contentType: false,
        //processData: false,
        success: function (response) {
            //console.log(response);
            if (response.success) {
                if (appendTo.find('.addBefore').length) {
                    appendTo.find('.addBefore').before(response.html);
                } else {
                    appendTo.append(response.html);
                }
                if (appendTo.find('.ajaxEl').length != total) {
                    clickedBtn.removeAttr('disabled', 'disabled');
                }
            } else if (response.message) {
                $('body').mnError(response.message);
            }
        }
    });
    clickedBtn.attr('data-step', step);
});

$('.arrowsNav').keyup(function (e) {
    let el,
        prev = keyRight,
        next = keyLeft,
        $this = $(this), key = e.which;

    if (isRtl) {
        prev = keyLeft;
        next = keyRight;
    }

    if (key === keyEnter) { // Enter
        $this.trigger('click');
        return;
    } else if (key === keyEsc) { // Esc
        $('.esc.ae').attr('aria-expanded', false);
        $('.esc.h0').height(0);
        $('.esc.cls').removeClass('expanded active');
    } else if (key === next || key === keyDown) { // Left - Down
        el = $this.next();
    } else if (key === prev || key === keyUp) { // Right - Up
        el = $this.prev();
    }

    if ($this.hasClass('focusClick')) {
        $(el).focus().trigger('click');
    } else {
        $(el).focus();
    }

    return false;
});

//** wpbf HTML Tags */
function initSelects() {

    function dropSelect(el) {
        let $rbox = el.attr('aria-expanded', true).parents('.reselbox');
        $rbox.toggleClass('expanded');
        if ($rbox.hasClass('expanded')) {
            let slctd = $rbox.find('.selected');
            $rbox.find('.dropbox').height($rbox.find('.sizer').outerHeight());
            
            if (slctd.length) {
                setTimeout(function () {
                    slctd.focus();
                }, 200);
            } else {
                setTimeout(function () {
                    $rbox.find('.dropbox li').eq(0).focus();
                }, 200);
            }
        } else {
            closeSelect(el);
        }
    }

    function closeSelect(el) {
        if (el === undefined) {
            el = $('.reselbox.expanded').find('.iw input');
        }

        if (el.length) {
            el.attr('aria-expanded', false)
                .parents('.reselbox')
                .removeClass('expanded')
                .find('.dropbox')
                .height(0);
        }

        return false;
    }

    let iInput = $('.reselbox .iw input');
    iInput.click(function () {
        dropSelect($(this));
    });

    iInput.keyup(function (e) {
        let $this = $(this),
            key   = e.which,
            prev = keyRight,
            next = keyLeft,
            $rbox = $this.parents('.reselbox'),
            $sdli = $rbox.find('.dropbox li.selected');

        if (isRtl) {
            prev = keyLeft;
            next = keyRight;
        }    

        if (key === keyEnter || key === keySpace) { // Enter or Space
            dropSelect($this);
        } else if (key === prev || key === keyUp) { // Up
            if ($sdli.length) $sdli = $sdli.prev();
            else $sdli = $rbox.find('.dropbox li:last-child');
            $sdli.trigger('click');
        } else if (key === next || key === keyDown) { // Down
            if ($sdli.length) $sdli = $sdli.next();
            else $sdli = $sdli = $rbox.find('.dropbox li').eq(0);
            $sdli.trigger('click');
        }

        return false;
    });

    $('.reselbox .dropbox input').keyup(function () {
        let $this = $(this),
            val  = $this.val(),
            dbox = $this.parents('.dropbox');

        dbox.find('li.finded').removeClass('finded');
        if (val) {
            if (val.length >= 3) {
                dbox.addClass('searching');
                let result = dbox.find('li:contains(' + val + ')').addClass('finded');
                if (result.length) dbox.addClass('hfinded');
                else dbox.removeClass('hfinded');
            }
        } else {
            dbox.removeClass('searching hfinded');
        }
        dbox.height(dbox.find('.sizer').outerHeight());
    });

    $('.reselbox li').click(function () {
        let $this = $(this),
            multi = false,
            $select = $('#' + $this.data('select')),
            $box = $this.parents('.reselbox'),
            $input = $box.find('.iw input'),
            cls = '.' + $this.data('class');

        if ($select.attr('multiple')) {
            multi = true;
        }

        $this.toggleClass('selected');
        if (multi) {
            let inpVal = '';
            $select.find("option:selected").prop("selected", false);
            $box.find('li.selected').each(function (i) {
                let $ethis = $(this),
                    tcls = '.' + $ethis.data('class');
                $select.find(tcls).prop('selected', true);
                if (i) inpVal += ', ' + $ethis.text();
                else inpVal = $ethis.text();
            });
            $input.prop('value', inpVal);
            $select.change();
            $box.addClass('focus');
        } else {
            $this.siblings().removeClass('selected');
            if ($this.hasClass('selected')) {
                $select.find(cls).prop('selected', true).change();
                $input.prop('value', $this.text());
                $box.addClass('focus');
            } else {
                $select.find(cls).prop('selected', false).change();
                $input.prop('value', '');
                $box.removeClass('focus');
            }
            $box.removeClass('expanded').find('.dropbox').height(0);
            $input.focus();
        }
    });

    $(document).on('click', function (e) {
        let $resel = $(e.target).parents('.reselbox');
        if ($resel.length === 0) {
            closeSelect();
        }
    });
}

function entry_aligns_popup() {
    $('.alignleft,.alignright').parents('div,blockquote,pre').append('<div class="clear"></div>');
}

function init_tabs() {
    $('.TabsHolder').each(function () {
        let $this = $(this);
        $this.height($this.find('.tabbox.active').outerHeight());
    });

    $('.TabsHead button').click(function () {
        let $this = $(this),
            $tab = $('.tabbox.' + $this.attr('id')),
            $tabs_id = $this.data('tid'),
            $tabs_nav = $('.TabsNav.' + $tabs_id);

        $this.addClass('active').attr('aria-expanded', true).siblings().removeClass('active').attr('aria-expanded', false);
        $tab.addClass('active').siblings().removeClass('active');
        $tab.parents('.TabsHolder').height($tab.outerHeight());

        if (smallScreen) {
            let $th = $($this.parents('.TabsHead')),
                $thb = $($this.parents('.thbox')),
                toh = $this.outerHeight();
            if ($th.hasClass('expanded')) {
                $thb.prepend($this);
                $th.height(toh);
                $thb.height(toh);
                setTimeout(function () {
                    $th.removeClass('expanded');
                }, 600);
            } else {
                $th.height(toh);
                $thb.height(toh);
                $th.addClass('expanded');
                let dh = 0;
                $thb.find('button').each(function () {
                    dh += $(this).outerHeight();
                });
                $thb.height(dh);
            }
            if ($tabs_nav.length) {
                $tabs_nav.find('.disabled').removeClass('disabled');
            }
        } else {
            if ($tabs_nav.length) {
                if ($this.next().length) {
                    $tabs_nav.find('.next').removeClass('disabled');
                } else {
                    $tabs_nav.find('.next').addClass('disabled');
                }
                if ($this.prev().length) {
                    $tabs_nav.find('.prev').removeClass('disabled');
                } else {
                    $tabs_nav.find('.prev').addClass('disabled');
                }
            }
        }
    });

    $('.TabsNav button').click(function () {
        let $this = $(this);
            id = $this.data('tid');
            $tabs = $('.TabsHead.' + id + ' .thbox');
            $current = $($tabs.find('button.active'));
            $tabs_nav = $('.TabsNav.' + id);

        if (smallScreen) {
            $tabs_nav.find('.disabled').removeClass('disabled');
        }
        let $next,$prev;
        if ($this.hasClass('next')) {
            if (smallScreen) {
                $next = $tabs.find('button').last();
                if ($next) {
                    $next.addClass('active').siblings().removeClass('active');
                    $tabs.prepend($next);
                }
            } else {
                $next = $($current.next());
                if ($next) {
                    $next.trigger('click');
                }
            }
        } else {
            if (smallScreen) {
                $prev = $tabs.find('button').eq(2);
                if ($prev) {
                    $prev.addClass('active').siblings().removeClass('active');
                    $tabs.prepend($prev);
                }
            } else {
                $prev = $($current.prev());
                if ($prev) {
                    $prev.trigger('click');
                }
            }
        }
    });
}

function init_faqs() {
    $('.FAQsBlock h3').click(function () {
        let $this = $(this),
            $li = $this.parents('li');
            $sibli = $li.siblings();
        $li.toggleClass('active');
        if ($li.hasClass('active')) {
            $this.attr('aria-expanded', 'true');
            $li.find('.answrap').height($li.find('.answrap .sizer').outerHeight());
        } else {
            $this.attr('aria-expanded', 'false');
            $li.find('.answrap').height(0);
        }
        $sibli.removeClass('active').find('.answrap').height(0);
        $sibli.find('> h3').attr('aria-expanded', 'false');
    });

    $('.FAQsBlock li').keyup(function (e) {
        if (e.which === keyEnter || e.which === keySpace) {
            $(this).find('h3').trigger('click');
        }
    });
}

$('.fileupbox input[type=file]').change(function () {
    let $this = $(this), 
        fileName = '';
    if ($this.attr('multiple')) {
        let files = $this[0].files;
        for (let i = 0; i < files.length; i++) {
            if (i) {
                fileName += ', ';
            }
            fileName += files[i].name;
        }
    } else {
        fileName = $this.val().split('\\').pop();
    }
    $this.parents('.fileupbox').find('input[type=text]').val(fileName);
});


//** WPCF 7 + Forms */

function init_labelsform() {
    $(document).on('focus click', '.labelsform input, .labelsform textarea', function (e) {
        formsFocus(e);
    });

    $(document).on('focusout', '.labelsform input, .labelsform textarea', function (e) {
        if (e.target.value === '') {
            formsFocus(e, 'rm');
        }
    });
}

function formsFocus(el, event) {
    let parent, $this = $(el.target);
    if (el.target.type == 'textarea') parent = $this.parents('.cft');
    else parent = $this.parents('.cfi');
    
    if (event === 'rm') parent.removeClass('focus');
    else parent.addClass('focus');
}

$(document).on('wpcf7:mailsent', function (e) {
    wpcf7Noty(e);
});

$(document).on('wpcf7:invalid', function (e) {
    wpcf7Noty(e, false);
});

function wpcf7Noty(formWrap, success) {
    var mess = '';
    var $form = $(formWrap.target).find('form');
    var $submit = $form.find('input[type="submit"]');
    $submit.prop('disabled', true);
    if (success != false) {
        success = true;
    }
    setTimeout(function () {
        mess = $('div.wpcf7-response-output').text();
        if (mess) {
            if (success) {
                $('body').mnSuccess(mess);
                $form.get(0).reset();
            } else {
                $('body').mnError(mess);
            }
        }
        setTimeout(function () {
            $submit.prop('disabled', false);
        }, 1500);
    }, 200);
}

function hexToRGBA(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }
}

function uID(pre) {
    pre = pre ?? 'wpbf-ju-id-';
    return pre + Math.floor(Math.random() * 33) + Date.now();
}

$(document).on('click','.setCookie',function(){
    let $this = $(this);
        cname = $this.data('coo-name'),
        value = $this.data('coo-value');

    if (cname != undefined && value != undefined) {
        setCookie(cname, value);
    }
});

function setCookie(cname, value) {
    localStorage.setItem(cname, value);
}

function getCookie(cname) {
    return localStorage.getItem(cname);
}