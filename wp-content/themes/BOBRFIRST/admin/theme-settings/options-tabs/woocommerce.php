<div class="wpbfTab woocommerce">
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e('Mini cart'); ?></h3>
            <p><?php _e('Show the cart menu of Woocommerce shop on main header.'); ?></p>
        </div>
        <div class="fg1 fb0 flex jcfe">
            <div class="input-holder">
                <input id="mini-cart" name="mini-cart" class="onoff" type="checkbox" <?php get_bf_setting('menu-shop'); ?>>
                <label for="mini-cart" class="onoff">Toggle</label>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e('Shop sidebar'); ?></h3>
            <p><?php _e('Select the sidebar for shop archive page, set the sidebar contents on Apparence > Widgets'); ?></p>
        </div>
        <div class="fg1 fb0 flex jcfe">
            <div class="input-holder">
                <?php $tmp = get_bf_setting('woocommerce-sidebar-shop'); ?>
                <select id="woocommerce-sidebar-shop" name="woocommerce-sidebar-shop">
                    <option <?= ($tmp == '') ? 'selected':''; ?> value="">None</option>
                    <option <?= ($tmp == 'right') ? 'selected':''; ?> value="right">Right</option>
                    <option <?= ($tmp == 'left') ? 'selected':''; ?> value="left">Left</option>
                    <option <?= ($tmp == 'right-left') ? 'selected':''; ?> value="right-left">Right and left</option>
                </select>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e('Product sidebar'); ?></h3>
            <p><?php _e('Select the sidebar for single product item of the shop, set the sidebar contents on Apparence > Widgets'); ?></p>
        </div>
        <div class="fg1 fb0 flex jcfe">
            <div class="input-holder">
                <?php $tmp = get_bf_setting('woocommerce-sidebar-single'); ?>
                <select id="woocommerce-sidebar-single" name="woocommerce-sidebar-single">
                    <option <?= ($tmp == '') ? 'selected':''; ?> value="">None</option>
                    <option <?= ($tmp == 'right') ? 'selected':''; ?> value="right">Right</option>
                    <option <?= ($tmp == 'left') ? 'selected':''; ?> value="left">Left</option>
                    <option <?= ($tmp == 'right-left') ? 'selected':''; ?> value="right-left">Right and left</option>
                </select>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e('Shop page'); ?></h3>
            <p><?php _e('Select the main shop page if it\'s different from the default Woocommerce shop archive page.'); ?></p>
        </div>
        <div class="fg1 fb0 flex jcfe">
            <div class="input-holder">
                <?php $tmp = get_bf_setting('shop-page');
                $tmp_2 = get_pages();
                $html = ""; ?>
                <select id="shop-page" name="shop-page">
                    <option value="">--</option>
                    <?php for ($i = 0; $i < count($tmp_2); $i++) {
                        $html .= '<option value="' . esc_attr($tmp_2[$i]->ID) . '" '.  esc_attr((($tmp==$tmp_2[$i]->ID) ? 'selected':'')) . '>' . esc_attr($tmp_2[$i]->post_title) . '</option>';
                    } echo $html; ?>
                </select>
            </div>
        </div>
    </div>
</div>