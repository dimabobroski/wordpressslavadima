<div class="wpbfTab customizations">
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e('Head Code'); ?></h3>
            <p><?php _e('Code before close head tag.'); ?></p>
        </div>
        <div class="pBody">
            <div class="input-holder imgfix mt15">
                <textarea id="head-code" name="head-code" style="height:calc(100px + 5vw);max-height:50vh;"><?= get_bf_setting('head-code') ?></textarea>
            </div>
        </div>
    </div>
    <hr />
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e('Body Code'); ?></h3>
            <p><?php _e('Code after open body tag.'); ?></p>
        </div>
        <div class="pBody">
            <div class="input-holder imgfix mt15">
                <textarea id="body-code" name="body-code" style="height:calc(100px + 5vw);max-height:50vh;"><?= get_bf_setting('body-code') ?></textarea>
            </div>
        </div>
    </div>
    <hr />
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e('Javascript global'); ?></h3>
            <p><?php _e('Javascript block loaded in every page. Insert the code without <script></script> tags. Use \' for text values.'); ?></p>
        </div>
        <div class="pBody">
            <div class="input-holder imgfix mt15">
                <textarea id="js-global" name="global-js" style="height:calc(100px + 5vw);max-height:50vh;"><?= get_bf_setting('global-js') ?></textarea>
            </div>
        </div>
    </div>
    <hr />
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e('CSS global'); ?></h3>
            <p><?php _e('CSS block loaded in every page.'); ?></p>
        </div>
        <div class="pBody">
            <div class="input-holder imgfix mt15">
                <textarea id="css-global" name="global-css" style="height:calc(100px + 5vw);max-height:50vh;"><?= get_bf_setting('global-css') ?></textarea>
            </div>
        </div>
    </div>
</div>