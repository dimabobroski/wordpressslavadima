<div class="wpbfTab socials-api">
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e("Social channels") ?></h3>
            <p><?php _e("Insert the links to your social channels account and pages.") ?></p>
        </div>
        <div class="pBody">
            <div class="social-admin inrowf">
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Facebook</p>
                    <input id="social-fb" name="social-fb" type="text" value="<?= get_bf_setting('social-fb'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Twitter</p>
                    <input id="social-tw" name="social-tw" type="text" value="<?= get_bf_setting('social-tw'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Youtube</p>
                    <input id="social-yt" name="social-yt" type="text" value="<?= get_bf_setting('social-yt'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Instagram</p>
                    <input id="social-ig" name="social-ig" type="text" value="<?= get_bf_setting('social-ig'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Pinterest</p>
                    <input id="social-pi" name="social-pi" type="text" value="<?= get_bf_setting('social-pi'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Linked-in</p>
                    <input id="social-in" name="social-in" type="text" value="<?= get_bf_setting('social-in'); ?>" />
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e("SMTP Email") ?></h3>
            <p><?php _e("Insert the SMTP informations to allow the contact form component to send messages with your server. The sender email should use the same @domain of the server.") ?></p>
        </div>
        <div class="pBody">
            <div class="social-admin inrowf">
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Host</p>
                    <input id="smtp-host" name="smtp-host" type="text" value="<?= get_bf_setting('smtp-host'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Port</p>
                    <input id="smtp-port" name="smtp-port" type="text" placeholder="465" value="<?= get_bf_setting('smtp-port'); ?>" />
                </div>
                <div class="input-text input-row full-input col3s30 smmw100 mt10">
                    <p>Email from</p>
                    <input id="smtp-email" name="smtp-email" type="text" value="<?= get_bf_setting('smtp-email'); ?>" />
                </div>
                <div class="input-text input-row full-input col2s30 smmw100 mt10">
                    <p>Username</p>
                    <input id="smtp-username" name="smtp-username" type="text" value="<?= get_bf_setting('smtp-username'); ?>" />
                </div>
                <div class="input-text input-row full-input col2s30 smmw100 mt10">
                    <p>Password</p>
                    <input id="smtp-psw" name="smtp-psw" type="password" value="<?= get_bf_setting('smtp-psw'); ?>" />
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="pTab inrowf aic">
        <div class="pHead col2s30 smw100">
            <h3><?php _e("Google maps key") ?></h3>
            <p><?php _e("Insert the Google maps API key, you can get a new key from developers.google.com/maps.") ?></p>
        </div>
        <div class="pBody col2s30 smw100">
            <div class="input-text input-row full-input">
                <input id="google-api-key" name="google-api-key" type="text" value="<?= get_bf_setting('google-api-key') ?>" />
            </div>
        </div>
    </div>
</div>