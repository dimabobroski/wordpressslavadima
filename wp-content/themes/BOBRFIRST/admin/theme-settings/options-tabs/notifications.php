<?php global $WPG; $dir_img = $WPG['dir_root'].'admin/images/notifications/'; ?>
<div class="wpbfTab notifications">
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e("Notifications position") ?></h3>
            <p><?php _e("Choose the site notifications position.") ?></p>
        </div>
        <div class="pBody inrowf">
            <?php $tmp = get_bf_setting('noty-position'); ?>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'top-left'; ?>
                <input id="<?= $val; ?>" type="radio" name="noty-position" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'top-left.jpg'; ?>" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'top-center'; ?>
                <input id="noty-p-<?= $val; ?>" type="radio" name="noty-position" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-p-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'top-center.jpg'; ?>" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'top-right'; ?>
                <input id="noty-p-<?= $val; ?>" type="radio" name="noty-position" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-p-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'top-right.jpg'; ?>" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'bottom-left'; ?>
                <input id="<?= $val; ?>" type="radio" name="noty-position" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'bottom-left.jpg'; ?>" class="noactive" alt="" />
                    <img src="<?= $dir_img.'bottom-right.jpg'; ?>" class="active" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'bottom-center'; ?>
                <input id="noty-p-<?= $val; ?>" type="radio" name="noty-position" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-p-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'bottom-center.jpg'; ?>" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'bottom-right'; ?>
                <input id="noty-p-<?= $val; ?>" type="radio" name="noty-position" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-p-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'bottom-right.jpg'; ?>" alt="" />
                </label>
            </div>
        </div>
    </div>
    <hr />
    <div class="pFields flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Side Animation") ?></h3>
            <p><?php _e("Set the slide animation from a side of a screen. (doesn't work with the center positions)") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-row">
                <?php $tmp = get_bf_setting('noty-from-side') ?>
                <input id="noty-from-side" name="noty-from-side" class="onoff" type="checkbox" value="<?= !empty($tmp)?$tmp:0; ?>"<?= ($tmp)?' checked':''; ?>>
                <label for="noty-from-side" class="onoff">Toggle</label>
            </div>
        </div>
    </div>
    <hr />
    <div class="pTab">
        <div class="pHead">
            <h3><?php _e("Notifications Style") ?></h3>
            <p><?php _e("Choose the site notifications style.") ?></p>
        </div>
        <div class="pBody inrowf">
            <?php $tmp = get_bf_setting('noty-style'); ?>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'style-1'; ?>
                <input id="noty-s-<?= $val; ?>" type="radio" name="noty-style" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-s-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'style-1.jpg'; ?>" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'style-2'; ?>
                <input id="noty-s-<?= $val; ?>" type="radio" name="noty-style" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-s-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'style-2.jpg'; ?>" alt="" />
                </label>
            </div>
            <div class="pFields fcc col3s30 smw100 mt10">
                <?php $val = 'style-3'; ?>
                <input id="noty-s-<?= $val; ?>" type="radio" name="noty-style" class="radio-img" value="<?= $val; ?>"<?= ($tmp == $val)?' checked':''; ?>/>
                <label for="noty-s-<?= $val; ?>" class="radio-img">
                    <img src="<?= $dir_img.'style-3.jpg'; ?>" alt="" />
                </label>
            </div>
        </div>
    </div>
</div>