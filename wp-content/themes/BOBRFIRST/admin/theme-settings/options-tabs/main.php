<div class="wpbfTab main active">
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Logo") ?></h3>
            <p><?php _e("Select an image file for your main logo.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="upload-box">
                <span class="close-button"></span>
                <?php $tmp = get_bf_setting('logo'); ?>
                <div class="upload-container">
                    <input type="hidden" name="logo" value="<?= esc_attr('https://framework.giga-art.com/wp-content/uploads/2021/11/wallpaperflare.com_wallpaper.jpg'); ?>">
                    <button class="upload-btn fcc<?php if (strlen($tmp) > 0) echo ' setted'; ?>">
                        <img src="<?php if (strlen($tmp) > 0) echo esc_attr($tmp); ?>" alt=""/>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Secondary logo") ?></h3>
            <p><?php _e("Secondary logo is used on fixed menu, transparent menu and middle logo menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="upload-box upload-row">
                <span class="close-button"></span>
                <?php $tmp = get_bf_setting('logo-2') ?>
                <div class="upload-container">
                    <div id="logo-2" class="upload-btn <?php if (strlen($tmp) > 0) echo "setted"?>" data-save="upload" style='<?php if (strlen($tmp) > 0) echo "background-image:url(" . esc_attr($tmp) . ")"; ?>'></div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Retina logo") ?></h3>
            <p><?php _e("Retina logo is showed only on high resolution displays.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="upload-box upload-row">
                <span class="close-button"></span>
                <?php $tmp = get_bf_setting('logo-retina') ?>
                <div class="upload-container">
                    <div id="logo-retina" class="upload-btn <?php if (strlen($tmp) > 0) echo "setted"?>" data-save="upload" style='<?php if (strlen($tmp) > 0) echo "background-image:url(" . esc_attr($tmp) . ")"; ?>'></div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Secondary retina logo") ?></h3>
            <p><?php _e("Retina logo is showed only on high resolution displays.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="upload-box upload-row">
                <span class="close-button"></span>
                <?php $tmp = get_bf_setting('logo-2-retina') ?>
                <div class="upload-container">
                    <div id="logo-2-retina" class="upload-btn <?php if (strlen($tmp) > 0) echo "setted"?>" data-save="upload" style='<?php if (strlen($tmp) > 0) echo "background-image:url(" . esc_attr($tmp) . ")"; ?>'></div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Favicon") ?></h3>
            <p><?php _e("Favicon top browser tab at 16px x 16px.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="upload-box upload-row">
                <span class="close-button"></span>
                <?php $tmp = get_bf_setting('favicon') ?>
                <div class="upload-container">
                    <div id="favicon" class="upload-btn <?php if (strlen($tmp) > 0) echo "setted"?>" data-save="upload" style='<?php if (strlen($tmp) > 0) echo "background-image:url(" . esc_attr($tmp) . ");"?> background-size: initial;'></div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <?php if (0) { ?>
    
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Font family") ?></h3>
                <p>
                    <?php _e("Select the main font family for your website. The fonts are provided by Google Fonts, you can preview the fonts from ") ?>
                    <a href="https://fonts.google.com/" target="_blank">fonts.google.com</a>
                </p>
            </div>
            <div class="fg1 fb0 flex jcfe">
                <div class="input-font-cnt">
                    <div class="fg1 fg0 flex jcfe">
                        <div class="input-text w100 input-row input-font">
                            <input id="font-family" data-save="text" class="font-family-value" type="text" value="<?= get_bf_setting('font-family') ?>" />
                            <input class="google-font-selector" type="text" />
                        </div>
                    </div>
                    <div class="chkbx inrowf">
                        <div class="input-row flex aic jcsb col3s30 mt15"">
                            <p>Thin</p>
                            <input id="100" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15"">
                            <p>Light</p>
                            <input id="300" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15"">
                            <p>Normal</p>
                            <input id="400" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15"">
                            <p>Semi Bold</p>
                            <input id="500" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15"">
                            <p>Bold</p>
                            <input id="600" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15"">
                            <p>Black</p>
                            <input id="900" type="checkbox">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Font family 2") ?></h3>
                <p>
                    <?php _e("Select the secondary font family for your website.") ?>
                </p>
            </div>
            <div class="fg1 fb0 flex jcfe">
                <div class="input-font-cnt">
                    <div class="fg1 fg0 flex jcfe">
                        <div class="input-text w100 input-row input-font">
                            <input id="font-family-2" data-save="text" class="font-family-value" type="text" value="<?= get_bf_setting('font-family-2') ?>" />
                            <input class="google-font-selector" type="text" />
                        </div>
                    </div>
                    <div class="chkbx inrowf">
                        <div class="input-row flex aic jcsb col3s30 mt15">
                            <p>Thin</p>
                            <input id="100" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15">
                            <p>Light</p>
                            <input id="300" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15">
                            <p>Normal</p>
                            <input id="400" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15">
                            <p>Semi Bold</p>
                            <input id="500" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15">
                            <p>Bold</p>
                            <input id="600" type="checkbox">
                        </div>
                        <div class="input-row flex aic jcsb col3s30 mt15">
                            <p>Black</p>
                            <input id="900" type="checkbox">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Icon family") ?></h3>
                <p><?php _e("Select the icon family for your website, available on Hybrid Composer components.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-select input-row">
                    <select id="icons-family" data-save="select">
                        <?php
                        $tmp = get_bf_setting('icons-family');
                        if (strlen($tmp) == 0) $tmp = "";
                        ?>
                        <option <?php if ($tmp == "font-awesome" || $tmp == "") echo "selected"; ?> value="font-awesome">Font Awesome</option>
                        <option <?php if ($tmp == "icons-mind-line") echo "selected"; ?> value="icons-mind-line">Icons Mind Line</option>
                        <option <?php if ($tmp == "icons-mind-solid") echo "selected"; ?> value="icons-mind-solid">Icons Mind Solid</option>
                    </select>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Main color") ?></h3>
                <p><?php _e("Main color of your website.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-color input-row">
                    <?php $tmp = get_bf_setting('main-color') ?>
                    <input id="main-color" type="text" data-save="color" value="<?php echo esc_attr($tmp); ?>" />
                    <span class="color-preview" style='<?php if (strlen($tmp) > 0) echo "background-color:" . esc_attr($tmp); ?>'></span>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Hover color") ?></h3>
                <p><?php _e("Hover color of your website.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-color input-row">
                    <?php $tmp = get_bf_setting('hover-color') ?>
                    <input id="hover-color" type="text" data-save="color" value="<?php echo esc_attr($tmp); ?>" />
                    <span class="color-preview" style='<?php if (strlen($tmp) > 0) echo "background-color:" . esc_attr($tmp); ?>'></span>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Color") ?> 3</h3>
                <p><?php _e("Additional color, used in some skin.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-color input-row">
                    <?php $tmp = get_bf_setting('color-3') ?>
                    <input id="color-3" type="text" data-save="color" value="<?php echo esc_attr($tmp); ?>" />
                    <span class="color-preview" style='<?php if (strlen($tmp) > 0) echo "background-color:" . esc_attr($tmp); ?>'></span>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Color") ?> 4</h3>
                <p><?php _e("Additional color, used in some skin.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-color input-row">
                    <?php $tmp = get_bf_setting('color-4') ?>
                    <input id="color-4" type="text" data-save="color" value="<?php echo esc_attr($tmp); ?>" />
                    <span class="color-preview" style='<?php if (strlen($tmp) > 0) echo "background-color:" . esc_attr($tmp); ?>'></span>
                </div>
            </div>
        </div>
        <hr />
    <?php } ?>
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Disable Gutenberg") ?></h3>
            <p><?php _e("Disable the Gutenberg page builder and restore the classic editor.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-row">
                <?php $tmp = get_bf_setting('disable-gutenberg') ?>
                <input id="disable-gutenberg" name="disable-gutenberg" class="onoff" type="checkbox" value="<?= !empty($tmp)?$tmp:0; ?>"<?= ($tmp)?' checked':''; ?>>
                <label for="disable-gutenberg" class="onoff">Toggle</label>
            </div>
        </div>
    </div>
</div>