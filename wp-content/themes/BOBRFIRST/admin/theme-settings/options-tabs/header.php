<div class="wpbfTab header">
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Menu type") ?></h3>
            <p><?php _e("Select the menu type.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-select input-row">
                <?php
                $tmp = get_bf_setting('menu-type');
                if (strlen($tmp) == 0) $tmp = "classic";
                ?>
                <select id="menu-type" data-save="select">
                    <option <?php if ($tmp == "classic") echo "selected"; ?> value="classic">Classic</option>
                    <option <?php if ($tmp == "big-logo") echo "selected"; ?> value="big-logo">Big logo</option>
                    <option <?php if ($tmp == "subline") echo "selected"; ?> value="subline">Subline</option>
                    <option <?php if ($tmp == "subtitle") echo "selected"; ?> value="subtitle">Subtitle</option>
                    <option <?php if ($tmp == "middle-logo") echo "selected"; ?> value="middle-logo">Middle logo</option>
                    <option <?php if ($tmp == "middle-box") echo "selected"; ?> value="middle-box">Middle box</option>
                    <option <?php if ($tmp == "icons") echo "selected"; ?> value="icons">Icons</option>
                    <option <?php if ($tmp == "side") echo "selected"; ?> value="side">Side menu</option>
                </select>
            </div>
            <img id="menu-type-preview" src="<?php //echo esc_url(HC_PLUGIN_URL) . '/admin/images/menu/menu-'. esc_attr($tmp) . '.png'; ?>" />
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Menu animation") ?></h3>
            <p><?php _e("Animation on hover of menu dropdown items.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-select input-row">
                <?php
                $tmp = get_bf_setting('menu-animation');
                if (strlen($tmp) == 0) $tmp = "";
                ?>
                <select id="menu-animation" data-save="select">
                    <option <?php if ($tmp == "") echo "selected"; ?> value="">None</option>
                    <option <?php if ($tmp == "fade-in") echo "selected"; ?> value="fade-in">Fade in</option>
                    <option <?php if ($tmp == "fade-top") echo "selected"; ?> value="fade-top">Fade top</option>
                    <option <?php if ($tmp == "fade-bottom") echo "selected"; ?> value="fade-bottom">Fade bottom</option>
                    <option <?php if ($tmp == "fade-left") echo "selected"; ?> value="fade-left">Fade left</option>
                    <option <?php if ($tmp == "fade-right") echo "selected"; ?> value="fade-right">Fade right</option>
                    <option <?php if ($tmp == "show-scale") echo "selected"; ?> value="show-scale">Show scale</option>
                    <option <?php if ($tmp == "pulse") echo "selected"; ?> value="pulse">Pulse</option>
                    <option <?php if ($tmp == "pulse-fast") echo "selected"; ?> value="pulse-fast">Pulse fast</option>
                    <option <?php if ($tmp == "pulse-horizontal") echo "selected"; ?> value="pulse-horizontal">Pulse horizontal</option>
                    <option <?php if ($tmp == "pulse-vertical") echo "selected"; ?> value="pulse-vertical">Pulse vertical</option>
                    <option <?php if ($tmp == "slide-right-left") echo "selected"; ?> value="slide-right-left">Slide right from left</option>
                    <option <?php if ($tmp == "slide-top-bottom") echo "selected"; ?> value="slide-top-bottom">Slide bottom from top</option>
                </select>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Transparent menu") ?></h3>
            <p><?php _e("Set transparent style for your menu.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-transparent" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-transparent'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Hamburger menu") ?></h3>
            <p><?php _e("Hamburger menu button for side menus. *Only for side menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-hamburger" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-hamburger'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Lateral dropdown") ?></h3>
            <p><?php _e("Lateral dropdown layout for side menus. *Only for side menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-lateral" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-lateral'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Wide menu") ?></h3>
            <p><?php _e("Full width menu layout. *Only for horizontal menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-wide-area" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-wide-area'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("One page") ?></h3>
            <p><?php _e("Activate it for one page and fullPage websites.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-one-page" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-one-page'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Fixed top") ?></h3>
            <p><?php _e("Menu is always visible on top. *Only for horizontal menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-fixed" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-fixed'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Search box") ?></h3>
            <p><?php _e("Show search box on right side of menu.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-search" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-search'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Search button") ?></h3>
            <p><?php _e("Show a search button on right side of menu.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-search-button" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-search-button'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Social icons") ?></h3>
            <p><?php _e("Show the social icons, you can set the icons on Social and API tab.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-social" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-social'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Centered") ?></h3>
            <p><?php _e("Set center menu.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-center" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-center'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Full width mega menu") ?></h3>
            <p><?php _e("Set full width sizes for the dropdown mega menu. *Only for horizontal menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-mega-full-width" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-mega-full-width'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Columns top padding") ?></h3>
            <p><?php _e("Top padding of the columns of the mega menu. *Only for mega menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-text input-row small-input">
                <input id="menu-mega-padding" type="text" data-save="text" data-mask="number" placeholder="px" value="<?php get_bf_setting('menu-mega-padding'); ?>" />
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("WPML Lan menu") ?></h3>
            <p><?php _e("Show the WPML language selector on the menu. You musy install and activate the WPML plugin.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-language" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-language'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Menu position") ?></h3>
            <p><?php _e("Menu position of horizontal menus. *Only for horizontal menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-select input-row">
                <?php
                $tmp = get_bf_setting('menu-position');
                if (strlen($tmp) == 0) $tmp = "left";
                ?>
                <select id="menu-position" data-save="select">
                    <option <?php if ($tmp == "left") echo "selected"; ?> value="left"><?php _e("Left") ?></option>
                    <option <?php if ($tmp == "right") echo "selected"; ?> value="right"><?php _e("Right") ?></option>
                </select>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Top logo") ?></h3>
            <p><?php _e("Show the logo above the menu, on top of the page. *Only for middle logo menu.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-top-logo" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-logo'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Top icons") ?></h3>
            <p><?php _e("Change the icons position of icons menu from left to top. *Only for icons menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-top-icons" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-icons'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Custom area content") ?></h3>
            <p><?php _e("Insert every HTML you want, you can copy the code blocks of framework-y.com and paste theme here.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-text-area full-input" spellcheck="false">
                <textarea id="menu-custom-area" data-save="text"><?php get_bf_setting('menu-custom-area') ?></textarea>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Middle box content") ?></h3>
            <p><?php _e("Insert every HTML you want, you can copy the code blocks of framework-y.com and paste theme here. *Only for middle box menus") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-text-area full-input" spellcheck="false">
                <textarea id="menu-middle-box" data-save="text"><?php get_bf_setting('menu-middle-box') ?></textarea>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <p class="item-title"><?php _e("Menu design settings") ?></p>
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Logo height") ?></h3>
            <p><?php _e("Set the main logo height of menu. In pixels.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-text input-row small-input">
                <input id="logo-height" type="text" data-save="text" placeholder="px" data-mask="number" value="<?php get_bf_setting('logo-height'); ?>" />
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Menu height") ?></h3>
            <p><?php _e("Set the menu height. *Only for horizontal menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-text input-row small-input">
                <input id="menu-height" type="text" data-save="text" placeholder="px" data-mask="number" value="<?php get_bf_setting('menu-height'); ?>" />
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Menu margin left") ?></h3>
            <p><?php _e("Menu margin left, use this option for centered logo menus.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-text input-row small-input">
                <input id="menu-margin-left" type="text" data-save="text" placeholder="px" data-mask="number" value="<?php get_bf_setting('menu-margin-left'); ?>" />
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <p class="item-title"><?php _e("Top mini menu") ?></p>
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Activate top menu") ?></h3>
            <p><?php _e("If activate the main menu show a secondary menu bar on top.") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="menu-top-area" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-area'); ?>>
            </div>
        </div>
    </div>
    <div id="top-area-options">
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Hide on scroll") ?></h3>
                <p><?php _e("Hide the top menu on page scroll.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-checkbox input-row">
                    <input id="menu-top-scroll-hide" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-scroll-hide'); ?>>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Search box") ?></h3>
                <p><?php _e("Show the search box on right.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-checkbox input-row">
                    <input id="menu-top-search" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-search'); ?>>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Social icons") ?></h3>
                <p><?php _e("Show the social icons, you can set the icons on Social and API tab.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-checkbox input-row">
                    <input id="menu-top-social" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-social'); ?>>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("WPML language menu") ?></h3>
                <p><?php _e("Show the WPML language selector on the menu. You musy install and activate the WPML plugin..") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-checkbox input-row">
                    <input id="menu-top-language" data-save="checkbox" type="checkbox" <?php get_bf_setting('menu-top-language'); ?>>
                </div>
            </div>
        </div>
        <hr />
        <div class="flex aic jcsb">
            <div class="w50 ml20">
                <h3><?php _e("Custom content") ?></h3>
                <p><?php _e("Custom content of the top menu. Insert every HTML you want, you can copy the code blocks of framework-y.com and paste theme here.") ?></p>
            </div>
            <div class="fg1 fg0 flex jcfe">
                <div class="input-text-area full-input" spellcheck="false">
                    <textarea id="menu-custom-top-area" placeholder="" data-save="text"><?php get_bf_setting('menu-custom-top-area') ?></textarea>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>