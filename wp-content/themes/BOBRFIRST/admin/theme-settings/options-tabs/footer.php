<div class="wpbfTab footer">
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Footer type","hc") ?></h3>
            <p><?php _e("Choose the footer type.","hc") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-select input-row">
                <?php
                $tmp = get_bf_setting('footer-type');
                if (strlen($tmp) == 0) $tmp = "base";
                ?>
                <select id="footer-type" data-save="select">
                    <option <?php if ($tmp == "base") echo "selected"; ?> value="base">Base</option>
                    <option <?php if ($tmp == "minimal") echo "selected"; ?> value="minimal">Minimal</option>
                    <option <?php if ($tmp == "parallax") echo "selected"; ?> value="parallax">Parallax</option>
                </select>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Social icons","hc") ?></h3>
            <p><?php _e("Show the social icons on the footer, you set the icons on Social and API tab.","hc") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="footer-social" data-save="checkbox" type="checkbox" <?php get_bf_setting('footer-social'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Wide","hc") ?></h3>
            <p><?php _e("Set full width footer.","hc") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-checkbox input-row">
                <input id="footer-wide" data-save="checkbox" type="checkbox" <?php get_bf_setting('footer-wide'); ?>>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Scroll top button","hc") ?></h3>
            <p><?php _e("Show the scroll to top button on bottom right position.","hc") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <div class="input-select input-row">
                <?php
                $tmp = get_bf_setting('scroll-top-btn');
                ?>
                <select id="scroll-top-btn" data-save="select">
                    <option <?php if ($tmp == "") echo "selected"; ?> value="">None</option>
                    <option <?php if ($tmp == "mobile") echo "selected"; ?> value="mobile">Mobile</option>
                    <option <?php if ($tmp == "always") echo "selected"; ?> value="always">Always</option>
                </select>
            </div>
        </div>
    </div>
    <hr />
    <div class="flex aic jcsb">
        <div class="w50 ml20">
            <h3><?php _e("Copyright text","hc") ?></h3>
            <p><?php _e("This text is showed as last text of the footer and is usually used for copyright texts.","hc") ?></p>
        </div>
        <div class="fg1 fg0 flex jcfe">
            <?php
            if (function_exists('icl_object_id')) {
                foreach ($langs as $item) {
                    $lan_code = $item['language_code'];
            ?>
            <div class="input-text input-row full-input">
                <input id="footer-copyright-<?php echo $lan_code; ?>" type="text" placeholder="<?php echo strtoupper($lan_code); ?> langauge" data-save="text" value="<?php get_bf_setting('footer-copyright-' . $lan_code); ?>" />
            </div>
            <?php }
            } else {
            ?>
            <div class="input-text input-row full-input">
                <input id="footer-copyright" type="text" data-save="text" value="<?php get_bf_setting('footer-copyright'); ?>" />
            </div>
            <?php } ?>
        </div>
    </div>
</div>