<?php
//*** Customize dashboard menu NEED REFUCTOR***//
function remove_admin_menu_links() {
	global $WPG;

	remove_theme_support( 'genesis-admin-menu' );
	remove_submenu_page( 'themes.php', 'nav-menus.php' );
	remove_submenu_page( 'themes.php', 'widgets.php' );
	remove_submenu_page( 'themes.php', 'customize.php' );
	add_menu_page( 'Menus', __( 'Menus' ), 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 30 );
	//add_menu_page('Widgets', __('Widgets'), 'manage_options', 'widgets.php', '', 'dashicons-welcome-widgets-menus', 31);
	add_menu_page( 'WPBF', 'WPBF', 'manage_options', 'wpbf-options-page', 'wpbf_settings_page', $WPG['dir_img'].'favicon.svg', 59 );
	add_submenu_page( 'wpbf-options-page', 'ACF Main Settings', 'ACF Settings', 'manage_options', 'acf-main-settings' );
	add_submenu_page( 'wpbf-options-page', 'Theme Strings Translate', 'Strings Translate', 'manage_options', 'acf-options-translate' );
	remove_menu_page('acf-main-settings');
	remove_menu_page('acf-options-translate');
}
add_action( 'admin_menu', 'remove_admin_menu_links', 900 );

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page([
		'page_title'  => 'Theme Settings',
		'menu_slug'   => 'acf-main-settings',
		'capability'  => 'manage_options',
	]);
}

function admin_enqueue() {
    if ( function_exists( 'wp_enqueue_media' ) ){
        wp_enqueue_media();
    } else {
        wp_enqueue_style('thickbox');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
    }
}
if (isset($_GET['page']) && $_GET['page'] == 'wpbf-options-page') {
	add_action('admin_enqueue_scripts', 'admin_enqueue', 100);
}

function wpbf_settings_page() {
	require_once 'tabs.php';
}

function bf_settings_init() {
	global $wpdb;

	$query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( BFDBNAME ) );

	if ( $wpdb->get_var( $query ) !== BFDBNAME ) {
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE {$table_name} (
			id int(11) NOT NULL AUTO_INCREMENT,
			name varchar(255) NOT NULL,
			value varchar(255) NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";

		init_deafault_bf_settings();
		
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}
	get_bf_settings();
}
add_action( 'admin_init', 'bf_settings_init' );

function init_deafault_bf_settings() {
	$args = [
		'logo' => 'test.jpg',
		'noty-style' => 'style-1',
		'noty-position' => 'bottom-left',
		'disable-gutenberg' => 1,
	];
	if (sizeof($args)) {
		foreach ($args as $key => $val) {
			update_bf_setting($key, $val);
		}
	}
}

function update_bf_setting($name, $val, $del = false) {
	global $wpdb;
	$BFs = get_bf_setting($name, true);

	if (isset($BFs['id'])) {
		if ($del) {
			$updated = $wpdb->delete(BFDBNAME, ['id' => $BFs['id']]);
		} else {
			$updated = $wpdb->update(BFDBNAME, ['name' => $name, 'value' => $val], ['id' => $BFs['id']]);
		}
	} else {
		$updated = $wpdb->insert(BFDBNAME, ['name' => $name, 'value' => $val]);
	}

	return $updated;
}

function get_bf_setting($name, $array = false) {
    global $wpdb; $return = [];

    $BFsQ = 'SELECT * FROM '. BFDBNAME .' WHERE name="' . $name . '"';
    $result = $wpdb->get_results($BFsQ);
	if (isset($result[0]->id)) {
		$return = (array) $result[0];
	}
	if (!$array) {
		$return = $return['value']??'';
	}

    return $return;
}

function bf_save_theme_settings() {
	$updated = 0;
	$data = $_REQUEST['data'];
	$return = ['success' => false,'message' => 'There is nothing to update'];

	if (sizeof($data)) {
		foreach ($data as $opt) {
			$uptd = update_bf_setting($opt['name'],$opt['value']??'');
			if ($uptd) {
				$updated++;
			}
		}
	}

	if ($updated) {
		$return = ['success' => true,'message' => $updated.' fields was updated.'];
	}

	echo json_encode($return);
	wp_die();
}

add_action('wp_ajax_bf_save_theme_settings','bf_save_theme_settings');