jQuery(document).ready(function () {
    let hash = window.location.hash;
    if (jQuery('#wpbfSettingsPage').length && hash.length) {
        jQuery(hash).trigger('click');
    }

    if (jQuery('#wpcf7-form').length && jQuery('body').hasClass('toplevel_page_wpcf7')) {
        jQuery('#postbox-container-2 p.submit').append('<button id="MBG">Generate</button>');
        var r = /<(\w+)[^>]*>.*<\/\1>/gi;
        var h2s = 'font-size:20px;';
        var ss = 'color:#2155da;';
        var contacts = '';
        var change = 0;
        var mail = '';
        var fields = [];
        var labels = {};
        var labels_f = {};
        jQuery('#MBG').on('click', function (e) {
            e.preventDefault();
            mail = '';
            contacts = '';
            jQuery('#wpcf7-mail-body').val('');
            jQuery('#wpcf7-mail-body').html('');
            var text = jQuery('#wpcf7-form').val();
            text = jQuery(text).find('span, div').contents().unwrap().end().end().html().split(/\n/);
            text = jQuery.grep(text, function (n) {
                return (n);
            });

            var i = 0;
            jQuery('#wpcf7-mail legend > span').each(function () {
                fields[i++] = jQuery(this).text().replace(/[[\]]|[*]|:+/g, '');
            });
            jQuery.each(text, function (key, value) {
                if (value.indexOf("label") >= 0) {
                    var $val = jQuery(value);
                    labels_f[$val.attr('for')] = $val.text();
                } else if (value.indexOf("placeholder") >= 0) {
                    var ph = value.match('placeholder "(.*)"')[1];
                    var temp = value.split(' ');
                    jQuery.each(temp, function (k, val) {
                        if (jQuery.inArray(val, fields) > -1) {
                            labels[val] = ph;
                        }
                    });
                } else if (value.indexOf("id") >= 0) {
                    var id = value.replace(/[[\]]|\s|[*]+/g, '').match('id:(.*)')[1];
                    var temp = value.split(' ');
                    jQuery.each(temp, function (k, val) {
                        if (jQuery.inArray(val, fields) > -1) {

                            labels[val] = labels_f[id];
                        }
                    });
                }
            });
            jQuery.each(fields, function (key, value) {
                contacts += '    <h2 style="' + h2s + '"><strong style="' + ss + '">' + labels[value] + ':</strong> [' + value + '] </h2>\n';
            });
            mail = '<div dir="rtl">\n' + contacts + '</div>';
            jQuery('#wpcf7-mail-body').val(mail);
            jQuery('#wpcf7-mail-subject').val('פניה חדשה מהאתר [_date] [_time]');
            jQuery('#wpcf7-mail-use-html').attr("checked", "checked");
            jQuery('#mail-panel-tab a.ui-tabs-anchor').trigger('click');
        });
    }

    //** Save/Updae By clicking ctr + shift + s **//
    jQuery(document).keyup(function (e) {
        if (e.which == 17) {
            isCtrl = false;
        }
        if (e.which == 16) {
            isShift = false;
        }
    });
    jQuery(document).keydown(function (e) {
        if (e.which == 17) {
            isCtrl = true;
        }
        if (e.which == 16) {
            isShift = true;
        }
        if (e.which == 83 && isCtrl && isShift) {
            save();
        }
    });
});

function save() {
    if (jQuery('#publish.button-primary').length > 0) {
        jQuery('#publish.button-primary').trigger('click');
    } else if (jQuery('#submit.button-primary').length > 0) {
        jQuery('#submit.button-primary').trigger('click');
    } else {
        jQuery('input[type=submit].button-primary').trigger('click');
    }
}

//** Sort ACF repeater by insert a order num **//
jQuery(document).on('dblclick', '.acf-table td.acf-row-handle.order', function () {
    dialog(jQuery(this));
});
function dialog($this) {
    var num = $this.find('span').text();
    var table = $this.parents('tbody');
    var row = $this.parents('.acf-row');
    var rows = table.find('.acf-row');
    var min = 1;
    var max = rows.length - 1;
    var input = prompt("הזן מספר סידורי חדש בין " + min + " ל" + max + ":", num);
    if (input != null && input != "") {
        var order = input;
        if (order != num && order >= min && order <= max) {
            if (order == 1) {
                row.prependTo(table);
            } else if (order == max) {
                row.appendTo(table);
            } else {
                row.insertBefore(rows.eq(order - 1));
            }
            var or = 1;
            setTimeout(function () {
                table.find('.acf-row').each(function () {
                    $(this).find('td.order span').text(or++);
                });
            }, 200);
        } else if (order < min || order > max) {
            alert('הוזן מספר שגוי.');
            dialog($this);
        }
    }
}

//**  wpbf Theme Options  */ 
jQuery('#wpbfSettingsPage .wpbfTabsNav li').click(function(e){
    e.preventDefault();
    let $this = jQuery(this),
        id = $this.attr('id');
    $this.addClass('active').siblings().removeClass('active');
    jQuery('#wpbfSettingsPage .wpbfTabsBox .wpbfTab.' + id).addClass('active').siblings().removeClass('active');
    window.location.hash = '#' + id;
    jQuery(window).scrollTop(0);
});

//**  wpbf Theme Options  */ 
jQuery('#wpbf_save_settings').click(function(e){
    e.preventDefault();
    let $this = jQuery(this).prop('disabled', true),
        $form = jQuery('#wpbf_settings_form'),
        FormData = $form.serializeArray();

    $form.find('input[type=checkbox]').map(function () {
        if (this.name != '') {
            if (!this.checked) {
                FormData.push({ name: this.name, value: 0 });
            } else {
                FormData.push({ name: this.name, value: 1 });
            }
        }
    });

    jQuery.ajax({
        type: "POST",
        url: window.wp_data.ajaxUrl,
        dataType: "json",
        data: {
            action: 'bf_save_theme_settings',
            data: FormData,
        },
        beforeSend: function() {
            
        },
        success: function (response) {
            console.log(response);
            if (response.success) {
                
            } else if (response.message) {
                console.error(response.message);
            }
            setTimeout(function(){
                $this.prop('disabled', false);
            },300);
        }
    });
});

jQuery('body').on('click', '.upload-btn', function(e){
    e.preventDefault();

    let $button = jQuery(this),
        $input = $button.parents('.upload-container').find('input[type="hidden"]'),  
        custom_uploader = wp.media({
            title: 'Insert image',
            library : {
                // uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
                type : 'image'
            },
            button: {
                text: 'Use this image' // button label text
            },
            multiple: false
        }).on('select', function() { // it also has "open" and "close" events
            let attachment = custom_uploader.state().get('selection').first().toJSON();
            $button.html('<img src="' + attachment.url + '" alt=""/>');
            $input.val(attachment.url).prop('change');
        }).open();
});
