<?php global $WPG, $wpdb, $head_scripts, $page_scripts_styles, $wp_scripts; //Define a global variables
define('BFDBNAME', $wpdb->base_prefix.'bf_theme_settings');
$WPG                   = $page_scripts_styles = [];
$head_scripts          = '';
$theme_dir             = get_template_directory_uri();
$theme_path            = TEMPLATEPATH;
$WPG['api_helper_url'] = 'https://giga-art.com/api-helpers/';
$WPG['gmap_key']       = '';
$WPG['dir_path']       = $theme_path. '/';
$WPG['dir_root']       = $theme_dir . '/';
$WPG['dir_js']         = $theme_dir . '/js/';
$WPG['dir_css']        = $theme_dir . '/css/';
$WPG['dir_img']        = $theme_dir . '/images/';
$WPG['user_logged']    = is_user_logged_in();
if ( $WPG['user_logged'] ) {
	$WPG['user']       = wp_get_current_user();
	$WPG['user_admin'] = current_user_can( 'administrator' );
	$WPG['user_role']  = $WPG['user']->roles[0];
}
//*** Insert other data to global ***//
$WPG['is_rtl']    = is_rtl();
$WPG['is_admin']  = is_admin();
$WPG['is_mobile'] = wp_is_mobile();
$WPG['home_url']  = get_option( 'home' );
$WPG['site_name'] = get_bloginfo( 'name' );

if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
	$WPG['lang'] = ICL_LANGUAGE_CODE;
} else {
	$temp_lang   = explode( '_', get_locale() );
	$WPG['lang'] = is_array( $temp_lang ) ? $temp_lang[0] : 'en';
}

if (current_user_can('administrator') && !is_admin() && 1) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

$WPG['icons']['arr'] = '<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" xmlns:xlink="http://www.w3.org/1999/xlink" width="512" height="512" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path style="fill:#00D8DE;" d="M286.187,268.435L112.968,441.677c-6.875,6.876-18.021,6.876-24.897,0.001L45.214,398.82 c-6.874-6.874-6.875-18.02-0.001-24.894l117.914-117.939l-69.122-69.122l-48.79-48.79c-6.875-6.875-6.875-18.021,0-24.896 l42.857-42.857c6.875-6.875,18.021-6.875,24.896,0l116.543,116.543l56.675,56.675C293.06,250.414,293.061,261.56,286.187,268.435z"/><path style="fill:#00A5FF;" d="M230.853,188.261L163.1,256.014l-25.897-25.897l-43.197-43.252l-48.79-48.79 c-6.875-6.875-6.875-18.021,0-24.896L88.07,70.325c6.876-6.876,18.025-6.875,24.899,0.003l91.987,92.035L230.853,188.261z"/><polygon style="fill:#0087FF;" points="230.853,188.261 163.1,256.014 146.67,239.557 214.422,171.804 "/><path style="fill:#00A5FF;" d="M466.786,268.435L293.568,441.677c-6.875,6.876-18.022,6.876-24.897,0.001l-42.858-42.858 c-6.874-6.874-6.875-18.02-0.001-24.894l117.914-117.939l-69.122-69.122l-48.79-48.79c-6.875-6.875-6.875-18.021,0-24.896 l42.857-42.857c6.875-6.875,18.021-6.875,24.896,0L410.11,186.865l56.675,56.675C473.66,250.414,473.66,261.56,466.786,268.435z"/><path style="fill:#00D8DE;" d="M411.452,188.261l-67.753,67.753l-25.897-25.897l-43.197-43.252l-48.79-48.79 c-6.875-6.875-6.875-18.021,0-24.896l42.854-42.854c6.876-6.876,18.025-6.875,24.899,0.003l91.987,92.035L411.452,188.261z"/><g><polygon style="fill:#05C1C1;" points="411.452,188.261 343.699,256.014 327.269,239.557 395.022,171.804 "/><path style="fill:#05C1C1;" d="M494.01,488.583c-4.143,0-7.5,3.358-7.5,7.5v8.417c0,4.142,3.357,7.5,7.5,7.5s7.5-3.358,7.5-7.5 v-8.417C501.51,491.941,498.153,488.583,494.01,488.583z"/><path style="fill:#05C1C1;" d="M449.738,488.583c-4.143,0-7.5,3.358-7.5,7.5v8.417c0,4.142,3.357,7.5,7.5,7.5s7.5-3.358,7.5-7.5 v-8.417C457.238,491.941,453.881,488.583,449.738,488.583z"/><path style="fill:#05C1C1;" d="M405.466,488.583c-4.143,0-7.5,3.358-7.5,7.5v8.417c0,4.142,3.357,7.5,7.5,7.5s7.5-3.358,7.5-7.5 v-8.417C412.966,491.941,409.608,488.583,405.466,488.583z"/></g><g><path style="fill:#0087FF;" d="M106.534,23.417c4.142,0,7.5-3.358,7.5-7.5V7.5c0-4.142-3.358-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v8.417 C99.034,20.059,102.392,23.417,106.534,23.417z"/><path style="fill:#0087FF;" d="M62.262,23.417c4.142,0,7.5-3.358,7.5-7.5V7.5c0-4.142-3.358-7.5-7.5-7.5s-7.5,3.358-7.5,7.5v8.417 C54.762,20.059,58.12,23.417,62.262,23.417z"/><path style="fill:#0087FF;" d="M17.99,0c-4.142,0-7.5,3.358-7.5,7.5v8.417c0,4.142,3.358,7.5,7.5,7.5s7.5-3.358,7.5-7.5V7.5 C25.49,3.358,22.132,0,17.99,0z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';

function wp_custom_scripts() {
    global $WPG;

    if (!$WPG['is_admin']) {
        wp_enqueue_style('main-style', $WPG['dir_root'].'style.min.css',[],filemtime($WPG['dir_path'].'style.min.css'));
        wp_enqueue_style('lightbox-style', $WPG['dir_css'].'lightgallery.min.css',[],filemtime($WPG['dir_path'].'css/lightgallery.min.css'));

        wp_deregister_script('wp-embed');
        wp_deregister_script('jquery');
        wp_deregister_script('jquery-migrate');
        
        wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', [], '3.5.1', false);
        wp_enqueue_script('jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.2/jquery-migrate.js', [], '3.3.2', false);
        wp_enqueue_script('LightgalleryScript', $WPG['dir_js'].'Lightgallery.js', false, filemtime($WPG['dir_path'].'js/Lightgallery.js'), true);
        wp_enqueue_script('Notification', $WPG['dir_js'].'Notifications.js', false, filemtime($WPG['dir_path'].'js/Notifications.js'), true);
        wp_enqueue_script('MainScripts', $WPG['dir_js'].'Scripts.js', false, filemtime($WPG['dir_path'].'js/Scripts.js'), true);


        wp_dequeue_style('addthis_all_pages-css');
        wp_dequeue_style('wp-block-library-rtl-css');
        wp_dequeue_style('contact-form-7');
        wp_dequeue_style('contact-form-7-rtl-css');
        wp_dequeue_style('contact-form-7-email-spam-blocker-css');
    }
}
add_action('wp_enqueue_scripts', 'wp_custom_scripts', 100);

//*** Default ***//
require_once 'functions-helpers/default.php';
require_once 'functions-helpers/remove-heads.php';
require_once 'functions-helpers/functions-helpers.php';
require_once 'functions-helpers/html-tags.php';
if ($WPG['is_admin']) {
    require_once 'admin/theme-settings/theme-settings.php';
}

function get_bf_settings() {
    global $WPG,$wpdb; $return = [];

    $BFsQ = 'SELECT * FROM '. BFDBNAME;
    $result = $wpdb->get_results($BFsQ);

	if (isset($result[0]->id) && !empty($result[0]->id)) {
		foreach ($result as $r) {
			$return[$r->name] = $r->value;
		}
	}

    $WPG['wpbf-options'] = $return;
}
add_action( 'init', 'get_bf_settings' );

//*** Plugins Helpers ***//
require_once 'functions-helpers/plugins-helpers/acf-helpers.php';
require_once 'functions-helpers/plugins-helpers/tiny-mce.php';
require_once 'functions-helpers/plugins-helpers/wpml-helpers.php';

//*** Maybe Plugins ***//
require_once 'functions-helpers/maybe-plugin/custom-post-type-manager.php';
require_once 'functions-helpers/maybe-plugin/google-fonts.php';
require_once 'functions-helpers/maybe-plugin/multilanguages.php';
require_once 'functions-helpers/maybe-plugin/post-ajax.php';
?>