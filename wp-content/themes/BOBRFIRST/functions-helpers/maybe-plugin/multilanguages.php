<?php global $translates; 
$translates = [ 
	'all' => [
		'en' => 'All ', 
		'he' => 'כל ה', 
		'ru' => 'Все '
	],
	'cat' => [
		'en' => 'Category', 
		'he' => 'קטגוריית', 
		'ru' => 'Категория'
	],
	'cats' => [
		'en' => 'Categories', 
		'he' => 'קטגוריות', 
		'ru' => 'Категории'
	],
	'tax_t' => [
		'en' => 'Category', 
		'he' => 'קטגוריות ', 
		'ru' => 'Категории'
	],
	'search' => [
		'en' => 'Search',
		'he' => 'חפש',
		'ru' => 'Поиск'
	],
	'parent'          => [
		'en' => 'Parent', 
		'he' => 'הורה',
		'ru' => 'Родитель',
	],
	'add_new' => [
		'en' => 'Add New', 
		'he' => 'הוסף חדש',
		'ru' => 'Добавить новое',
	],
	'contact'   => [ 'en' => 'Contact', 'he' => 'צור קשר', 'ru' => 'Контакт' ],
	'back_home' => [ 'en' => 'Back to the homepage', 'he' => 'חזרה לעמוד הבית', 'ru' => 'Назад на главную' ],
	'load_more' => [ 'en' => 'Load More', 'he' => 'טען עוד', 'ru' => 'Поиск' ],
	'read_more' => [ 'en' => 'Read more', 'he' => 'קרא אוד', 'ru' => 'Читать далее' ],
    'read_less' => [ 'en' => 'Close', 'he' => 'סגור', 'ru' => 'Закрыть' ],
	'title_404' => [
		'en' => 'The page you reached does not exist.',
		'he' => 'העמוד אליו הגעת אינו קיים.',
		'ru' => 'Страница, которую вы достигли, не существует.'
	],
	'stitle_404'      => [
		'en' => 'Here are some links to help you:',
		'he' => 'הנה כמה לינקים שיוכלו לעזור לך:',
		'ru' => 'Вот несколько ссылок, чтобы помочь вам:'
	],
	'mobile_btn_menu' => [
		'en' => 'Click To Open Main Menu',
		'he' => 'לחץ לפתיחת תפריט ראשי',
		'ru' => 'Нажмите, чтобы открыть главное меню'
	],
];
function multilang($str,$echo = false,$before = '',$after = '') { 
    global $WPG,$translates; 
    $lang = $WPG['lang']??''; 
    $return = ''; 
    $opt_str = $str.'_ml'; 
    if (isset($WPG['options'][$opt_str])&&!empty($WPG['options'][$opt_str])) { 
        $return = $WPG['options'][$opt_str]; 
    } elseif (isset($translates[$str][$WPG['lang']]) && !empty($translates[$str][$WPG['lang']])) { 
        $return = $translates[$str][$WPG['lang']]; 
    } elseif(isset($translates[$str]['en']) && !empty($translates[$str]['en'])) { 
        $return = $translates[$str]['en']; 
    } else { 
        $return = $str; 
    } 
 
    if ($before !== false) { 
        if ($return != $str) { 
            if (empty($before)) { 
                $return = '<span class="wpbf-ml" data-wpbf-ml="'.$str.'">'.$return.'</span>'; 
            } else { 
                $return = $before.$return.$after; 
            } 
        } 
    } 
 
    if ($echo) { 
        echo $return; 
    } elseif ($return) { 
        return $return; 
    } 
} 
if(function_exists('acf_add_options_page') && is_array($translates) && sizeof($translates)) { 
    $g_id = 'group_wpbf_mlfg'; 
    $i = 0; 
    foreach ($translates as $k => $v) { 
        $dv = ''; 
        $f_id = 'field_mxmlf'.$i; $i++; 
        $optstr = $k.'_ml'; 
 
        if (isset($v[$WPG['lang']])&&!empty($v[$WPG['lang']])) { 
            $dv = $v[$WPG['lang']]; 
        } elseif (isset($v['en'])&&!empty($v['en'])) { 
            $dv = $v['en']; 
        } 
 
        $acf_fields[] = [ 
            'key' => $f_id, 
            'label' => $k, 
            'name' => $optstr, 
            'type' => 'text', 
            'parent' => $g_id, 
            'instructions' => '', 
            'default_value' => '', 
            'placeholder' => $dv, 
            'wrapper' => [ 
                'width'=> '33.3%', 
                'class'=> '', 
                'id'=> '' 
            ] 
        ]; 
    } 
    acf_add_options_page(array( 
        'page_title'  => 'Theme Template Translate', 
        'menu_title' => 'Translate',
        'menu_slug' => 'acf-options-translate', 
    )); 
    acf_add_local_field_group(array( 
        'key' => $g_id, 
        'title' => 'wpbf Site Translation', 
        'fields' => $acf_fields, 
        'location' => array ( 
            array ( 
                array ( 
                    'param' => 'options_page', 
                    'operator' => '==', 
                    'value' => 'acf-options-translate', 
                ), 
            ), 
        ), 
    )); 
}