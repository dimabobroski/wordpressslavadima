<?php
function minify_html($html) {
   	$search = [
		'/(\n|^)(\x20+|\t)/',
		'/(\n|^)\/\/(.*?)(\n|$)/',
		'/\n/',
		'/\<\!--.*?-->/',
		'/(\x20+|\t)/', # Delete multispace (Without \n)
		'/\>\s+\</', # strip whitespaces between tags
		'/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
		'/=\s+(\"|\')/'  # strip whitespaces between = "'
	];

   $replace = [
		"\n",
		"\n",
		" ",
		"",
		" ",
		"><",
		"$1>",
		"=$1"
	];

    $html = preg_replace($search,$replace,$html);
    return $html;
}

function phone_link( $num ) {
	return preg_replace( "/[^A-Za-z0-9+]/", "", $num );
}

function map_link( $addr, $waze = false ) {
	global $WPG;

	if (is_array($addr)) {
		$address = $addr['address'];
	} else {
		$address = $addr;
	}

	if ( $WPG['is_mobile'] || $waze ) {
		$link = 'waze://?q=' . $address;
	} else {
		$link = 'https://maps.google.com/?q=' . $address;
	}

	return $link;
}

//*** Remove disallowed tags from string ***//
function remove_ctags( $str, $disallowed = [ 'http://', 'https://', 'www.', 'WWW.' ] ) {
	if ( $disallowed ) {
		foreach ( $disallowed as $d ) {
			if ( strpos( $str, $d ) === 0 ) {
				$str = str_replace( $d, '', $str );
			}
		}

		return $str;
	} else {
		return false;
	}
}

//** NEED REFUCTORING **//
function btn( $inn = '', $url = '', $params = [], $loading = true ) {
	//https://ianlunn.github.io/Hover/ Used animations

	$loader = '';

	if ( isset( $params['tag'] ) ) {
		$tag = $params['tag'];
	} elseif ( ! empty( $url ) ) {
		$tag = 'a';
	} else {
		$tag = 'button';
	}
	if ( isset( $params['class'] ) ) {
		$class = $params['class'];
	} else {
		$class = 'stdbtn def hfs16 br20';
	}

	$html = '<' . $tag;
	if ( ! empty( $url ) ) {
		$html .= ' href="' . $url . '"';
	}
	$html .= ' role="button"';
	if ( $loading ) {
		if ( $loading != true ) {
			$loader = $loading;
		} else {
			$class  .= ' loadbtn';
			$loader = '<div class="loadLayer fcc">';
			$loader .= '<div class="bloader">Loading...</div>';
			$loader .= '</div>';
		}
	}
	$html .= ' class="' . $class . '"';
	if ( isset( $params['attr'] ) ) {
		$html .= ' ' . $params['attr'];
	}
	if ( isset( $params['label'] ) ) {
		$html .= ' aria-label="' . $params['label'] . '"';
	}
	if ( isset( $params['labelby'] ) ) {
		$html .= ' aria-labelledby="' . $params['labelby'] . '"';
	}
	if ( isset( $params['desc'] ) ) {
		$html .= ' aria-describedby="' . $params['desc'] . '"';
	}
	if ( isset( $params['data'] ) ) {
		if ( is_array( $params['data'] ) ) {
			foreach ( $params['data'] as $k => $v ) {
				$html .= ' data-' . $k . '="' . $v . '"';
			}
		} else {
			$html .= ' ' . $params['data'];
		}
	}
	$html .= '>';
	if ( isset( $params['before'] ) ) {
		$html .= '<i class="icon">' . $params['before'] . '</i>';
	}
	$html .= '<span class="notouch">';
	$html .= $inn;
	$html .= '</span>';
	if ( isset( $params['after'] ) ) {
		$html .= '<i class="icon">' . $params['after'] . '</i>';
	}
	if ( $loader ) {
		$html .= $loader;
	}
	$html .= '</' . $tag . '>';

	return $html;
}

// !!! Refactor Function for auto responsive image !!! //
function rimg( $slug, $params = [ 'id' => null, 'def' => '', 'return' => 'url' ] ) {
	global $WPG;
	$data = null;
	$def  = $return = '';

	if ( $WPG['is_mobile'] ) {
		$mob_slug = $slug . '_mob';
	}
	if ( isset( $params['def'] ) && ! empty( $params['def'] ) ) {
		$def = $params['def'];
	} else {
		$def = 'defthumb.jpg';
	}
	if ( isset( $params['id'] ) && $params['id'] != null && $params['id'] != '' ) {
		if ( $WPG['is_mobile'] ) {
			$data = get_field( $mob_slug, $params['id'] );
		}
		if ( $data == null || $data == '' ) {
			$data = get_field( $slug, $params['id'] );
		}
	} else {
		if ( $WPG['is_mobile'] ) {
			if ( isset( $WPG['fields'][ $mob_slug ] ) && ! empty( $WPG['fields'][ $mob_slug ] ) ) {
				$data = $WPG['fields'][ $mob_slug ];
			}
		}
		if ( isset( $WPG['fields'][ $slug ] ) && ! empty( $WPG['fields'][ $slug ] ) && $data == null ) {
			$data = $WPG['fields'][ $slug ];
		}
	}
	if ( $data == null || $data == '' ) {
		$data = $WPG['dir_img'] . $def;
	}

	switch ( $params['return'] ) {
		case 'img':
			$alt = $attr = '';
			if ( isset( $params['alt'] ) && ! empty( $params['alt'] ) ) {
				$alt = $params['alt'];
			}
			if ( isset( $params['attr'] ) && ! empty( $params['attr'] ) ) {
				$attr = $params['attr'];
			}
			$return = '<img src="' . $data . '"';
			if ( isset( $params['class'] ) && ! empty( $params['class'] ) ) {
				$return .= ' ' . $params['class'];
			}
			if ( $attr != '' ) {
				$return .= ' ' . $attr;
			}
			$return .= ' alt="' . $alt . '"/>';
			break;

		case 'bg':
			$return = 'background-image: url(' . $data . ');';
			break;

		default:
			$return = $data;
			break;
	}

	return $return;
}

function get_menu( $name, $class = '', $params = [], $depth = 2 ) {
    if ( !has_nav_menu($name) ) {
        return false;
    }

    $def_params = [
        'theme_location' => $name,
        'depth' => $depth,
        'container_class' => $name,
        'menu_class' => $class
    ];
    $params = array_merge($def_params, $params);
    wp_nav_menu($params);
}

function modify_menu_atts( $atts, $item, $args ) {
    global $WPG;
    // var_dump($atts);
    if (filter_var($item->url, FILTER_VALIDATE_URL) === FALSE) {
        if (!$WPG['is_front']) {
            $atts['href'] = $WPG['home_url'].$item->url;
        }
        $atts['data-area-description'] = 'לחיצה על הקישור יועבר לחלק הרלוונטי בדף (קישור פנימי)';
    }
    return $atts;
}
//add_filter( 'nav_menu_link_attributes', 'modify_menu_atts', 10, 3 );

function get_page_url( $id ) {
    if ( empty($id) ) {
        return false;
    }
	$link = '';

	if ( is_string( $id ) ) {
		$tpl_pages = get_pages( array(
			'meta_key'   => '_wp_page_template',
			'meta_value' => $id
		) );
		if ( is_array( $tpl_pages ) && count( $tpl_pages ) ) {
			$tpl_pages = $tpl_pages[0];
		}
		$link = get_the_permalink( $tpl_pages->ID );
	} elseif ( is_integer( $id ) ) {
		$link = get_the_permalink( $id );
	} elseif ( is_object( $id ) ) {
		if ( isset( $id->term_id ) ) {
			$link = get_term_link( $id );
		} else {
			$link = get_the_permalink( $id );
		}
	}

	return $link;
}

//** If has string or other element in variable */
function has( $el = false ) {
	if(empty($el)){
        return false;
    }

    $return = false;

	if ( is_array( $el ) || is_object( $el ) ) {
        $el = array_filter( (array) $el );
        if (sizeof( (array) $el )) {
            $return = true;
        }
	} elseif ( ! empty( $el ) ) {
		$return = true;
	}

	return $return;
}

//*** Function to easy work with dates ***//
function get_date($format = '') {
    if (empty($format)) {
        $date_format = get_option('date_format');
        $time_format = get_option('time_format');
        $format = $date_format.' '.$time_format;
    }
    $date = date($format, current_time('timestamp'));
    
	return $date;
}

function date_convert( $date, $format, $rformat = '', $params = [] ) {
	if ( empty( $rformat ) ) {
		$rformat = $format;
		$backf   = false;
	} else {
		$backf = true;
	}

	$Timezone              = new DateTimeZone( 'Asia/Jerusalem' );
	$DateTime              = DateTime::createFromFormat( $format, $date, $Timezone );
	$date_array['date']    = $DateTime->format( $rformat );
	$date_array['date_wd'] = $DateTime->format( 'w' );

	if ( $params['calc'] ) {
		$DateTime->modify( $params['calc'] );
		$date_array['calc']    = $DateTime->format( $rformat );
		$date_array['calc_wd'] = $DateTime->format( 'w' );
	}

	if ( $params['diff'] == 'now' ) {
		$now                  = ( new DateTime( 'now' ) )->format( 'YmdHis' );
		$ymd                  = $DateTime->format( 'YmdHis' );
		$willbe               = ( $ymd - $now > 0 ? true : false );
		$date_array['willbe'] = $willbe;
	}

	if ( $params['return'] == 'array' ) {
		$date_array['last'] = $DateTime->format( 't' );
		$date               = $date_array;
	} else {
		if ( $params['diff'] == 'now' ) {
			$date = $willbe;
		} else {
			$date = $DateTime->format( $rformat );
		}
	}

	return $date;
}

function get_thumb_url( $id = '', $params = ['size' => 'medium', 'def' => 'defthumb.jpg', 'alt' => false] ) {
    global $WPG;

	$thumb = $alt = '';
	$size = $params['size']??'medium';
	$def = $params['def']??'defthumb.jpg';

	if ( empty($id) ) {
		global $post;
		$id = $post->ID;
	}

	if ( is_object( $id ) ) {
		$thumb = get_field( 'thumb', $id );
	} elseif ( is_array( $id ) ) {
		$thumb = get_field( 'thumb', $id['tax'] . '_' . $id['id'] );
	} else {
		$thumbnail_id = get_post_thumbnail_id( $id );
		$url          = wp_get_attachment_image_src( $thumbnail_id, $size )[0];
		$alt          = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true );
	}

	if ( !empty( $thumb ) ) {
		if ($size == 'full') {
			$url = $thumb['url'];
		} else {
			$url = $thumb['url'][ $size ];
		}
		$alt = $thumb['alt']??'';
	}

	if ( empty( $url ) ) {
		$url = $WPG['dir_img'] . $def;
	}

	if ( isset( $params['alt'] ) && $params['alt'] ) {
		$return = [
			'url' => $url,
			'alt' => $alt
		];
	} else {
		$return = $url;
	}

	return $return;
}

function get_thumb_data($id = '',$size = '', $return = 'array') {
    if ($id == '') {
        global $post;
        $id = $post->ID;
    }    
    $thumbnail_id    = get_post_thumbnail_id($id);
    if ($thumbnail_id) {
        $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
    }

    if (isset($thumbnail_image[0]) && $thumbnail_image) {
        $attachment = $thumbnail_image[0];
        switch ($return) {
            case 'data':
                $return = $attachment;
                break;

            case 'title':
                $return = $attachment->post_title??'';
                break;

            case 'alt':
                $return = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true )??'';
                break;

            case 'cap':
                $return = $attachment->post_excerpt??'';
                break;

            case 'desc':
                $return = $attachment->post_content??'';
                break;

            case 'credit':
                $return = photo_credit($attachment);
                break;

            case 'src':
                $return = wp_get_attachment_image_src($thumbnail_id, $size)[0];
                break;
            
            default:
                $return = [
                    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
                    'caption' => $attachment->post_excerpt,
                    'description' => $attachment->post_content,
                    'href' => get_permalink($attachment->ID),
                    'src' => $attachment->guid,
                    'title' => $attachment->post_title,
                    'credit' => photo_credit($attachment)
                ];
                if (!empty($size)) {
                    $return[$size] = wp_get_attachment_image_src($thumbnail_id, $size)[0];
                }
                break;
        }
    } else {
        $return = '';
    }

    return $return;
}

function tablet_is_mobile($is_mobile) {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'Tablet') !== false) {
        $is_mobile = true;
    }
    return $is_mobile;
}
add_filter('wp_is_mobile', 'tablet_is_mobile');

function get_browser_type() {

    $ExactBrowserNameUA = strtolower($_SERVER['HTTP_USER_AGENT']);
    
    if (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")) {
        // OPERA
        $ExactBrowserNameBR = "opera";
    } elseif (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "chrome/")) {
        // CHROME
        $ExactBrowserNameBR = "chrome";
    } elseif (strpos(strtolower($ExactBrowserNameUA), "msie")) {
        // INTERNET EXPLORER
        $ExactBrowserNameBR = "ie";
    } elseif (strpos(strtolower($ExactBrowserNameUA), "firefox/")) {
        // FIREFOX
        $ExactBrowserNameBR = "firefox";
    } elseif (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")==false and strpos(strtolower($ExactBrowserNameUA), "chrome/")==false) {
        // SAFARI
        $ExactBrowserNameBR = "safari";
    } else {
        // OUT OF DATA
        $ExactBrowserNameBR = "other";
    }

    return $ExactBrowserNameBR;
}

function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	if(empty($color))
          return $default; 
 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        $rgb =  array_map('hexdec', $hex);

        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }

        return $output;
}

function debug($data) {
	echo '<pre dir="ltr">';
	var_dump($data);
	echo '</pre>';
}

function escape($string) {
	return htmlspecialchars($string, ENT_QUOTES);
}