<?php
//*** Remove headlinks ***//
function removeHeadLinks() {
    // Remove <link rel="https://api.w.org/" href="https://framework.giga-art.com/wp-json/" /><link rel="alternate" type="application/json" href="https://framework.giga-art.com/wp-json/wp/v2/pages/2" />
    remove_action('wp_head', 'rest_output_link_wp_head', 10);

	// First, we remove all the RSS feed links from wp_head using remove_action
	// remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );

	// Resource Hints is a rather new W3C specification that “defines the dns-prefetch,
	// preconnect, prefetch, and prerender relationships of the HTML Link Element (<link>)”.
	// These can be used to assist the browser in the decision process of which origins it
	// should connect to, and which resources it should fetch and preprocess to improve page performance.
	remove_action( 'wp_head', 'wp_resource_hints', 2 );

	// https://developer.wordpress.org/reference/functions/wp_oembed_add_discovery_links/
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

	// WordPress Page/Post Shortlinks
	// URL shortening is sometimes useful, but this automatic ugly url
	// in your header is useless. There is no reason to keep this. None.
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );

	// Remove <link rel="EditURI" type="application/rsd+xml" title="RSD" href="DOMAIN/xmlrpc.php?rsd" />
	remove_action( 'wp_head', 'rsd_link' );
    
	// Windows Live Writer Manifest Link
	// If you don’t know what Windows Live Writer is (it’s another blog editing client), then remove this link.
	remove_action( 'wp_head', 'wlwmanifest_link' );

	// // WordPress Generator (with version information)
	// // This announces that you are running WordPress and what version you are using. It serves no purpose.
	remove_action( 'wp_head', 'wp_generator' );

    // Remove All Yoast HTML Comments
    if (class_exists('WPSEO_Options')) {
        add_filter( 'wpseo_debug_markers', '__return_false' );
    }
}
add_action( 'init', 'removeHeadLinks' );

function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    // add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    // add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

// Remove unwanted styles from wp or plugins
function my_style_loader_tag_filter($html, $handle) {
    $fonts = ['font-style'];
    $disallowed = ['wp-block-library','contact-form-7-rtl','addthis_all_pages'];
    if (in_array($handle,$disallowed)) {
        return '';
    }
    if (in_array($handle,$fonts)) {
        return str_replace('rel="stylesheet"',
            'rel="preload" as="style" type="text/css" onload="this.rel=\'stylesheet\'" crossorigin="anonymous"', $html);
    }
    return $html;
}
add_filter('style_loader_tag', 'my_style_loader_tag_filter', 10, 2);

// <!-- Google CDN -->
// <link rel="dns-prefetch" href="//ajax.googleapis.com">

// <!-- Google API -->
// <link rel="dns-prefetch" href="//apis.google.com">

// <!-- Google Fonts -->
// <link rel="dns-prefetch" href="//fonts.googleapis.com">
// <link rel="dns-prefetch" href="//fonts.gstatic.com">

// <!-- Google Analytics -->
// <link rel="dns-prefetch" href="//www.google-analytics.com">

// <!-- Google Tag Manager -->
// <link rel="dns-prefetch" href="//www.googletagmanager.com">

// <!-- Google Publisher Tag -->
// <link rel="dns-prefetch" href="//www.googletagservices.com">

// <!-- Google AdSense -->
// <link rel="dns-prefetch" href="//adservice.google.com">
// <link rel="dns-prefetch" href="//pagead2.googlesyndication.com">
// <link rel="dns-prefetch" href="//tpc.googlesyndication.com">

// <!-- Google Blogger -->
// <link rel="dns-prefetch" href="//bp.blogspot.com">
// <link rel="dns-prefetch" href="//1.bp.blogspot.com">
// <link rel="dns-prefetch" href="//2.bp.blogspot.com">
// <link rel="dns-prefetch" href="//3.bp.blogspot.com">
// <link rel="dns-prefetch" href="//4.bp.blogspot.com">

// <!-- Microsoft CDN -->
// <link rel="dns-prefetch" href="//ajax.microsoft.com">
// <link rel="dns-prefetch" href="//ajax.aspnetcdn.com">

// <!-- Amazon S3 -->
// <link rel="dns-prefetch" href="//s3.amazonaws.com">

// <!-- Cloudflare CDN -->
// <link rel="dns-prefetch" href="//cdnjs.cloudflare.com">

// <!-- jQuery CDN -->
// <link rel="dns-prefetch" href="//code.jquery.com">

// <!-- Bootstrap CDN -->
// <link rel="dns-prefetch" href="//stackpath.bootstrapcdn.com">

// <!-- Font Awesome CDN -->
// <link rel="dns-prefetch" href="//use.fontawesome.com">

// <!-- Facebook -->
// <link rel="dns-prefetch" href="//connect.facebook.net">

// <!-- Twitter -->
// <link rel="dns-prefetch" href="//platform.twitter.com">

// <!-- Linkedin -->
// <link rel="dns-prefetch" href="//platform.linkedin.com">

// <!-- Vimeo -->
// <link rel="dns-prefetch" href="//player.vimeo.com">

// <!-- GitHub -->
// <link rel="dns-prefetch" href="//github.githubassets.com">

// <!-- Disqus -->
// <link rel="dns-prefetch" href="//referrer.disqus.com">
// <link rel="dns-prefetch" href="//c.disquscdn.com">

// <!-- Gravatar -->
// <link rel="dns-prefetch" href="//0.gravatar.com">
// <link rel="dns-prefetch" href="//2.gravatar.com">
// <link rel="dns-prefetch" href="//1.gravatar.com">

// <!-- BuySellads -->
// <link rel="dns-prefetch" href="//stats.buysellads.com">
// <link rel="dns-prefetch" href="//s3.buysellads.com">

// <!-- DoubleClick -->
// <link rel="dns-prefetch" href="//ad.doubleclick.net">
// <link rel="dns-prefetch" href="//googleads.g.doubleclick.net">
// <link rel="dns-prefetch" href="//stats.g.doubleclick.net">
// <link rel="dns-prefetch" href="//cm.g.doubleclick.net">