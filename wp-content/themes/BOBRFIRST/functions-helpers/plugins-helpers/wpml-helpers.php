<?php
//*** Fix WP rest api link with WPML ***//
add_filter( 'rest_url', function ( $url ) {
	$result = preg_match( '/(\?.+)\//U', $url, $matches );
	if ( ! $result || ! isset( $matches[0] ) || ! isset( $matches[1] ) ) {
		return $url;
	}
	$url = str_replace( $matches[0], '', $url );

	return $url . $matches[1];
} );


//*** Function for auto make language selector ***//
function langs_select( $args = 'flag' ) {
	global $WPG;

	if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
		$langs     = icl_get_languages( 'skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str' );
		$flags_dir = $WPG['dir_img'] . 'flags/';

		if ( $args != 'flag' && strpos( $args, ',' ) !== false ) {
			$args = explode( ',', $args );
		} else {
			$args = array( $args );
		}

		$h_flag      = 0;
		$h_name      = 0;
		$h_short     = 0;
		$h_trans     = 0;
		$h_select    = 0;
		$show_active = 1;

		if ( in_array( 'name', $args ) ) {
			$h_name = 1;
		}
		if ( in_array( 'short', $args ) ) {
			$h_short = 1;
		}
		if ( in_array( 'trans', $args ) ) {
			$h_trans = 1;
		}
		if ( in_array( 'select', $args ) ) {
			$h_select = 1;
		}
		if ( in_array( 'hide_active', $args ) && ! $h_select ) {
			$show_active = 0;
			$args        = array_diff( $args, array( 'hide_active' ) );
		}
		if ( in_array( 'flag', $args ) || ( $h_select && count( $args ) == 1 ) ) {
			$h_flag = 1;
		}

		foreach ( $langs as $key => $val ) {

			$active = $val['active'];
			if ( ( $active && $show_active ) || ! $active ) {
				$url             = $val['url'];
				$tag             = explode( '-', $val['tag'] )[1];
				$code            = $val['code'];
				$native_name     = $val['native_name'];
				$translated_name = $val['translated_name'];

				if ( $active ) {
					$cls = ' active';
				} else {
					$cls = '';
				}

				if ( $h_short ) {
					if ( $h_trans || $code == 'en' ) {
						$name = ucfirst( $code );
					} elseif ( $code == 'he' ) {
						$name = substr( $native_name, 0, 4 );
					} else {
						$name = substr( $native_name, 0, 3 );
					}
				} else {
					if ( $h_name ) {
						$name = $native_name;
					} elseif ( $h_trans ) {
						$name = $translated_name;
					}
				}

				$start  = '<a id="Lang-' . $code . '" href="' . $url . '" class="lang flex aic' . $cls . '">';
				$inside = '';
				$end    = '</a>';

				if ( $h_flag ) {
					$flag   = '<img src="' . $flags_dir . strtolower( $tag ) . '.png' . '" alt=""/>';
					$inside = $flag;
				}

				if ( $name ) {
					$inside .= '<span>' . $name . '</span>';
				}

				if ( $h_select ) {
					$temp .= '<li class="select-item' . $cls . '">' . $start . $inside . $end . '</li>';
				} else {
					$temp .= $start . $inside . $end;
				}
			}
		}
		if ( $h_select ) {
			$script = '<script>$(document).ready(function(){var e=$("#LangsSelect ul");e.height(e.outerHeight()),e.removeClass("dropped");var a=e.find("li.active");a.find("a").attr("href","javascript:void(0);"),e.prepend(a),$("#LangsSelect").removeAttr("class"),$("#LangsSelect ul li a").click(function(a){e.hasClass("dropped")?(e.prepend($(this).parent()),e.removeClass("dropped")):(a.preventDefault(),e.addClass("dropped"))})});</script>';
			if ( $h_flag ) {
				$ul = '<ul class="langs_list dropped hflags">';
			} else {
				$ul = '<ul class="langs_list dropped">';
			}
			$temp = '<div id="LangsSelect" class="overh flex aic">' . $ul . $temp . '</ul><svg height="32px" style="enable-background:new 0 0 31.999 32"version=1.1 viewBox="0 0 31.999 32" width="31.999px" x=0px xml:space=preserve xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink y=0px><g><path d="M31.92,5.021l-14.584,22.5c-0.089,0.139-0.241,0.223-0.406,0.229c-0.004,0-0.009,0-0.014,0   c-0.16,0-0.311-0.076-0.404-0.205L0.096,5.044C-0.015,4.893-0.031,4.69,0.054,4.523S0.312,4.25,0.5,4.25h31   c0.183,0,0.352,0.101,0.438,0.261C32.025,4.67,32.019,4.868,31.92,5.021z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg></div>' . $script;
		}
		$return = $temp;

		return $return;
	} else {
		return $WPG['lang'];
	}
}