<?php
//*** Add google map key to acf maps ***//
function acf_google_map_api( $api ) {
	global $WPG;
	
	$lang = $WPG['lang'] == 'he' ? 'iw' : $WPG['lang'];
	
	$api['key']      = $WPG['gmap_key'];
	$api['language'] = $lang;
	return $api;
}

add_filter( 'acf/fields/google_map/api', 'acf_google_map_api' );

//*** Function for work with yooutube and vimeo and used for .videopop class ***//
function iframe( $iframe, $params = [], $class = 'iframe-' ) {
    if (empty($iframe)) {
        return false;
    }

    $v_def_params = array(
        'api'       => true,
        'loop'      => 0,
        'title'     => true,
        'color'     => 'white',
        'width'     => '',
        'xhtml'     => false,
        'byline'    => true,
        'height'    => '',
        'portrait'  => true,
        'callback'  => '',
        'autoplay'  => false,
        'maxwidth'  => '',
        'maxheight' => '',
        'player_id' => '',
        'autopause' => true,
    );

    $y_def_params = array(
        'fs'             => 0,
        'rel'            => 0,
        'loop'           => 0,
        'color'          => 'white',
        'theme'          => 'light',
        'playlist'       => '',
        'showinfo'       => 0,
        'autoplay'       => 1,
        'controls'       => 2,
        'autohide'       => 1,
        'disablekb'      => 1,
        'enablejsapi'    => 1,
        'modestbranding' => 0,
    );

    $iframe_atts = '';
    $return = $params['return']??'iframe';
    unset( $params['return'] );

    if (strpos($iframe, 'src') !== false) {
        preg_match('/src="(.+?)"/', $iframe, $matches);
        $src = $matches[1];
    } else {
        $src = $iframe;
    }

	if ( strpos( $src, 'vimeo' ) !== false ) {

        $type = 'vimeo';
        $params = array_merge($v_def_params, $params);
        $params = array_filter( $params );

        $the_id = $vimeoID = str_replace( 'video/', '', substr( parse_url( $src, PHP_URL_PATH ), 1 ) );
        if ( $return == 'array' ) {
            $thumb = vimeo_video_image( $vimeoID );
        }
        $the_src    = $new_src = add_query_arg( $params, $src );
        $iframe_atts = 'webkitallowfullscreen mozallowfullscreen';

    } elseif ( strpos( $src, 'youtu.be' ) !== false || strpos( $src, 'watch?v=' ) !== false || strpos( $src, '?feature=oembed' ) !== false ) {

        $pattern = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user|shorts)\/))([^\?&\"'>]+)/";
        preg_match( $pattern, $src, $matches );
        if ( !empty($matches) ) {
            $the_id = $youtube_id = $matches[1];
        }
        $src = 'https://www.youtube.com/embed/' . $youtube_id;

        $type = 'youtube';
        $params = array_merge($v_def_params, $params);
        $params = array_filter( $params );

        $thumb      = 'https://img.youtube.com/vi/' . $youtube_id . '/0.jpg';
        $the_src    = $new_src = add_query_arg( $params, $src );
    
    } else {

        $type = 'video';
        $the_src    = $src;

	}
    
    $the_iframe = '<iframe src="' . $the_src . '" class="' . $class . $type . '" frameborder="0" allowfullscreen' . $iframe_atts . '></iframe>';

	switch ( $return ) {
		case 'id':
			return $the_id;
			break;

		case 'url':
			return $the_src;
			break;

		case 'src':
			return $the_src;
			break;

		case 'data':
			echo 'data-type="' . $type . '" data-code="' . $the_id . '"';
			break;

		case 'array':
			$the_return = array(
				'id'     => $the_id,
				'src'    => $the_src,
				'type'   => $type,
				'thumb'  => $thumb,
				'iframe' => $the_iframe,
			);

			return $the_return;
			break;

		default:
			return $the_iframe;
			break;
	}
}

//*** Help function for iframe() to get video image ***//
function vimeo_video_image( $id ) {
	$ch = curl_init( 'http://vimeo.com/api/v2/video/' . $id . '.php' );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
	$a    = curl_exec( $ch );
	$hash = unserialize( $a );

	return $hash[0]["thumbnail_medium"];
}