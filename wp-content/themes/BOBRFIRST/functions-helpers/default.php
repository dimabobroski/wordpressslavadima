<?php
//*** Load current page data to global $WPG ***//
function wpg_init() {
	global $WPG, $template;
	$WPG['noindex'] = false;

	$wp              = &$GLOBALS['wp_query'];
	$WPG['id']       = $wp->queried_object_id;
	$WPG['format']   = get_post_format( $WPG['id'] ) ?: 'standard';
	$temp            = explode( '/', $template );
	$WPG['template'] = end( $temp );

	$WPG['is_home']  = $wp->is_home;
	$WPG['is_front'] = $wp->is_front_page();

	$WPG['is_page']   = $wp->is_page;
	$WPG['is_single'] = $wp->is_single;

	$WPG['is_cat']     = $wp->is_category;
	$WPG['is_tag']     = $wp->is_tag;
	$WPG['is_tax']     = $wp->is_tax;
	$WPG['is_term']    = ( $wp->is_tax || $wp->is_category || $wp->is_archive ) ? true : false;
	$WPG['is_archive'] = $wp->is_archive;

	$WPG['is_loop']       = $wp->in_the_loop;
	$WPG['is_search']     = $wp->is_search;
	$WPG['is_404']        = $wp->is_404;
	$WPG['is_paged']      = $wp->is_paged;
	$WPG['is_admin']      = $wp->is_admin;
	$WPG['is_attachment'] = $wp->is_attachment;
	$WPG['is_singular']   = $wp->is_singular;
	$WPG['is_robots']     = $wp->is_robots;
	$WPG['is_embed']      = $wp->is_embed;

	if ( $WPG['is_term'] ) {
		$WPG['term'] = $wp->queried_object;
		$WPG['title'] = $wp->queried_object->name;
		$WPG['type']  = $wp->queried_object->taxonomy;
		if ( empty( $WPG['type'] ) ) {
			$temp         = explode( '-', str_replace( '.php', '', $WPG['template'] ) );
			$WPG['type'] = end( $temp );
		}
		$WPG['tax'] = $WPG['type'] . '_' . $WPG['id'];
	} elseif ( $wp->queried_object ) {
		$WPG['title'] = $wp->queried_object->post_title;
		$WPG['type']  = $wp->queried_object->post_type;
	}
	if ( empty( $WPG['type'] ) && $wp->query_vars ) {
		$WPG['type'] = $wp->query_vars['post_type'];
	}

	if ( ! empty( $WPG['id'] && empty( $WPG['template'] ) ) ) {
		$template = get_post_meta( $WPG['id'], '_wp_page_template', true );
		if ( ! $template || 'default' == $template ) {
			return '';
		}
		$WPG['template'] = $template;
	}

	$noindex = array(
		'search.php',
		'page-wpbf-login.php',
	);
	if ( $WPG['is_search'] || in_array( $WPG['template'], $noindex ) ) {
		$WPG['noindex'] = true;
	}

	if ( $WPG['is_page'] || $WPG['is_single'] ) {
		$WPG['from'] = $WPG['id'];
	} elseif ( key_exists( 'tax', $WPG ) ) {
		$WPG['from'] = $WPG['tax'];
	}
	$WPG['fields'] = $WPG['options'] = [];
	if ( function_exists( 'get_field' ) ) {
		
		$WPG['options'] = get_fields('options');

		if (key_exists( 'from', $WPG )) {
			$WPG['fields'] = get_fields( $WPG['from'] );
		} else {
			$WPG['fields'] = get_fields();
		}
			
	}
	// $WPG['wpbf-options'] = get_bf_settings();
	$WPG = array_filter( $WPG, function ( $value ) {
		return $value !== '';
	} );
}
add_action( 'wp_head', 'wpg_init' );

/** add option enable/disable favicon in admin panel **/
function favicon() {
	global $WPG;

	$icon = ( isset( $WPG['options']['favicon'] ) && ! empty( $WPG['options']['favicon'] ) ) ? $WPG['options']['favicon'] : $WPG['dir_img'] . 'favicon.png';

	echo '<link rel="icon" href="' . $icon . '" type="image/png"/>';
}

add_action( 'wp_head', 'favicon' );
add_action( 'admin_head', 'favicon' );

//*** add option enable/Disable Gutenberg Completely ***//
if ( version_compare( $GLOBALS['wp_version'], '5.0-beta', '>' ) ) {
	// WP > 5 beta
	add_filter( 'use_block_editor_for_post_type', '__return_false', 100 );
} else {
	// WP < 5 beta
	add_filter( 'gutenberg_can_edit_post_type', '__return_false' );
}

//*** Async scripts loading ***//
function async_scripts_load( $url ) {
	global $WPG;
	if ( strpos( $url, '#asyncload' ) === false ) {
		return $url;
	} elseif ( $WPG['is_admin'] ) {
		return str_replace( '#asyncload', '', $url );
	} else {
		return str_replace( '#asyncload', '', $url ) . "' async='async";
	}
}

add_filter( 'clean_url', 'async_scripts_load', 11, 1 );
////****** WP Speed up END ******////

//*** Enqueue custom scripts and styles ***//
function custom_admin_js_css() {
	global $WPG;
	if ( $WPG['is_admin'] ) {
		wp_enqueue_script( 'admin_script', $WPG['dir_root'] . 'admin/admin.js' );
		wp_enqueue_style( 'dashboard-style', $WPG['dir_root'] . 'admin/admin.css' );
	}
}

add_action( 'admin_head', 'custom_admin_js_css' );

//*** Add support of posts thumbnail ***//
add_theme_support( 'post-thumbnails' );

//*** Register custom menu locations ***//
function register_menus() {
	register_nav_menu( 'header-menu', __( 'Header Menu' ) );
	register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
	register_nav_menu( 'sidebar-menu', __( 'Sidebar Menu' ) );
	register_nav_menu( 'menu-404', __( '404 Menu' ) );
}

add_action( 'init', 'register_menus' );

//*** Function to better way to enqueue scripts or styles ***//
function add_script_style( $files = false ) {
	global $WPG, $page_scripts_styles, $wp_scripts;

	if ( $files && is_array( $files ) ) {
		foreach ( $files as $file ) {
			if ( ! in_array( $file, $page_scripts_styles ) ) {
				$ext = pathinfo( $file, PATHINFO_EXTENSION );
				$dir = $WPG['dir_root'];
				$name = $file;
				if ( 'js' === $ext ) {
					$dir = strpos( $file, 'http' ) !== false ? $file : $dir . 'js/' . $file;
				} elseif ( 'css' === $ext ) {
					$dir = strpos( $file, 'http' ) !== false ? $file : $dir . 'css/' . $file;
				} else {
					$dir = $file;
				}
				if ( strpos( $name, '/' ) !== false ) {
					$name = explode( '/', $file );
					$name = end( $name );
				}
				$name = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace( '.', '-', $name ));

				if ( 'js' === $ext ) {
					wp_enqueue_script( $name, $dir, false, '', true );
				} else {
					wp_enqueue_style( $name, $dir, false, '', 'all' );
				}
				$page_scripts_styles[] = $file;
			}
		}
	}
}

//*** Allow wp svg support ***//
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {
	$filetype = wp_check_filetype( $filename, $mimes );
	return [
		'ext'             => $filetype['ext'],
		'type'            => $filetype['type'],
		'proper_filename' => $data['proper_filename']
	];
}, 10, 4 );

function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

//*** Function to easy load svg file by name or url ***//
function svg($url, $size = '', $params = ['id'=>'', 'class'=>'', 'alt'=>'', 'desc'=>'', 'clear'=>true]) {
	global $WPG;

	$uID = uniqid('wpbf-svg-');
	$svg = $path = $alt = $class = $title = $desc = '';
	$alb = ' aria-labelledby="';
	$nosvg = $WPG['dir_img'] . 'icons/nosvg.png';

	if ( is_array( $url ) ) {
		if ( empty( $alt ) ) {
			$alt = $url['alt'];
		}
		if (!empty($size)) {
			$url = $url['sizes'][$size];
		} else {
			$url = $url['url'];
		}
	}

	if (has($params['id']??'')) {
		$id = ' id="' . $params['id'] . '"';
	} else {
		$id = ' id="' . $uID . '"';
	}

	if (has($params['class']??'')) {
		$class = ' class="'.$params['class'].'"';
	}

	if (has($params['alt']??'')) {
		$tID = $uID.'-title';
		$alb .= $tID;
		$alt = ' alt="'.$params['alt'].'"';
		$title = '<title id="' . $tID . '">'.$params['alt'].'</title>';
	}

	if (has($params['desc']??'')) {
		$dID = $uID.'-desc';
		$alb .= ' '.$dID;
		$alt = ' alt="'.$params['alt'].'"';
		$desc = '<desc id="' . $dID . '">'.$params['alt'].'</desc>';
	}

	if ($alb != ' aria-labelledby="') {
		$id .= $alb . '"';
	}

	if (!empty($size)) {
		if ( strpos($size, ':') !== false ) {
			$size = explode( ':', $size );
			$wd   = $size[0];
			$he   = $size[1];
		} else {
			$wd = $he = $size;
		}

		$id .= ' style="width:' . $wd . 'px;height:' . $he . 'px;min-width:' . $wd . 'px;min-height:' . $he . 'px;"';
	}

	if(filter_var($url, FILTER_VALIDATE_URL)) {
		if (strpos($url, $WPG['home_url']) !== false && strpos($url, '.svg') !== false) {
			$path = attachment_url_to_path($url);
		} else {
			$svg = '<img src="' . $url . '"'. $class . $alt . '/>';
		}
	} elseif (strpos($url, '<svg') !== false) {
		$svg = $url;
	} else {
		$path = $WPG['dir_path'] . 'images/' . $url;
	}
	
	if (!empty($path) && empty( $svg ) && file_exists( $path ) ) {
		$svg = file_get_contents( $path );
	}
	
	if (strpos($svg, 'id="') !== false) {
		preg_match_all('/id="([^"]+)/', $svg, $matches);
		if (has($matches[1]??'')) {
			foreach ($matches[1] as $i) {
				$idz = uniqid('wpbf-' . $i . '-');
				$svg = str_replace('id="' . $i . '"', 'id="' . $idz . '"', $svg);
				$svg = str_replace('#'.$i, '#'.$idz, $svg);
			}
		}
	}

	if (!isset($params['clear'])||$params['clear'] == true) {
		$svg = preg_replace('/(id|data-name)="[^wpbf][^"]+"/', '', $svg);
	}

	if (!empty($id)) {
		$svg = str_replace('<svg', '<svg'.$id, $svg);
	}

	if (!empty($title)||!empty($desc)) {
		$rep = $title . $desc;
		$svg = preg_replace('/>/s', '>'.$rep, $svg, 1);
	}
	
	if (empty($svg)) {
		$svg = '<img src="' . $nosvg . '"'. $class . $alt . '/>';
	} else {
		$svg = minify_html($svg);
	}

	return $svg;
}

//*** Convert wp url to path ***//
function attachment_url_to_path( $url ) {
	$parsed_url = parse_url( $url );
	if(empty($parsed_url['path'])) return false;
		$file = ABSPATH . ltrim( $parsed_url['path'], '/');
	if (file_exists( $file)) return $file;
		return false;
}

// Check if needed
function svg_s( $svg, $size, $class = '' ) {
	$temp = explode( '>', $svg );
	if ( is_string( $size ) ) {
		$size = explode( ':', $size );
		$wd   = $size[0];
		$he   = $size[1];
	} else {
		$wd = $he = $size;
	}
	$temp[0] = preg_replace( '/(<[^>]+) id=".*?"/i', '$1', $temp[0] );
	if ( strpos( $temp[0], 'style="' ) !== false ) {
		$temp[0] = str_replace( 'style="', 'style="width:' . $wd . 'px;height:' . $he . 'px;min-width:' . $wd . 'px;min-height:' . $he . 'px;', $temp[0] );
	} else {
		$temp[0] = str_replace( '<svg', '<svg style="width:' . $wd . 'px;height:' . $he . 'px;min-width:' . $wd . 'px;min-height:' . $he . 'px;" ', $temp[0] );
	}
	if ( $class ) {
		if ( strpos( $temp[0], 'class="' ) !== false ) {
			$temp[0] = str_replace( 'class="', 'class="' . $class . ' ', $temp[0] );
		} else {
			$temp[0] = str_replace( '<svg', '<svg class="' . $class . '" ', $temp[0] );
		}
	}
	$sized = implode( '>', $temp );

	return $sized;
}

function js_variables() {
	global $WPG;

	$wpbfDataFront = [
        'noty-style',
        'noty-position',
        'noty-from-side',
    ];

	$variables = array(
		'ajaxUrl'  => admin_url( 'admin-ajax.php' ),
		'isRtl'    => $WPG['is_rtl'],
		'isMobile' => $WPG['is_mobile'],
		'mobileWidth' => 1024
	);
	if (sizeof($wpbfDataFront) && isset($WPG['wpbf-options']) && sizeof($WPG['wpbf-options'])) {
		foreach ($wpbfDataFront as $d) {
			if (key_exists($d,$WPG['wpbf-options'])) {
				$variables['wpbf'][$d] = $WPG['wpbf-options'][$d];
			}
		}
	}

	echo(
		'<script type="text/javascript">window.wp_data = ' . json_encode( $variables ) . ';</script>'
	);
}

add_action( 'wp_head', 'js_variables' );
add_action( 'admin_head', 'js_variables' );

//*** Limit maximum file size for upload ***//
function media_upload_size_limit( $size ) {
	$size = 10 * 1024 * 1024;
	return $size;
}

add_filter( 'upload_size_limit', 'media_upload_size_limit', 20 );

//*** Add custom media sizes ***//
function wpdocs_theme_setup() {
	add_image_size( 'thumbnail', 320, 320, true );
	add_image_size( 'medium', 640, 500 );
	add_image_size( 'large', 1024, 1024 );
	add_image_size( 'full-hd', 1920, 1080 );
	add_image_size( 'hd', 1280, 720 );
	add_image_size( 'mobile', 800, 9999 );
	add_image_size( 'medium_large', 760, 9999 );
}

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );

//*** Add titles for custom media sizes ***//
function custom_sizes_titles( $sizes ) {

	$sizes = array(
		'thumbnail'    => 'Thumbnail',
		'medium'       => 'Medium',
		'large'        => 'Large',
		'hd'           => 'HD 720p',
		'full-hd'      => 'Full HD',
		'mobile'       => 'Mobile',
		'medium_large' => 'Medium Large',
	);
	return $sizes;
}

add_filter( 'image_size_names_choose', 'custom_sizes_titles' );


function inject_content_filters($content) {
	if (strpos($content, '<p><span id="more-') !== false) {
		$content = preg_replace('/<span id\=\"(more\-\d+)"><\/span>/', '<!--more-->', $content);
		$mContent = explode('<p><!--more--></p>',$content);
		$pr_id = uniqid('readMoreBox-');
		foreach ($mContent as $i => $mc) {
			$id = $pr_id . $i;
			if ($i) {
				$content .= '<div class="morewrap mt15">';
				$content .= '<div id="' . $id . '" class="morebox ovhide h0 overh anim"><div class="sizer">' . str_replace('<p></p>','',$mc) . '</div></div>';
				$content .= '<button class="textReadMore" aria-expanded="false" aria-controls="' . $id . '" data-rless="' . multilang('read_less',false,false) . '" data-rmore="' . multilang('read_more',false,false) . '"><span>' . multilang('read_more') . '</span></button>';
				$content .= '</div>';
			} else {
				$content = $mc;
			}
		}
	}
	return $content;
}
add_filter('the_content', 'inject_content_filters', 999);
