<?php get_header(); global $WPG; if (has($WPG['fields']??'')) { $fe = &$WPG['fields']; } else { $fe = ''; }  ?>
<div id="ThePage">
    <div class="container">
        <h1><?= $WPG['title']; ?></h1>
        <div class="entry">
            <?php the_content(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>