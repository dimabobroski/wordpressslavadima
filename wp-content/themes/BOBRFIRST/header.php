<!DOCTYPE html>
<?php global $WPG; ?>
<html <?php language_attributes(); echo !$WPG['is_rtl']?' dir="ltr"':''; ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>" charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
	<meta name="theme-color" content="<?= has($WPG['options']['theme_color_mob']??'') ? $WPG['options']['theme_color_mob'] : '#1A1145'; ?>"/>
	<?php if ( $WPG['noindex'] ) { ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php } ?>
	<?php // echo init_font(); //Assistant Default
	//echo init_fonts([['name'=>'Heebo','sizes'=>'300,400,500,600,700'],['name'=>'Assistant']]); ?>
	<?= has($WPG['options']['before_head']??'') ? $WPG['options']['before_head'] : ''; ?>
</head>
<body <?php body_class(); ?>>
<?= has($WPG['options']['before_head']??'') ? $WPG['options']['after_body'] : ''; ?>
<div id="Wrapper" class="flex fdc jcsb">
	<?php if ($WPG['template'] != 'page-wpbf-login.php') { ?>
	<div id="TopWrap">
		<div id="HeadFix">
			<header>

			</header>
		</div>
		<main id="Content">
	<?php } ?>