const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const browserSync = require('browser-sync').create();
var csslint = require('gulp-csslint');
const cleanCSS = require('gulp-clean-css');
 // add stylelint check scss
sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./wp-content/themes/twentytwenty-child/src/scss/*.scss')
        .pipe(sourcemaps.init({loadMaps: true}))
         .pipe(sass().on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(sourcemaps.write("."))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./wp-content/themes/twentytwenty-child/'));
});

gulp.task('lint', function() {
    gulp.src('./wp-content/themes/twentytwenty-child/*.css')
        .pipe(csslint())
        .pipe(csslint.formatter())
});

gulp.task('watch', function () {
    browserSync.init({
        proxy: "localhost"
    });
    gulp.watch(['./wp-content/themes/twentytwenty-child/src/scss/*.scss','**/*.php'], gulp.parallel(['sass']))
        .on('change',browserSync.reload);
});